﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
   public class VechileDetails:BaseEntity
    {
        public int? Id { get; set; }
        public string RegistrationNumber { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Variant { get; set; }
        public string Options { get; set; }
        public int MfdYear { get; set; }
        public string TransmitionType { get; set; }
        public string FuelType { get; set; }
        public string BodyType { get; set; }
        public string CustomerId { get; set; }
    }
}
