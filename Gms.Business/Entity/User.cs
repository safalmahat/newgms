﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
   public class User:BaseEntity
    {
        public int? Id { get; set; }
        public string UserName { get; set; }
        public string Hash { get; set; }
        public string TempHash { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsLocked { get; set; }
        public int PasswordFailureCount { get; set; }
        public List<RolesModel> Roles { get; set; }
    }

}
