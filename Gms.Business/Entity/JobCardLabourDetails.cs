﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
   public class JobCardLabourDetails:BaseEntity
    {
        public int? Id { get; set; }
        public int? JobId { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public string Unit { get; set; }
        public float Amount { get; set; }
        public float TotalAmount { get; set; }

    }
}
