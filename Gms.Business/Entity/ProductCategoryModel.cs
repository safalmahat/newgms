﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
  public  class ProductCategoryModel:BaseEntity
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool status { get; set; }
    }
}
