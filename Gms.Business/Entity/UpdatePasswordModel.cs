﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
   public class UpdatePasswordModel
    {
        public int Id { get; set; }
        public string Hash { get; set; }
        public string NewHash { get; set; }
    }
}
