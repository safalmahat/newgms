﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
  public  class ProductModel:BaseEntity
    {
        public int? Id { get; set; }
        public int CategoryId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public double? ActualPrice { get; set; }
        public double? DiscountPercentage { get; set; }
        public double? Price { get; set; }
        public int Stock { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; } = true;
    }
}
