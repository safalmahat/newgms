﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
   public class LoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public bool DeleteExistingToken { get; set; }
    }
}
