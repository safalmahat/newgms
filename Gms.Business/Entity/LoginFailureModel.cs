﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
    public class LoginFailureModel
    {
        public int LoginAttemptsLeft { get; set; }
        public bool ActiveToken { get; set; }
    }
}
    