﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
    public class JobCardDetails:BaseEntity
    {
        public int? Id { get; set; }
        public int? JobId { get; set; }
        public string ComplainDescription { get; set; }
    }
}
