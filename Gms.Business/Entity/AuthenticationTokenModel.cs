﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
    public class AuthenticationTokenModel
    {
        public string Token { get; set; }
        public int UserId { get; set; }
        public List<string> Roles { get; set; }
        public int ClientId { get; set; }
        public int ClientTypeId { get; set; }
        public string ClientName { get; set; }
        public string ClientAddress { get; set; }
        public string UserFullName { get; set; }
        public int RoleId { get; set; }
        public bool IsSuperUser { get; set; }
        public string Username { get; set; }
        public int CommisionPercentage { get; set; }
        public bool success { get; set; }
        public string message { get; set; }
    }
}
