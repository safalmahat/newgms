﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
   public class BaseEntity
    {
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int RowCount { get; set; }
    }
}
