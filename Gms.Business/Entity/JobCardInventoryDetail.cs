﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
  public  class JobCardInventoryDetail:BaseEntity
    {
        public int? Id { get; set; }
        public int? JobId { get; set; }
        public string Product { get; set; }
        public float Price { get; set; }
        public float Amount { get; set; }
        public int Unit { get; set; }
    }
}
