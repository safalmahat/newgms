﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
    public class JobCard:BaseEntity
    {
        public int? Id { get; set; }
        public string JobNumber { get; set; }
        public string KmReading { get; set; }
        public int VehicleId { get; set; }
        public float FuelQuantity { get; set; }
    }
}
