﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
   public interface ICurrentUser
    {
        string Token { get; set; }
        int UserId { get; set; }
        IEnumerable<string> Permissions { get; set; }
        int ClientId { get; set; }
        int ClientTypeId { get; set; }
        string ClientName { get; set; }
        string ClientAddress { get; set; }
        string UserFullName { get; set; }
        int RoleId { get; set; }
        bool IsSuperUser { get; set; }
        string Username { get; set; }
        int CommisionPercentage { get; set; }
    }
   public class CurrentUser: ICurrentUser
    {
        public string Token { get; set; }
        public int UserId { get; set; }
        public IEnumerable<string> Permissions { get; set; }
        public int ClientId { get; set; }
        public int ClientTypeId { get; set; }
        public string UserFullName { get; set; }
        public int RoleId { get; set; }
        public bool IsSuperUser { get; set; }
        public string Username { get; set; }
        public int CommisionPercentage { get; set; }
        public string ClientName { get; set; }
        public string ClientAddress { get; set; }
    }
}
