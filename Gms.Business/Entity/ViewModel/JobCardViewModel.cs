﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity.ViewModel
{
   public class JobCardViewModel
    {
        public JobCard JobCard { get; set; }
        public List<JobCardDetails> JobCardDetails { get; set; }
        public List<JobCardInventoryDetail> JobCardInventoryDetails { get; set; }
        public List<JobCardInventoryCheckDetails> JobCardInventoryCheckDetails { get; set; }
        public List<JobCardLabourDetails> JobCardLabourDetails{ get; set; }
    }
}
