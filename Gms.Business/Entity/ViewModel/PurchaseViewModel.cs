﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity.ViewModel
{
   public class PurchaseViewModel
    {
        public PurchaseModel Purchase { get; set; }
        public List<PurchaseDetailModel> PurchaseDetails { get; set; }
    }
}
