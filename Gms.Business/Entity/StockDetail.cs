﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
   public class StockDetail:BaseEntity
    {
        public int Id { get; set; }
        public int PurchaseId { get; set; }
        public int Quantity { get; set; }
        public int ProductId { get; set; }
    }
}
