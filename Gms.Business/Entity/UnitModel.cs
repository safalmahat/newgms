﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
    public class UnitModel:BaseEntity
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public bool status { get; set; }
    }
}
