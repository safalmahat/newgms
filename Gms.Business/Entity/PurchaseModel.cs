﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
  public  class PurchaseModel:BaseEntity
    {
        public int Id { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public string PurchaseNo { get; set; }
        public int TotalBillAmount { get; set; }
        public int TotalPaidAmount { get; set; }
        public int TotalDueAmount { get; set; }
        public string PaymentMethod { get; set; }
        public int? SupplierId { get; set; }
        public bool Status { get; set; } = true;
    }
}
