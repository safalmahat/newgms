﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
   public class RolesModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
    }
}
