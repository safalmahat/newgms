﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Entity
{
  public  class JobCardInventoryCheckDetails:BaseEntity
    {
        public int? Id { get; set; }
        public int? JobId { get; set; }
        public string Item { get; set; }
        public int Unit { get; set; }
    }
}
