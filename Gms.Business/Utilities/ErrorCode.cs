﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Utilities
{
    public enum ErrorCode
    {
        InvalidCredentials = 1000,
        NameIsNotUnique = 1001,
        InvalidAccessToken = 1002,
        UserLocked = 1003,
        ValidationError = 1004,
        UserExist=1005,
        PasswordLengthError = 1,
        PasswordNumericPresenceError = 2,
        PasswordSpecialCharPresenceError = 3,
        PasswordUpperCasePresenceError = 4,
        PasswordLowerCasePresenceError = 5,
        PasswordRepeatingCharError = 6,
        PasswordUserNameError = 7,
        PasswordHistoryError = 8,
        ValidationFirstNameReq = 2001,
        ValidationFirstNameLen = 2002,
        ValidationLastNameReq = 2003,
        ValidationLastNameLen = 2004,
        ValidationUserNameReq = 2005,
        ValidationUserNameLen = 2006,
        ValidationUserNameAllowedChars = 2007,
        ValidationUserNameReqAlpha = 2008,
        ValidationEmailReq = 2009,
        ValidationEmailError = 2010,
        ValidationPhoneError = 2011,
        ValidationExtError = 2012,
        ValidationJobTitleLength = 2013,
        ValidationStartDateReq = 2014,
        ValidationStartDateError = 2015,
        ValidationEndDateError = 2016,
        UserInactive = 1005,
        AccessDenied = 1006
    }
}
