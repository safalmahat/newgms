﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Utilities
{
   public static class Constants
    {
        public const string MESSAGE_USERNAME_DOES_NOT_EXISTS = "Username does not exists.";
        public const string MESSAGE_INVALID_PASSWORD= "Your Password is invalid";
        public const string MESSAGE_USER_ACCOUNT_IS_LOCKED = "Your account is locked. Please contact your administrator.";
        public const string MESSAGE_EMAIL_ALREADY_EXISTS = "Email is already exists.";
        public const string MESSAGE_USER_NAME_ALREADY_EXISTS = "Username is already exists.";
    }
}
