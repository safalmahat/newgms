﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Utilities
{
    public interface IJsonUtility
    {
        T DeSerialize<T>(string jsonString) where T : class;
        string Serialize<T>(T model) where T : class;
        string Serialize<T>(T model, string serializerSettings) where T : class;
    }
    public class Jsonutility : IJsonUtility
    {
        public T DeSerialize<T>(string jsonString) where T : class
        {
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        public string Serialize<T>(T model) where T : class
        {
            return JsonConvert.SerializeObject(model);
        }

        public string Serialize<T>(T model, string serializerSettings) where T : class
        {
            dynamic jsonSerializerSettings;
            switch (serializerSettings)
            {
                default:
                    {
                        jsonSerializerSettings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
                        break;
                    }
            }
            return JsonConvert.SerializeObject(model, jsonSerializerSettings);
        }
    }
}
