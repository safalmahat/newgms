﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.MappingProfile
{
   public class UserRoleMappingProfile:Profile
    {
        public UserRoleMappingProfile()
        {
            var map = CreateMap<UserRolesDto, UserRolesModel>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
               
          ;
            var reverseMap = map.ReverseMap().ValidateMemberList(MemberList.Destination);
        }
    }
}
