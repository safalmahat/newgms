﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.MappingProfile
{
   public class CustomerDetailMappingProfile:Profile
    {
        public CustomerDetailMappingProfile()
        {
            var map = CreateMap<CustomerDetailsDto, CustomerDetails>()
                   .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                   .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.Phone_Number))
                    .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                    .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
                    .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.Full_Name))
                    .ForMember(dest => dest.MobileNumber, opt => opt.MapFrom(src => src.Mobile_Number))
                    .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.Created_By))
                    .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.Created_On))
                    .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.Updated_By))
                    .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.Updated_On))
                    .ForMember(dest => dest.RowCount, opt => opt.MapFrom(src => src.Row_Count))
            ;
            var reverseMap = map.ReverseMap().ValidateMemberList(MemberList.Destination);
        }
    }
}
