﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.MappingProfile
{
  public  class VechileDetailMappingProfile:Profile
    {
        public VechileDetailMappingProfile()
        {
            var map = CreateMap<VehicleDetailsDto, VechileDetails>()
                   .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                   .ForMember(dest => dest.RegistrationNumber, opt => opt.MapFrom(src => src.Registration_Number))
                   .ForMember(dest => dest.MfdYear, opt => opt.MapFrom(src => src.Mfd_Year))
                   .ForMember(dest => dest.TransmitionType, opt => opt.MapFrom(src => src.Transmition_Type))
                   .ForMember(dest => dest.FuelType, opt => opt.MapFrom(src => src.Fuel_Type))
                   .ForMember(dest => dest.BodyType, opt => opt.MapFrom(src => src.Body_Type))
                   .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.Created_By))
                   .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.Customer_Id))
                   .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.Created_On))
                   .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.Updated_By))
                   .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.Updated_On));
            var reverseMap = map.ReverseMap().ValidateMemberList(MemberList.Destination);
        }

    }
}
