﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.MappingProfile
{
    public static class MappingProfileConfiguration
    {
        public static MapperConfiguration InitializeAutoMapper()
        {
            var profiles = new Profile[]
           {
                 new CustomerDetailMappingProfile(),
                 new VechileDetailMappingProfile(),
                 new JobCardMappingProfile(),
                 new JobCardDetailsMappingProfile(),
                 new JobCardInventoryDetailMappingProfile(),
                 new JobCardInventoryCheckDetailMappingProfile(),
                 new JobCardLabourDetailsMappingProfile(),
                 new UserMappingProfile(),
                 new SupplierMappingProfile(),
                 new UnitMappingProfile(),
                 new ProductCategoryMappingProfile(),
                 new ProductMappingProfile(),
                 new PurchaseMappingProfile(),
                 new PurchaseDetailMappingProfile(),
                 new StockDetailMappingProfile(),
                 new RolesMappingProfile(),
                 new UserRoleMappingProfile(),
           };
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfiles(profiles);
            });
            return config;
        }
    }
}
