﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.MappingProfile
{
   public class JobCardInventoryDetailMappingProfile:Profile
    {
        public JobCardInventoryDetailMappingProfile()
        {
            var map = CreateMap<JobCardInventoryDetailDto, JobCardInventoryDetail>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.JobId, opt => opt.MapFrom(src => src.Job_Id))
                .ForMember(dest => dest.Product, opt => opt.MapFrom(src => src.Product))
                 .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
                 .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount))
                 .ForMember(dest => dest.Unit, opt => opt.MapFrom(src => src.Unit))
                 .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.Created_By))
                 .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.Created_On))
                 .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.Updated_By))
                 .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.Updated_On))
                 .ForMember(dest => dest.RowCount, opt => opt.MapFrom(src => src.Row_Count))
         ;
            var reverseMap = map.ReverseMap().ValidateMemberList(MemberList.Destination);
        }
    }
}
