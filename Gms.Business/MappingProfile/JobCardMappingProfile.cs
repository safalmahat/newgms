﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.MappingProfile
{
   public class JobCardMappingProfile:Profile
    {
        public JobCardMappingProfile()
        {
            var map = CreateMap<JobCardDto, JobCard>()
                   .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                   .ForMember(dest => dest.JobNumber, opt => opt.MapFrom(src => src.Job_Number))
                   .ForMember(dest => dest.KmReading, opt => opt.MapFrom(src => src.Km_Reading))
                   .ForMember(dest => dest.VehicleId, opt => opt.MapFrom(src => src.Vehicle_Id))
                   .ForMember(dest => dest.JobNumber, opt => opt.MapFrom(src => src.Job_Number))
                   .ForMember(dest => dest.FuelQuantity, opt => opt.MapFrom(src => src.Fuel_Quantity))
                    .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.Created_By))
                    .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.Created_On))
                    .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.Updated_By))
                    .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.Updated_On));
            var reverseMap = map.ReverseMap().ValidateMemberList(MemberList.Destination);
        }

    }
}
