﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.MappingProfile
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            var map = CreateMap<UserDto, User>()
                  .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
                   .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.user_name))
                     .ForMember(dest => dest.TempHash, opt => opt.MapFrom(src => src.temp_hash))
                       .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.first_name))
                         .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.last_name))
                           .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.email))
                             .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.phone_number))
                               .ForMember(dest => dest.IsLocked, opt => opt.MapFrom(src => src.is_locked))
                                 .ForMember(dest => dest.PasswordFailureCount, opt => opt.MapFrom(src => src.password_failure_count))
 .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.Created_By))
                    .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.Created_On))
                    .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.Updated_By))
                    .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.Updated_On))
                    .ForMember(dest => dest.RowCount, opt => opt.MapFrom(src => src.Row_Count))
                  ;
            var reverseMap = map.ReverseMap().ValidateMemberList(MemberList.Destination);
        }
    }
}
