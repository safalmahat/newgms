﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.MappingProfile
{
  public  class RolesMappingProfile:Profile
    {
        public RolesMappingProfile()
        {
            var map = CreateMap<RolesDto, RolesModel>()
                   .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            ;
            var reverseMap = map.ReverseMap().ValidateMemberList(MemberList.Destination);
        }
    }
}
