﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.MappingProfile
{
    public class SupplierMappingProfile : Profile
    {
        public SupplierMappingProfile()
        {
            var map = CreateMap<SupplierDto, SupplierModel>()
                   .ForMember(dest => dest.ContactPerson, opt => opt.MapFrom(src => src.contact_person))
                   .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.phone_number))
                   .ForMember(dest => dest.MobileNumber, opt => opt.MapFrom(src => src.mobile_number))
                     .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.Created_By))
                    .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.Created_On))
                    .ForMember(dest => dest.UpdatedBy, opt => opt.MapFrom(src => src.Updated_By))
                    .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.Updated_On))
                    .ForMember(dest => dest.RowCount, opt => opt.MapFrom(src => src.Row_Count))
            ;


            var reverseMap = map.ReverseMap().ValidateMemberList(MemberList.Destination);
        }
    }
}
