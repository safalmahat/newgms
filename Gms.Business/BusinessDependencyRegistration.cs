﻿using Gms.Business.Component;
using Gms.Business.Entity;
using Gms.Business.MappingProfile;
using Gms.Business.Service;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Unity;
using Vilk.ERP.UserManagement.Services;

namespace Gms.Business
{
    public class BusinessDependencyRegistration
    {
        public static void Configure(IUnityContainer container)
        {
            container.RegisterInstance(MappingProfileConfiguration.InitializeAutoMapper().CreateMapper())
            .RegisterType<ICustomerDetailService, CustomerDetailService>()
            .RegisterType<IUtilityService, UtilityService>()
            .RegisterType<IVechileDetailService, VechileDetailService>()
            .RegisterType<IUserService, UserService>()
            .RegisterType<ICustomPasswordHasher, CustomPasswordHasher>()
            .RegisterType<ITokenGenerator, TokenGenerator>()
            .RegisterType<ITokenService, TokenService>()
            .RegisterInstance<Microsoft.AspNetCore.Identity.IPasswordHasher<object>>(new Microsoft.AspNetCore.Identity.PasswordHasher<object>(null))
            .RegisterType<ICurrentUser, CurrentUser>()
            .RegisterType<IJobCardService, JobCardService>()
            .RegisterType<IMasterDataService, MasterDataService>()
            .RegisterType<ISupplierService, SupplierService>()
            .RegisterType<IUnitService, UnitService>()
            .RegisterType<IProductCategoryService, ProductCategoryService>()
             .RegisterType<IProductService, ProductService>()
             .RegisterType<IPurchaseService, PurchaseService>()
            ;
        }
    }
}
