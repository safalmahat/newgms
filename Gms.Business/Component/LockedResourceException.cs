﻿using Gms.Business.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Component
{
    public class LockedResourceException : Exception
    {
        public LockedResourceException(ErrorCode errorCode, string errorMessage)
            : base(errorMessage)
        {
            HResult = (int)errorCode;
        }
    }
}
