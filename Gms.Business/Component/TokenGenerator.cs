﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Component
{
    public interface ITokenGenerator
    {
        string GetToken();
    }
    public class TokenGenerator : ITokenGenerator
    {
        public string GetToken()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
