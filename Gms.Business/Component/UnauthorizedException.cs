﻿using Gms.Business.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Component
{
    public class UnauthorizedException : Exception
    {
        public UnauthorizedException(ErrorCode errorCode, string errorMessage)
            : base(errorMessage)
        {
            HResult = (int)errorCode;
        }
    }
}
