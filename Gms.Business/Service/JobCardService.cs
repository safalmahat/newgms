﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.Business.Entity.ViewModel;
using Gms.DAL.DTO;
using Gms.DAL.Repo;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gms.Business.Service
{
    public interface IJobCardService
    {
        Task<List<dynamic>> GetAllJob(PagedDatasource pagedDatasource);
        Task<List<JobCardDetails>> GetJobCardDetailsByJobId(int id);
        Task Insert(JobCardViewModel data);
        Task<JobCardViewModel> GetJobCard(int id);
        Task Update(JobCardViewModel data);
        Task<int> GenerateJobNumber();
    }
   public class JobCardService: IJobCardService
    {
        private readonly IJobCardRepo _jobCardRepo;
        private readonly IMapper _mapper;
        IDatabaseManager _databaseManager;
        ITransctionScopeFactory _transctionScopeFactory;
        private readonly ICurrentUser _currentUser;
        private readonly IMasterDataRepo _masterDataRepo;
        public JobCardService(IJobCardRepo jobCardRepo,IMapper mapper, IDatabaseManager databaseManager,
                ITransctionScopeFactory transctionScopeFactory, 
                ICurrentUser currentUser,
                IMasterDataRepo masterDataRepo
                )
        {
            _jobCardRepo = jobCardRepo;
            _mapper = mapper;
            _databaseManager = databaseManager;
            _transctionScopeFactory = transctionScopeFactory;
            _currentUser = currentUser;
            _masterDataRepo = masterDataRepo;
        }
        public async Task<List<dynamic>> GetAllJob(PagedDatasource pagedDatasource)
        {
            if (string.IsNullOrWhiteSpace(pagedDatasource.SORTBYCOLUMN))
            {
                pagedDatasource.SORTBYCOLUMN = " Id ";
            }
            return await _jobCardRepo.GetAllJob(pagedDatasource);
        }

        public async Task<JobCardViewModel> GetJobCard(int id)
        {
            JobCardViewModel jobCardModel = new JobCardViewModel();

            JobCard jobCard=  _mapper.Map<JobCard>(await _jobCardRepo.GetjobCard(id));
            List<JobCardDetails> jobCardDetails = _mapper.Map<List<JobCardDetails>>(await _jobCardRepo.GetJobCardDetailsByJobId(id));

            List<JobCardInventoryDetail> jobCardInventoryDetail = _mapper.Map<List<JobCardInventoryDetail>>(await _jobCardRepo.GetJobCardInventoryDetailsByJobId(id));

            List<JobCardInventoryCheckDetails> jobCardInventoryCheckDetailsDto = _mapper.Map<List<JobCardInventoryCheckDetails>>(await _jobCardRepo.GetInventoryCheckItemsByJobId(id));


            List<JobCardLabourDetails> jobCardLabourDetails = _mapper.Map<List<JobCardLabourDetails>>(await _jobCardRepo.GetLabourByJobId(id));
            jobCardModel.JobCard = jobCard;

            jobCardModel.JobCardDetails = jobCardDetails;
            jobCardModel.JobCardInventoryDetails = jobCardInventoryDetail;
            jobCardModel.JobCardInventoryCheckDetails = jobCardInventoryCheckDetailsDto;
            jobCardModel.JobCardLabourDetails = jobCardLabourDetails;
            return jobCardModel;
        }

        public  async Task<List<JobCardDetails>> GetJobCardDetailsByJobId(int id)
        {
            return _mapper.Map<List<JobCardDetails>>(await _jobCardRepo.GetJobCardDetailsByJobId(id));
        }

        public async Task Insert(JobCardViewModel data)
        {
            JobCardDto jobCard = _mapper.Map<JobCardDto>(data.JobCard);
           List<JobCardDetailsDto> jobCardDetailsDto = _mapper.Map<List<JobCardDetailsDto>>(data.JobCardDetails);
            List<JobCardInventoryDetailDto> JobCardInventoryDetailDto= _mapper.Map<List<JobCardInventoryDetailDto>>(data.JobCardInventoryDetails);
            List<JobCardInventoryCheckDetailsDto> jobCardInventoryCheckDetails = _mapper.Map<List<JobCardInventoryCheckDetailsDto>>(data.JobCardInventoryCheckDetails);
            List<JobCardLabourDetailDto> jobCardLabourDetailDto = _mapper.Map<List<JobCardLabourDetailDto>>(data.JobCardLabourDetails);
            //save job detail
            jobCard.Updated_On = DateTime.Now;
            int id = await _jobCardRepo.SaveJobCard(jobCard);
            jobCardDetailsDto.ForEach(job =>
            {
                job.Created_On = DateTime.Now;
                job.Updated_On = DateTime.Now;
                job.Created_By = _currentUser.UserId;
                job.Updated_By = _currentUser.UserId;
                job.Job_Id = id;
                _jobCardRepo.SaveJobCardDetail(job);
            });
            JobCardInventoryDetailDto.ForEach(jobIn =>
            {
                jobIn.Created_By = _currentUser.UserId;
                jobIn.Updated_By = _currentUser.UserId;
                jobIn.Created_On = DateTime.Now;
                jobIn.Updated_On = DateTime.Now;
                jobIn.Job_Id = id;
                _jobCardRepo.SaveJobCardInventoryDetail(jobIn);
            });
            jobCardInventoryCheckDetails.ForEach(inventory =>
            {
                inventory.Created_On = DateTime.Now;
                inventory.Created_By = _currentUser.UserId;
                inventory.Updated_On = DateTime.Now;
                inventory.Updated_By = _currentUser.UserId;
                inventory.Job_Id = id;
                _jobCardRepo.SaveJobCardInventoryCheckDetail(inventory);
            });
            jobCardLabourDetailDto.ForEach(labour =>
            {
                labour.Created_On = DateTime.Now;
                labour.Created_By = _currentUser.UserId;
                labour.Updated_On = DateTime.Now;
                labour.Updated_By = _currentUser.UserId;
                labour.Job_Id = id;
                _jobCardRepo.SaveJobCardLabourDetail(labour);
            });
        }
        public async Task Update(JobCardViewModel data)
        {
            JobCardDto jobCard = _mapper.Map<JobCardDto>(data.JobCard);
            List<JobCardDetailsDto> jobCardDetailsDto = _mapper.Map<List<JobCardDetailsDto>>(data.JobCardDetails);
            List<JobCardInventoryDetailDto> JobCardInventoryDetailDto = _mapper.Map<List<JobCardInventoryDetailDto>>(data.JobCardInventoryDetails);
            List<JobCardInventoryCheckDetailsDto> jobCardInventoryCheckDetails = _mapper.Map<List<JobCardInventoryCheckDetailsDto>>(data.JobCardInventoryCheckDetails);
            List<JobCardLabourDetailDto> jobCardLabourDetailDto = _mapper.Map<List<JobCardLabourDetailDto>>(data.JobCardLabourDetails);
            //save job detail
            jobCard.Updated_On = DateTime.Now;
           await _jobCardRepo.UpdateJobCard(jobCard);
            jobCardDetailsDto.ForEach(job =>
            {
                if(job.Id>0)
                {
                    job.Updated_By = _currentUser.UserId;
                    job.Updated_On = DateTime.Now;
                    _jobCardRepo.UpdateJobCardDetail(job);
                }
                else
                {
                    job.Created_On = DateTime.Now;
                    job.Updated_On = DateTime.Now;
                    job.Created_By = _currentUser.UserId;
                    job.Updated_By = _currentUser.UserId;
                    job.Job_Id =(int)data.JobCard.Id;
                    _jobCardRepo.SaveJobCardDetail(job);
                }
              
            });
            JobCardInventoryDetailDto.ForEach(jobIn =>
            {
                if(jobIn.Id>0)
                {
                    jobIn.Updated_By = _currentUser.UserId;
                    jobIn.Updated_On = DateTime.Now;
                    _jobCardRepo.UpdateJobCardInventoryDetail(jobIn);
                }
                else
                {
                    jobIn.Created_By = _currentUser.UserId;
                    jobIn.Updated_By = _currentUser.UserId;
                    jobIn.Created_On = DateTime.Now;
                    jobIn.Updated_On = DateTime.Now;
                    jobIn.Job_Id =(int)data.JobCard.Id;
                    _jobCardRepo.SaveJobCardInventoryDetail(jobIn);
                }
               
            });
            jobCardInventoryCheckDetails.ForEach(inventory =>
            {
                if(inventory.Id>0)
                {
                    inventory.Updated_By = _currentUser.UserId;
                    inventory.Updated_On = DateTime.Now;
                    _jobCardRepo.UpdateJobCardInventoryCheckDetail(inventory);
                }
                else
                {
                    inventory.Created_On = DateTime.Now;
                    inventory.Created_By = _currentUser.UserId;
                    inventory.Updated_On = DateTime.Now;
                    inventory.Updated_By = _currentUser.UserId;
                    inventory.Job_Id = jobCard.Id;
                    _jobCardRepo.SaveJobCardInventoryCheckDetail(inventory);
                }
               
            });
            jobCardLabourDetailDto.ForEach(labour =>
            {
                if(labour.Id>0)
                {
                    labour.Updated_On = DateTime.Now;
                    labour.Updated_By = _currentUser.UserId;
                    _jobCardRepo.UpdateJobCardLabourDetail(labour);
                }
                else
                {
                    labour.Created_On = DateTime.Now;
                    labour.Created_By = _currentUser.UserId;
                    labour.Updated_On = DateTime.Now;
                    labour.Updated_By = _currentUser.UserId;
                    labour.Job_Id = jobCard.Id;
                    _jobCardRepo.SaveJobCardLabourDetail(labour);
                }
         
            });
        }
        public async Task<int> GenerateJobNumber()
        {
            int count = await _jobCardRepo.GetTotalJobCard();
            return count + 1;
        }
    }
}
