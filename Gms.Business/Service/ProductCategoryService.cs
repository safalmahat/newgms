﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using Gms.DAL.Repo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gms.Business.Service
{
    public interface IProductCategoryService
    {
        Task<List<ProductCategoryModel>> GetProductAllCategory(PagedDatasource pagedDataSource);
        Task Save(ProductCategoryModel data);
        Task<ProductCategoryModel> Get(int id);
        Task Update(ProductCategoryModel data);
        Task Delete(int id);
    }
    public class ProductCategoryService : IProductCategoryService
    {
        private IProductCategoryRepo _productCategoryRepo;
        private IMapper _mapper;
        private ICurrentUser _currentUser;
        public ProductCategoryService(IProductCategoryRepo productCategoryRepo, IMapper mapper, ICurrentUser currentUser)
        {
            _productCategoryRepo = productCategoryRepo;
            _mapper = mapper;
            _currentUser = currentUser;
        }



        public async Task<List<ProductCategoryModel>> GetProductAllCategory(PagedDatasource pagedDataSource)
        {
            pagedDataSource.SORTBYCOLUMN = " id";
            if (!string.IsNullOrEmpty(pagedDataSource.FILTERQUERY) && pagedDataSource.FILTERQUERY != "undefined")
                pagedDataSource.FILTERQUERY = " where " + pagedDataSource.FILTERQUERY;
            if (pagedDataSource.FILTERQUERY == "undefined")
                pagedDataSource.FILTERQUERY = string.Empty;
            List<ProductCategoryDto> dtos = await _productCategoryRepo.GetProductAllCategory(pagedDataSource);
            List<ProductCategoryModel> models = _mapper.Map<List<ProductCategoryModel>>(dtos);
            return models;
        }

        public async Task Save(ProductCategoryModel data)
        {
            data.status = true;
            ProductCategoryDto dto = _mapper.Map<ProductCategoryDto>(data);
            dto.Created_By = _currentUser.UserId;
            dto.Created_On= System.DateTime.Now;
            await _productCategoryRepo.Save(dto);
        }
        public async Task Delete(int id)
        {
            await _productCategoryRepo.Delete(id);
        }

        public async Task<ProductCategoryModel> Get(int id)
        {
            ProductCategoryDto unit = await _productCategoryRepo.Get(id);
            return _mapper.Map<ProductCategoryModel>(unit);
        }
        public async Task Update(ProductCategoryModel data)
        {
            ProductCategoryDto dto = _mapper.Map<ProductCategoryDto>(data);
            dto.Updated_By = _currentUser.UserId;
            dto.Updated_On = System.DateTime.Now;
            await _productCategoryRepo.Update(dto);
        }
    }
}
