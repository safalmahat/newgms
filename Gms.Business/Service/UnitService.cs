﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using Gms.DAL.Repo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gms.Business.Service
{
    public interface IUnitService
    {
        Task<List<UnitModel>> GetUnits(PagedDatasource pagedDataSource);
        Task Save(UnitModel data);
        Task<UnitModel> Get(int id);
        Task Update(UnitModel data);
        Task Delete(int id);

    }
    public class UnitService : IUnitService
    {
        private IUnitRepo _unitRepo;
        private IMapper _mapper;
        private ICurrentUser _currentUser;
        public UnitService(IUnitRepo unitRepo, IMapper mapper, ICurrentUser currentUser)
        {
            _unitRepo = unitRepo;
            _mapper = mapper;
            _currentUser = currentUser;
        }



        public async Task<List<UnitModel>> GetUnits(PagedDatasource pagedDataSource)
        {
            pagedDataSource.SORTBYCOLUMN = " id";
            if (!string.IsNullOrEmpty(pagedDataSource.FILTERQUERY) && pagedDataSource.FILTERQUERY != "undefined")
                pagedDataSource.FILTERQUERY = " where " + pagedDataSource.FILTERQUERY;
            if (pagedDataSource.FILTERQUERY == "undefined")
                pagedDataSource.FILTERQUERY = string.Empty;
            List<UnitDto> unitDtos = await _unitRepo.GetUnits(pagedDataSource);
            List<UnitModel> unitModels = _mapper.Map<List<UnitModel>>(unitDtos);
            return unitModels;
        }

        public async Task Save(UnitModel data)
        {
            data.status = true;
            UnitDto unitDto = _mapper.Map<UnitDto>(data);
            unitDto.Created_By = _currentUser.UserId;
            unitDto.Created_On = System.DateTime.Now;
            await _unitRepo.Save(unitDto);
        }
        public async Task Delete(int id)
        {
            await _unitRepo.Delete(id);
        }

        public async Task<UnitModel> Get(int id)
        {
            UnitDto unit = await _unitRepo.Get(id);
            return _mapper.Map<UnitModel>(unit);
        }
        public async Task Update(UnitModel data)
        {
            UnitDto unitDto = _mapper.Map<UnitDto>(data);
            unitDto.Updated_By = _currentUser.UserId;
            unitDto.Updated_On = System.DateTime.Now;
            await _unitRepo.Update(unitDto);
        }
    }
}
