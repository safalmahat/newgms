﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using Gms.DAL.Repo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gms.Business.Service
{
    public interface ISupplierService
    {
        Task<List<SupplierModel>> Get(PagedDatasource pagedDataSource);
        Task Save(SupplierModel data);
        Task<SupplierModel> Get(int id);
        Task Update(SupplierModel data);
        Task Delete(int id);
    }
    public class SupplierService : ISupplierService
    {
        private ISupplierRepo _supplierRepo;
        private IMapper _mapper;
        private ICurrentUser _currentUser;
        private IUtilityService _utilityService;
        public SupplierService(ISupplierRepo supplierRepo, IMapper mapper, ICurrentUser currentUser, IUtilityService utilityService)
        {
            _supplierRepo = supplierRepo;
            _mapper = mapper;
            _currentUser = currentUser;
            _utilityService = utilityService;
        }
        public async Task Delete(int id)
        {
            await _supplierRepo.Delete(id);
        }

        public async Task<List<SupplierModel>> Get(PagedDatasource pagedDataSource)
        {
            if (!string.IsNullOrEmpty(pagedDataSource.FILTERQUERY) && pagedDataSource.FILTERQUERY != "undefined")
                pagedDataSource.FILTERQUERY = " where " + pagedDataSource.FILTERQUERY;
            if (pagedDataSource.FILTERQUERY == "undefined")
                pagedDataSource.FILTERQUERY = string.Empty;
            if (string.IsNullOrWhiteSpace(pagedDataSource.SORTBYCOLUMN))
            {
                pagedDataSource.SORTBYCOLUMN = " Id ";
            }
            else
            {
                pagedDataSource.SORTBYCOLUMN = _utilityService.ToDTOProperty<CustomerDetails, CustomerDetailsDto>(pagedDataSource.SORTBYCOLUMN);
            }
            List<SupplierDto> supplierDtos = await _supplierRepo.Get(pagedDataSource);
            List<SupplierModel> supplierModels = _mapper.Map<List<SupplierModel>>(supplierDtos);
            return supplierModels;
        }

        public async Task<SupplierModel> Get(int id)
        {
            SupplierDto supplierDto = await _supplierRepo.Get(id);
            SupplierModel supplierModel = _mapper.Map<SupplierModel>(supplierDto);
            return supplierModel;
        }

        public async Task Save(SupplierModel data)
        {
            SupplierDto supplierDto = _mapper.Map<SupplierDto>(data);
            supplierDto.status = true;
            supplierDto.Created_By = _currentUser.UserId;
            supplierDto.Created_On = System.DateTime.Now;
            await _supplierRepo.Save(supplierDto);
        }

        public async Task Update(SupplierModel data)
        {
            SupplierDto supplierDto = _mapper.Map<SupplierDto>(data);
            supplierDto.Updated_By = _currentUser.UserId;
            supplierDto.Updated_On = System.DateTime.Now;
            await _supplierRepo.Update(supplierDto);
        }
    }
}
