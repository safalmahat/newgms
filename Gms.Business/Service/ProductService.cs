﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using Gms.DAL.Repo;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Gms.Business.Service
{
    public interface IProductService
    {
        Task<List<dynamic>> GetAllProduct(PagedDatasource pagedDataSource);
        Task Save(ProductModel data);
        Task<ProductModel> Get(int id);
        Task Update(ProductModel data);
        Task Delete(int id);
        Task<List<ProductModel>> GetAvailableProduct();
    }
    public class ProductService : IProductService
    {
        private IProductRepo _productRepo;
        private IMapper _mapper;
        private ICurrentUser _currentUser;
        private readonly string _wwwRootPath;
        private readonly IHostingEnvironment _hostingEnv;
        private readonly IUtilityService _utilityService;
        public ProductService(IProductRepo productRepo, IMapper mapper, ICurrentUser currentUser, IHostingEnvironment hostingEnv, IUtilityService utilityService)
        {
            _productRepo = productRepo;
            _mapper = mapper;
            _currentUser = currentUser;
            this._hostingEnv = hostingEnv;
            _utilityService = utilityService;
            this._wwwRootPath = Path.Combine(this._hostingEnv.ContentRootPath, "wwwroot");
        }
        public async Task Delete(int id)
        {
            await _productRepo.Delete(id);
        }

        public async Task<ProductModel> Get(int id)
        {
            ProductDto unit = await _productRepo.Get(id);
            return _mapper.Map<ProductModel>(unit);
        }

        public async Task<List<dynamic>> GetAllProduct(PagedDatasource pagedDataSource)
        {
          
            if (!string.IsNullOrEmpty(pagedDataSource.FILTERQUERY) && pagedDataSource.FILTERQUERY != "undefined")
            {
                if (_currentUser.ClientTypeId != 1)
                {
                    pagedDataSource.FILTERQUERY = " and " + pagedDataSource.FILTERQUERY;
                }
                else
                    pagedDataSource.FILTERQUERY = " where " + pagedDataSource.FILTERQUERY;
            }
            if (pagedDataSource.FILTERQUERY == "undefined")
                pagedDataSource.FILTERQUERY = string.Empty;
            if (string.IsNullOrWhiteSpace(pagedDataSource.SORTBYCOLUMN))
            {
                pagedDataSource.SORTBYCOLUMN = " Id ";
            }
            else
            {
                pagedDataSource.SORTBYCOLUMN = _utilityService.ToDTOProperty<ProductModel, ProductDto>(pagedDataSource.SORTBYCOLUMN);
            }
            List<dynamic> productDtos = await _productRepo.GetAllProduct(pagedDataSource);
         //   List<ProductModel> productModels = _mapper.Map<List<dynamic>>(productDtos);
            return productDtos;

        }

        public async Task Save(ProductModel data)
        {
            try
            {
                data.Status = true;
                //data.Thumbnail = await this.Upload(data.thumbnail, ImageTypeEnum.Product);
                ProductDto dto = _mapper.Map<ProductDto>(data);
                dto.Created_By = _currentUser.UserId;
                dto.Created_On = System.DateTime.Now;
                await _productRepo.Save(dto);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public async Task Update(ProductModel data)
        {
            data.Status = true;
           // data.thumbnail = await this.Upload(data.thumbnail, ImageTypeEnum.Product);
            ProductDto dto = _mapper.Map<ProductDto>(data);
            dto.Updated_By = _currentUser.UserId;
            dto.Updated_On = System.DateTime.Now;
            await _productRepo.Update(dto);
        }
        //public async Task<string> Upload(string base64, ImageTypeEnum imageType)
        //{
        //    if (base64 == null)
        //        return null;
        //    bool deleteSourceFile = true;

        //    ImageDataHelper image = ImageDataHelper.TryParse(base64);
        //    if (image != null)
        //    {
        //        string[] splitChar = image != null ? image.MimeType.Split(new char[] { '/' }) : new string[] { };
        //        string extension = splitChar[splitChar.Length - 1];

        //        string fileName = GenerateUniqueNameWithExtension(extension);
        //        string newFileName = GenerateUniqueNameWithExtension(extension);
        //        //Create folder
        //        string imageBasePathString = Path.Combine("images", GetImageBaseFolderName(imageType));
        //        string imageBasePath = Path.Combine(this._wwwRootPath, "images", GetImageBaseFolderName(imageType));
        //        bool exists = Directory.Exists(imageBasePath);
        //        if (!exists) Directory.CreateDirectory(imageBasePath);

        //        string imagefullPath = Path.Combine(this._wwwRootPath, imageBasePath, fileName);
        //        //    string newFilePath = Path.Combine(this._wwwRootPath, imageBasePath, newFileName);
        //        if (image != null)
        //            try
        //            {
        //                File.WriteAllBytes(imagefullPath, image.RawData);
        //                string returnPath = Path.Combine(imageBasePath, fileName);
        //                string newFilePath = Path.Combine(imageBasePath, newFileName);
        //                ImageDataHelper.OptimizeImage(returnPath, newFilePath, 300, 300, deleteSourceFile, 20);
        //                //  ImageDataHelper.OptimizeImage(imagefullPath, newFilePath, 300, 300, deleteSourceFile, 75);

        //            }
        //            catch (Exception ex)
        //            {

        //            }
        //        return Path.Combine(imageBasePathString, newFileName);
        //    }
        //    return "";
        //}
        //private string GetImageBaseFolderName(ImageTypeEnum imageTypeEnum)
        //{
        //    switch (imageTypeEnum)
        //    {
        //        case ImageTypeEnum.Product:
        //            return ImagePathConstants.Product;
        //        case ImageTypeEnum.Default:
        //            return ImagePathConstants.Defaults;
        //    }
        //    return null;
        //}

        private string GenerateUniqueNameWithExtension(string extension)
        {
            string UniqueName = String.Format("{0:d9}", (DateTime.Now.Ticks / 10) % 1000000000);
            string[] arr = { UniqueName.Trim(), ".", extension.Trim() };
            return string.Concat(arr);
        }
        public async Task<List<ProductModel>> GetAvailableProduct()
        {
            return _mapper.Map<List<ProductModel>>(await _productRepo.GetAvailableProduct());
        }
    }
}
