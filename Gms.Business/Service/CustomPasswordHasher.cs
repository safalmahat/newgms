﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.Business.Service
{
    public interface ICustomPasswordHasher
    {
        string HashPassword(string password);
        PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword);
    }
    public class CustomPasswordHasher : ICustomPasswordHasher
    {
        private IPasswordHasher<object> _passwordHasher;
        public object Obj { get; set; }
        public CustomPasswordHasher(IPasswordHasher<object> passwordHasher)
        {
            Obj = new object();
            _passwordHasher = passwordHasher;
        }
        public string HashPassword(string password)
        {
            return _passwordHasher.HashPassword(Obj, password);
        }

        public PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            return _passwordHasher.VerifyHashedPassword(Obj, hashedPassword, providedPassword);
        }
    }
}
