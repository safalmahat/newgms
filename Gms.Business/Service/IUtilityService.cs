﻿using AutoMapper;
using Gms.Business.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Gms.Business.Service
{
    public interface IUtilityService
    {
        string ToDTOProperty<T1, T2>(string modelProp) where T1 : class where T2 : class;
    }
    public class UtilityService : IUtilityService
    {
        private IMapper _mapper;
        public UtilityService(IMapper mapper)
        {
            _mapper = mapper;
        }
        public string ToDTOProperty<T1, T2>(string modelProp) where T1 : class where T2 : class
        {
            var map = _mapper.ConfigurationProvider.FindTypeMapFor<T1, T2>();

            var propertyMap = map.PropertyMaps.First(pm => pm.SourceMember ==
            typeof(T1).GetProperty(modelProp, BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase));

            var attrInfo = propertyMap.SourceMember.GetCustomAttribute<TableAliasAttribute>();
            string aliasName = attrInfo == null ? "" : attrInfo.Name;

            return (string.IsNullOrWhiteSpace(attrInfo?.Name) ? "" : "" + attrInfo?.Name + ".") + propertyMap.DestinationName;
        }
    }
}
