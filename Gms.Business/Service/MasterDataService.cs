﻿using Gms.DAL.DTO;
using Gms.DAL.Repo;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gms.Business.Service
{
    public interface IMasterDataService
    {
        Task<List<MasterDataDto>> GetMasterDataByCategory(string category);
    }
    public class MasterDataService : IMasterDataService
    {
        private readonly IMasterDataRepo _masterDataRepo;
        public MasterDataService(IMasterDataRepo masterDataRepo)
        {
            _masterDataRepo = masterDataRepo;
        }
        public async Task<List<MasterDataDto>> GetMasterDataByCategory(string category)
        {
            return await _masterDataRepo.GetMasterDataByCategory(category);
        }
    }
}
