﻿using AutoMapper;
using Gms.Business.Component;
using Gms.Business.Entity;
using Gms.Business.Service;
using Gms.Business.Utilities;
using Gms.DAL.DTO;
using Gms.DAL.Repo;
using Gms.Database;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Vilk.ERP.UserManagement.Services
{
    public interface IUserService
    {
        Task<UserDto> ValidateLoginCredentials(LoginModel value);
        Task<User> GetUserById(int userId);
        Task<UserDto> GetUserByEmail(string email);
        Task SaveUser(User data);
        Task<List<User>> GetUsers(PagedDatasource pagedDatasource);
        Task UpdateUserProfile(User data);
        Task UpdatePassword(UpdatePasswordModel data);
        Task<List<RolesModel>> GetRoles();
        Task<List<string>> GetAllRolesByUserId(int userId);
    }


    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUserRepo _userRepo;
        private readonly ICustomPasswordHasher _passwordHasher;
        private readonly IDatabaseManager _databaseManager;
        private readonly ITransctionScopeFactory _transctionScopeFactory;
        private readonly IUtilityService _utilityService;

        public UserService(
            IMapper mapper,
            IUserRepo userRepo,
            ICustomPasswordHasher passwordHasher,
            IDatabaseManager databaseManager,
            ITransctionScopeFactory transctionScopeFactory,
            UtilityService utilityService
            )
        {
            _mapper = mapper;
            _userRepo = userRepo;
            _passwordHasher = passwordHasher;
            _databaseManager = databaseManager;
            _transctionScopeFactory = transctionScopeFactory;
            _utilityService = utilityService;
        }

        public async Task<UserDto> GetUserByEmail(string email)
        {
            UserDto userDto = await _userRepo.GetUserByEmail(email);
            return _mapper.Map<UserDto>(userDto);
        }

        public async Task<User> GetUserById(int userId)
        {
            return _mapper.Map<User>(await _userRepo.GetUserById(userId));
        }

        public async Task<List<User>> GetUsers(PagedDatasource pagedDatasource)
        {
            if (string.IsNullOrWhiteSpace(pagedDatasource.SORTBYCOLUMN))
            {
                pagedDatasource.SORTBYCOLUMN = " Id ";
            }
            else
            {
                pagedDatasource.SORTBYCOLUMN = _utilityService.ToDTOProperty<User, UserDto>(pagedDatasource.SORTBYCOLUMN);
            }
            List<User> data = _mapper.Map<List<User>>(await _userRepo.GetAllUsers(pagedDatasource));
            return data;
        }

        public async Task SaveUser(User data)
        {
            var user = await _userRepo.CheckUserExist(data.UserName);
            if (user != null)
            {
                throw new BadRequestException(ErrorCode.UserExist, Constants.MESSAGE_USER_NAME_ALREADY_EXISTS);
            }
            UserDto userDto = _mapper.Map<UserDto>(data);
            userDto.Created_On = System.DateTime.Now;
            userDto.Updated_On = System.DateTime.Now;
            //hash password
            userDto.hash = _passwordHasher.HashPassword(userDto.hash);
            userDto.password_failure_count = 0;
            int userId = await _userRepo.SaveUser(userDto);
            //save user roles
            await _userRepo.TaskDeleteAllRoles(userId);
            foreach (var item in data.Roles)
            {
                UserRolesDto userRolesDto = new UserRolesDto();
                userRolesDto.UserId = userId;
                userRolesDto.RoleId = item.Id;
                await _userRepo.InsertUserInRoles(userRolesDto);
            }
        }

        public async Task<UserDto> ValidateLoginCredentials(LoginModel value)
        {
            UserDto userDto = null;
            LoginFailureModel loginFailureModel = new LoginFailureModel();
            if (value != null && !string.IsNullOrWhiteSpace(value.Username))
            {
                userDto = await _userRepo.GetUser(value.Username);
            }
            if (userDto == null)
            {
                throw new BadRequestException(ErrorCode.InvalidCredentials, Constants.MESSAGE_USERNAME_DOES_NOT_EXISTS);
                //throw new System.Exception("User does not exists");
            }

            if (userDto.is_locked)
            {
                throw new LockedResourceException(
                                ErrorCode.UserLocked,
                                Constants.MESSAGE_USER_ACCOUNT_IS_LOCKED);
                //throw new System.Exception("User account is locaked");
            }
            PasswordVerificationResult passwordVerificationResult = _passwordHasher.VerifyHashedPassword(userDto.hash, value.Password);
            if (passwordVerificationResult == PasswordVerificationResult.Failed)
            {
                throw new BadRequestException(ErrorCode.InvalidCredentials, Constants.MESSAGE_INVALID_PASSWORD);
                //throw new System.Exception("Invalid Password");
            }
            return userDto;
        }
        public async Task UpdateUserProfile(User data)
        {
            await _userRepo.UpdateUser(_mapper.Map<UserDto>(data));
            //save user roles
            await _userRepo.TaskDeleteAllRoles((int)data.Id);
            foreach (var item in data.Roles)
            {
                await _userRepo.InsertUserInRoles(_mapper.Map<UserRolesDto>(item));
            }
        }
        public async Task UpdatePassword(UpdatePasswordModel data)
        {

            //check old password matched or not
            string password = await _userRepo.GetPassword(data.Id);
            PasswordVerificationResult passwordVerificationResult = _passwordHasher.VerifyHashedPassword(password, data.Hash);
            if (passwordVerificationResult == PasswordVerificationResult.Failed)
            {
                throw new BadRequestException(ErrorCode.InvalidCredentials, Constants.MESSAGE_INVALID_PASSWORD);
            }
            else
            {
                UserDto dto = new UserDto();
                dto.id = data.Id;
                dto.hash = _passwordHasher.HashPassword(data.NewHash);
                await _userRepo.UpdatePassword(dto);
            }

        }
        public async Task<List<RolesModel>> GetRoles()
        {
            return _mapper.Map<List<RolesModel>>(await _userRepo.GetAllRoles());
        }
       public async Task<List<string>> GetAllRolesByUserId(int userId)
        {
            return await _userRepo.GetAllRolesByUserId(userId);
        }
    }
}
