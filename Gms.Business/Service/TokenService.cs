﻿using Gms.Business.Component;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using Gms.DAL.Repo;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Vilk.ERP.UserManagement.Services
{
    public interface ITokenService
    {
        Task<AuthenticationTokenModel> CreateToken(LoginModel loginModel);
        Task<AuthenticationTokenModel> ValidateToken(string tokenValue);
        void Delete(string value);
    }
    public class TokenService : ITokenService
    {
        private IUserService _userService;
        private ITokenGenerator _tokenGenerator;
        private ITokenRepo _tokenRepo;
    
        public TokenService(IUserService userService, ITokenGenerator tokenGenerator,
            ITokenRepo tokenRepo
            )
        {
            _userService = userService;
            _tokenGenerator = tokenGenerator;
            _tokenRepo = tokenRepo;
         
        }
        public async Task<AuthenticationTokenModel> CreateToken(LoginModel loginModel)
        {
            AuthenticationTokenModel authenticationTokenModel = null;
            UserDto userDto = await _userService.ValidateLoginCredentials(loginModel);

            if (userDto != null)
            {
                string newToken = _tokenGenerator.GetToken();
                var token = new TokenDto()
                {
                    value = newToken,
                    user_id = userDto.id,
                    time_stamp = System.DateTime.Now,
                };
                //get role id 
                authenticationTokenModel = new AuthenticationTokenModel()
                {
                    Token = newToken,
                    UserId = userDto.id,
                    UserFullName = string.Format("{0} {1}", userDto.first_name, userDto.last_name),
                    Username = userDto.user_name,
                    Roles = await _userService.GetAllRolesByUserId(userDto.id)
                   
                };
                _tokenRepo.DeleteTokenByUserId(userDto.id);

                _tokenRepo.CreateToken(token);
            }
            return authenticationTokenModel;
        }

        public async Task<AuthenticationTokenModel> ValidateToken(string tokenValue)
        {
            TokenDto token = await _tokenRepo.GetTokenByValue(tokenValue);
            if (token != null)
            {
                var userModel = await _userService.GetUserById(token.user_id);
                var roles = await _userService.GetAllRolesByUserId(token.user_id);
                var authenticationTokenModel = new AuthenticationTokenModel()
                {
                    Token = token.value,
                    UserId = token.user_id,
                    Username = userModel.UserName,
                    UserFullName = string.Format("{0} {1}", userModel.FirstName, userModel.LastName),
                    Roles=roles
                  
                };
                return authenticationTokenModel;
            }
            else
            {
                var msg = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Unauthorized Access" };
                throw new HttpResponseException(msg);
            }
        }
        public void Delete(string value)
        {
            var token = _tokenRepo.GetTokenByValue(value);
            _tokenRepo.DeleteToken(value);
        }

    }
}
