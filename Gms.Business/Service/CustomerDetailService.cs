﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using Gms.DAL.Repo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gms.Business.Service
{
    public interface ICustomerDetailService
    {
        Task<List<CustomerDetails>> GetAllCustomer(PagedDatasource pagedDatasource);
        Task Save(CustomerDetails data);
        Task Delete(int id);
        Task<CustomerDetails> GetCustomerById(int id);
        Task Update(CustomerDetails data);
    }
    public class CustomerDetailService : ICustomerDetailService
    {
        private readonly ICustomerDetailRepo _customerDetailRepo;
        private IMapper _mapper;
        private IUtilityService _utilityService;
        private readonly ICurrentUser _currentUser;
        public CustomerDetailService(ICustomerDetailRepo customerDetailRepo, IMapper mapper, IUtilityService utilityService, ICurrentUser currentUser)
        {
            _customerDetailRepo = customerDetailRepo;
            _mapper = mapper;
            _utilityService = utilityService;
            _currentUser = currentUser;
        }
        public async Task<List<CustomerDetails>> GetAllCustomer(PagedDatasource pagedDatasource)
        {
            if (string.IsNullOrWhiteSpace(pagedDatasource.SORTBYCOLUMN))
            {
                pagedDatasource.SORTBYCOLUMN = " Id ";
            }
            else
            {
                pagedDatasource.SORTBYCOLUMN = _utilityService.ToDTOProperty<CustomerDetails, CustomerDetailsDto>(pagedDatasource.SORTBYCOLUMN);
            }
            List<CustomerDetails> data =  _mapper.Map<List<CustomerDetails>>(await _customerDetailRepo.GetAllCustomer(pagedDatasource));
            return data;
        }

        public async Task Save(CustomerDetails data)
        {
            data.CreatedBy = _currentUser.UserId;
            data.CreatedDate = System.DateTime.Now;
            data.UpdatedBy = _currentUser.UserId;
            data.UpdatedDate = System.DateTime.Now;
            CustomerDetailsDto customerDetailsDto = _mapper.Map<CustomerDetailsDto>(data);
            await _customerDetailRepo.Save(customerDetailsDto);
        }
        public async Task Delete(int id)
        {
            await _customerDetailRepo.Delete(id);
        }

        public async Task<CustomerDetails> GetCustomerById(int id)
        {
            return _mapper.Map<CustomerDetails>(await _customerDetailRepo.GetCustomerById(id));
        }

        public async Task Update(CustomerDetails data)
        {
            data.UpdatedBy = _currentUser.UserId;
            data.UpdatedDate = System.DateTime.Now;
           await _customerDetailRepo.Update(_mapper.Map<CustomerDetailsDto>(data));
        }
    }
}
