﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.DAL.DTO;
using Gms.DAL.Repo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gms.Business.Service
{
    public interface IVechileDetailService
    {
        Task<List<dynamic>> GetAllVechiles(PagedDatasource pagedDatasource);
        Task Save(VechileDetails data);
        Task Delete(int id);
        Task<VechileDetails> GetById(int id);
        Task Update(VechileDetails data);
    }
    public class VechileDetailService : IVechileDetailService
    {
        public readonly IVehicleDetailRepo _vehicleDetailRepo;
        private readonly IUtilityService _utilityService;
        private readonly IMapper _mapper;
        public VechileDetailService(IVehicleDetailRepo vehicleDetailRepo, IUtilityService utilityService, IMapper mapper )
        {
            _vehicleDetailRepo = vehicleDetailRepo;
            _utilityService = utilityService;
            _mapper = mapper;
        }
        public Task<List<dynamic>> GetAllVechiles(PagedDatasource pagedDatasource)
        {
            if (string.IsNullOrWhiteSpace(pagedDatasource.SORTBYCOLUMN))
            {
                pagedDatasource.SORTBYCOLUMN = " Id ";
            }
            //else
            //{
            //    pagedDatasource.SORTBYCOLUMN = _utilityService.ToDTOProperty<VechileDetails, VehicleDetailsDto>(pagedDatasource.SORTBYCOLUMN);
            //}
            return _vehicleDetailRepo.GetAllVechiles(pagedDatasource);
        }
        public async Task Save(VechileDetails data)
        {
            VehicleDetailsDto customerDetailsDto = _mapper.Map<VehicleDetailsDto>(data);
            await _vehicleDetailRepo.Save(customerDetailsDto);
        }
        public async Task Delete(int id)
        {
            await _vehicleDetailRepo.Delete(id);
        }

        public async Task<VechileDetails> GetById(int id)
        {
            return _mapper.Map<VechileDetails>(await _vehicleDetailRepo.GetById(id));
        }

        public async Task Update(VechileDetails data)
        {
          await _vehicleDetailRepo.Update(_mapper.Map<VehicleDetailsDto>(data));
        }
    }
}
