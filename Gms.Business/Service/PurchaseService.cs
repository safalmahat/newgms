﻿using AutoMapper;
using Gms.Business.Entity;
using Gms.Business.Entity.ViewModel;
using Gms.DAL.DTO;
using Gms.DAL.Repo;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gms.Business.Service
{
    public interface IPurchaseService
    {
        Task Save(PurchaseViewModel purchaseViewModel);
        Task<IEnumerable<dynamic>> Get(PagedDatasource pagedDataSource);
        Task<IEnumerable<dynamic>> GetPurchaseDetails(int purchaseId);
        Task<PurchaseViewModel> GetPurchase(int id);
        Task Update(PurchaseViewModel purchaseViewModel);
        Task<int> GeneratePurchaseNumber();
        Task Delete(int id);
    }
    public class PurchaseService : IPurchaseService
    {
        private IPurchaseRepo _purchaseRepo;
        private IPurchaseDetailRepo _purchaseDetailRepo;
        private IMapper _mapper;
        private IDatabaseManager _databaseManager;
        private ITransctionScopeFactory _transctionScopeFactory;
        private ICurrentUser _currentUser;
        private IUtilityService _utilityService;
        private IStockDetailRepo _stockDetailRepo;
        public PurchaseService(IPurchaseDetailRepo purchaseDetailRepo, IPurchaseRepo purchaseRepo, IMapper mapper,
            IDatabaseManager databaseManager, ITransctionScopeFactory transctionScopeFactory,
            ICurrentUser currentUser,
            IUtilityService utilityService,
            IStockDetailRepo stockDetailRepo
            )
        {
            _purchaseDetailRepo = purchaseDetailRepo;
            _purchaseRepo = purchaseRepo;
            _mapper = mapper;
            _databaseManager = databaseManager;
            _transctionScopeFactory = transctionScopeFactory;
            _currentUser = currentUser;
            _utilityService = utilityService;
            _stockDetailRepo = stockDetailRepo;
        }

        public async Task Save(PurchaseViewModel purchaseViewModel)
        {
            PurchaseDto purchaseDto = _mapper.Map<PurchaseDto>(purchaseViewModel.Purchase);
            List<PurchaseDetailDto> purchaseDetailDtos = _mapper.Map<List<PurchaseDetailDto>>(purchaseViewModel.PurchaseDetails);

            using (var transction = _transctionScopeFactory.CreatetransctionScope())
            {
                using (var connection = _databaseManager.CreateDbConnection())
                {
                    //save purchase and return id
                    purchaseDto.Created_On = System.DateTime.Now;
                    purchaseDto.Created_By= _currentUser.UserId;
                    int purchaseId = await _purchaseRepo.Save(purchaseDto, connection);
                    //save purchase details
                    foreach (var item in purchaseDetailDtos)
                    {
                        item.PurchaseId = purchaseId;
                        item.Created_By = _currentUser.UserId;
                        item.Created_On = System.DateTime.Now;
                        await _purchaseDetailRepo.Save(item, connection);


                        //update the stock
                        StockDetailDto stockDetailDto = await _stockDetailRepo.GetStockDetailByProductId(item.ProductId, connection);
                        if(stockDetailDto !=null)
                        {
                            StockDetailDto data = new StockDetailDto();

                            int existingStock = stockDetailDto.Quantity;
                            int newQuantity = existingStock + item.Quantity;
                            data.Quantity = newQuantity;
                            data.ProductId = item.ProductId;
                            data.Updated_By = _currentUser.UserId;
                            data.Updated_On = DateTime.Now;
                            await _stockDetailRepo.Update(data,connection);
                        }
                        else
                        {
                            StockDetailDto data = new StockDetailDto();
                            int newQuantity = item.Quantity;
                            data.Quantity = newQuantity;
                            data.ProductId = item.ProductId;
                            data.Created_By = _currentUser.UserId;
                            data.Created_On = DateTime.Now;
                            await _stockDetailRepo.Save(data,connection);
                        }
                    }
                }
                transction.Complete();
            }
            
        }
        public async Task<IEnumerable<dynamic>> Get(PagedDatasource pagedDataSource)
        {
        
            if (!string.IsNullOrEmpty(pagedDataSource.FILTERQUERY) && pagedDataSource.FILTERQUERY != "undefined")

                pagedDataSource.FILTERQUERY = " where " + pagedDataSource.FILTERQUERY;
            if (pagedDataSource.FILTERQUERY == "undefined")
                pagedDataSource.FILTERQUERY = string.Empty;
            if (string.IsNullOrWhiteSpace(pagedDataSource.SORTBYCOLUMN))
            {
                pagedDataSource.SORTBYCOLUMN = " Id ";
            }
            else
            {
                pagedDataSource.SORTBYCOLUMN = _utilityService.ToDTOProperty<PurchaseModel, PurchaseDto>(pagedDataSource.SORTBYCOLUMN);
            }
            return await _purchaseRepo.Get(pagedDataSource);
        }
        public async Task<IEnumerable<dynamic>> GetPurchaseDetails(int purchaseId)
        {
            return await _purchaseRepo.GetPurchaseDetails(purchaseId);
        }
        public async Task<PurchaseViewModel> GetPurchase(int id)
        {
            PurchaseViewModel viewModel = new PurchaseViewModel();
            PurchaseDto purchaseDto = await _purchaseRepo.GetPurchase(id);
            PurchaseModel purchaseModel = _mapper.Map<PurchaseModel>(purchaseDto);
            List<PurchaseDetailDto> purchaseDetailDtos = await _purchaseDetailRepo.GetPurchaseDetailsByPurchaseId(id);
            List<PurchaseDetailModel> purchaseDetailModels = _mapper.Map<List<PurchaseDetailModel>>(purchaseDetailDtos);
            viewModel.Purchase = purchaseModel;
            viewModel.PurchaseDetails = purchaseDetailModels;
            return viewModel;
        }

        public async Task Update(PurchaseViewModel purchaseViewModel)
        {
            PurchaseDto purchaseDto = _mapper.Map<PurchaseDto>(purchaseViewModel.Purchase);
            List<PurchaseDetailDto> purchaseDetailDtos = _mapper.Map<List<PurchaseDetailDto>>(purchaseViewModel.PurchaseDetails);

            using (var transction = _transctionScopeFactory.CreatetransctionScope())
            {
                using (var connection = _databaseManager.CreateDbConnection())
                {
                    purchaseDto.Updated_By = _currentUser.UserId;
                    purchaseDto.Updated_On = DateTime.Now;
                    await _purchaseRepo.Update(purchaseDto, connection);
                    //save purchase details
                    purchaseDetailDtos.ForEach(x => {
                        x.Created_By = _currentUser.UserId;
                        x.Created_On = System.DateTime.Now;
                    }
                     );
                    foreach (var item in purchaseDetailDtos)
                    {

                        if (item.Id > 0)
                        {
                            item.Updated_By = _currentUser.UserId;
                            item.Updated_On = System.DateTime.Now;
                            await _purchaseDetailRepo.Update(item, connection);
                        }
                        else
                        {
                            item.PurchaseId = (int)purchaseViewModel.Purchase.Id;
                            item.Created_On = System.DateTime.Now;
                            item.Created_By = _currentUser.UserId;
                            await _purchaseDetailRepo.Save(item, connection);
                        }
                        //update the stock
                        StockDetailDto stockDetailDto = await _stockDetailRepo.GetStockDetailByProductId(item.ProductId, connection);
                        if (stockDetailDto != null)
                        {
                            StockDetailDto data = new StockDetailDto();

                            int existingStock = stockDetailDto.Quantity;
                            int newQuantity = existingStock + item.Quantity;
                            data.Quantity = newQuantity;
                            data.ProductId = item.ProductId;
                            data.Updated_By = _currentUser.UserId;
                            data.Updated_On = DateTime.Now;
                            await _stockDetailRepo.Update(data, connection);
                        }
                        else
                        {
                            StockDetailDto data = new StockDetailDto();
                            int newQuantity = item.Quantity;
                            data.Quantity = newQuantity;
                            data.ProductId = item.ProductId;
                            data.Created_By = _currentUser.UserId;
                            data.Created_On = DateTime.Now;
                            await _stockDetailRepo.Save(data, connection);
                        }
                    }

                }
                transction.Complete();
            }
        }

        public async Task<int> GeneratePurchaseNumber()
        {
            var totalData = await _purchaseRepo.GetTotalPurchaseCount();
            return totalData + 1;
        }
        public async Task Delete(int id)
        {
            await _purchaseRepo.Delete(id);
        }
    }
}
