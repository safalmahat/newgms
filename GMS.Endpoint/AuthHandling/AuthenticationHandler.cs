﻿
using Gms.Business.Entity;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Net;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Vilk.ERP.UserManagement.Services;

namespace GMS.Endpoint.AuthHandling
{
    public class AuthenticationHandler : AuthenticationHandler<AuthenticationOptions>
    {
        const string TOKEN_PREFIX = "Bearer ";
        private ITokenService _tokenService;
        private ICurrentUser _currentUser;
        public AuthenticationHandler(IOptionsMonitor<AuthenticationOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            ITokenService tokenService,
            ICurrentUser currentUser
            ) : base(options, logger, encoder, clock)
        {
            _tokenService = tokenService;
            _currentUser = currentUser;
        }
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (Request.Path.Value.Equals(@"/token", StringComparison.OrdinalIgnoreCase))
            {
                return AuthenticateResult.NoResult();

            }
            string token = string.Empty;
            string authorizationHeader = Request.Headers[HttpRequestHeader.Authorization.ToString()];
            if (!string.IsNullOrEmpty(authorizationHeader))
            {
                if (authorizationHeader.StartsWith(TOKEN_PREFIX) &&
                    authorizationHeader.Length > TOKEN_PREFIX.Length)
                {
                    token = authorizationHeader.Substring(TOKEN_PREFIX.Length).Trim();
                }
            }

            if (string.IsNullOrEmpty(token))
            {
                return AuthenticateResult.Fail("");
            }

            AuthenticationTokenModel authenticationTokenModel = null;
            try
            {
                authenticationTokenModel = await _tokenService.ValidateToken(token);
            }
            catch
            {
                return AuthenticateResult.Fail("");
            }

            _currentUser.UserId = authenticationTokenModel.UserId;
            _currentUser.ClientTypeId = authenticationTokenModel.ClientTypeId;
            _currentUser.ClientId = authenticationTokenModel.ClientId;
            _currentUser.Token = authenticationTokenModel.Token;
            _currentUser.Permissions = authenticationTokenModel.Roles;
            _currentUser.UserFullName = authenticationTokenModel.UserFullName;
            _currentUser.RoleId = authenticationTokenModel.RoleId;
            _currentUser.IsSuperUser = authenticationTokenModel.IsSuperUser;
            _currentUser.Username = authenticationTokenModel.Username;
            _currentUser.CommisionPercentage = authenticationTokenModel.CommisionPercentage;
            _currentUser.ClientName = authenticationTokenModel.ClientName;
            _currentUser.ClientAddress = authenticationTokenModel.ClientAddress;


            Context.User = new ClaimsPrincipal();
            var claimIdentity = new ClaimsIdentity("Vilk.Authentication");
            if (authenticationTokenModel.Roles != null)
            {
                foreach (var role in authenticationTokenModel.Roles)
                {
                    claimIdentity.AddClaim(new Claim(ClaimsIdentity.DefaultRoleClaimType, role));
                }
            }
            Context.User.AddIdentity(claimIdentity);
            var ticket = new AuthenticationTicket(Context.User, Scheme.Name);
            return AuthenticateResult.Success(ticket);
        }
    }
}
