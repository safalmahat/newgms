using Gms.Business;
using Gms.DAL;
using Gms.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using Newtonsoft;
using Newtonsoft.Json.Converters;
using GMS.Endpoint.AuthHandling;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using GMS.Endpoint.ErrorHandling;
using Microsoft.AspNetCore.Http;
using Unity;
using System.Runtime.Loader;
using System.Reflection;

namespace GMS.Endpoint
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(c =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                c.Filters.Add(new AuthorizeFilter(policy));
            }).AddNewtonsoftJson(options =>
    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
).
        AddControllersAsServices();


            //services.AddControllersWithViews();
            //services.AddControllers().AddNewtonsoftJson((option) => option.SerializerSettings.Converters.Add(new StringEnumConverter()));

            services.AddAuthentication("GMS.Authentication")
               .AddScheme<AuthenticationOptions, AuthenticationHandler>("GMS.Authentication", null);

            services.AddAuthorization();
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            Unity.Microsoft.DependencyInjection.ServiceProvider serviceProvider =
                    Unity.Microsoft.DependencyInjection.ServiceProvider.ConfigureServices(services)
                    as Unity.Microsoft.DependencyInjection.ServiceProvider;

            var container = (UnityContainer)serviceProvider;
            ConfigureContainer(container);
            services.AddTransient<IMiddlewareFactory>(_ => { return new GMS.Endpoint.Helper.MiddlewareFactory(container); });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";
                //  spa.Options.StartupTimeout = TimeSpan.FromSeconds(1000);
                if (env.IsDevelopment())
                {
                    //Configure the timeout to 5 minutes to avoid "The Angular CLI process did not start listening for requests within the timeout period of 50 seconds." issue
                    spa.UseAngularCliServer(npmScript: "start");

                }
            });
        }
        public void ConfigureContainer(IUnityContainer container)
        {
            container
                .RegisterSingleton<IErrorHandlingMiddleware, ErrorHandlingMiddleware>()

                .RegisterSingleton<IExceptionToErrorConversionHandler, ExceptionToErrorConversionHandler>()
                .RegisterSingleton<ICustomExceptionToErrorConverter, SimpleExceptionToErrorConverter>(typeof(SimpleExceptionToErrorConverter).Name)
                .RegisterSingleton<ICustomExceptionToErrorConverter, ValidationExceptionToErrorConverter>(typeof(ValidationExceptionToErrorConverter).Name);

            BusinessDependencyRegistration.Configure(container);
            DatabaseDependencyRegistration.Configure(container, Configuration);
            DalDependencyRegistration.Configure(container);
        }
    }
    internal class CustomAssemblyLoadContext : AssemblyLoadContext
    {
        public IntPtr LoadUnmanagedLibrary(string absolutePath)
        {
            return LoadUnmanagedDll(absolutePath);
        }

        protected override IntPtr LoadUnmanagedDll(String unmanagedDllName)
        {
            return LoadUnmanagedDllFromPath(unmanagedDllName);
        }

        protected override Assembly Load(AssemblyName assemblyName)
        {
            throw new NotImplementedException();
        }
    }

}

