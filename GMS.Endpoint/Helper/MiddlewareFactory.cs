﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unity;

namespace GMS.Endpoint.Helper
{
    public class MiddlewareFactory : IMiddlewareFactory
    {
        IUnityContainer _container;
        public MiddlewareFactory(IUnityContainer container)
        {
            _container = container;
        }

        public IMiddleware Create(Type middlewareType)
        {
            return _container.Resolve(middlewareType) as IMiddleware;
        }

        public void Release(IMiddleware middleware)
        {

        }
    }
}
