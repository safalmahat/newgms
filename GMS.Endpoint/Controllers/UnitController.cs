﻿using Gms.Business.Entity;
using Gms.Business.Service;
using Gms.DAL.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMS.Endpoint.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class UnitController : ControllerBase
    {
        private IUnitService _unitService;

        public UnitController(IUnitService unitService)
        {
            _unitService = unitService;
        }

        public async Task<IActionResult> GetAllUnit([FromQuery] PagedDatasource pagedDataSource)
        {
            var unitModels = await _unitService.GetUnits(pagedDataSource);
            return Ok(unitModels);
        }
        [HttpPost]
        public async Task<IActionResult> Post(UnitModel data)
        {
            await _unitService.Save(data);
            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _unitService.Delete(id);
            return Ok();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            UnitModel unit = await _unitService.Get(id);
            return Ok(unit);
        }
        [HttpPut]
        public async Task<IActionResult> Update(UnitModel data)
        {
            await _unitService.Update(data);
            return Ok();
        }

    }
}
