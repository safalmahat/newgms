﻿using Gms.Business.Entity;
using Gms.Business.Service;
using Gms.DAL.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vilk.ERP.UserManagement.Services;

namespace GMS.Endpoint.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
       
        private IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] PagedDatasource pagedDatasource)
        {
            return Ok(await _userService.GetUsers(pagedDatasource));
        }
        [HttpPost]
        public async Task<IActionResult> Post(User data)
        {
            await _userService.SaveUser(data);
            return Ok();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            return Ok(await _userService.GetUserById(id));
        }
        [HttpPut]
        public async Task<IActionResult> Update(User data)
        {
            await _userService.UpdateUserProfile(data);
            return Ok();
        }
        [HttpPost("updatePassword")]
        public async Task<IActionResult> UpdatePassword(UpdatePasswordModel data)
        {
            await _userService.UpdatePassword(data);
            return Ok();
        }
        [HttpGet("roles")]
        public async Task<IActionResult> GetRoles()
        {
            return Ok(await _userService.GetRoles());
        }
    }

}
