﻿using Gms.Business.Entity;
using Gms.Business.Service;
using Gms.DAL.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMS.Endpoint.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerDetailController : ControllerBase
    {
        private readonly ICustomerDetailService _customerDetailService;
        public CustomerDetailController(ICustomerDetailService customerDetailService)
        {
            _customerDetailService = customerDetailService;
        }
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] PagedDatasource pagedDatasource)
        {
            return Ok(await _customerDetailService.GetAllCustomer(pagedDatasource));
        }
        [HttpPost]
        public async Task<IActionResult> Post(CustomerDetails data)
        {
           await _customerDetailService.Save(data);
            return Ok();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _customerDetailService.GetCustomerById(id));
        }
        [HttpPut]
        public async Task<IActionResult> Update(CustomerDetails data)
        {
            await _customerDetailService.Update(data);
            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _customerDetailService.Delete(id);
            return Ok();
        }
    }

}
