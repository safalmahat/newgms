﻿using Gms.Business.Entity;
using Gms.Business.Entity.ViewModel;
using Gms.Business.Service;
using Gms.DAL.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMS.Endpoint.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MasterDataController : ControllerBase
    {
        private readonly IMasterDataService _masterDataService;
        public MasterDataController(IMasterDataService masterDataService)
        {
            _masterDataService = masterDataService;
        }
        [HttpGet("{category}")]
        public async Task<IActionResult> Get(string category)
        {
            return Ok(await _masterDataService.GetMasterDataByCategory(category));
        }
      
    }

}
