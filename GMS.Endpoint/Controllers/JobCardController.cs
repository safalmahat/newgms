﻿using Gms.Business.Entity;
using Gms.Business.Entity.ViewModel;
using Gms.Business.Service;
using Gms.DAL.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMS.Endpoint.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class JobCardController : ControllerBase
    {
        private readonly IJobCardService _jobCardService;
        private readonly IMasterDataService _masterDataService;
        public JobCardController(IJobCardService jobCardService, IMasterDataService masterDataService)
        {
            _jobCardService = jobCardService;
            _masterDataService = masterDataService;
        }
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] PagedDatasource pagedDatasource)
        {
            return Ok(await _jobCardService.GetAllJob(pagedDatasource));
        }
        [HttpGet("{id}/details")]
        public async Task<IActionResult> GetJobCardDetails(int id)
        {
            return Ok(await _jobCardService.GetJobCardDetailsByJobId(id));
        }
        [HttpPost]
        public async Task<IActionResult> Post(JobCardViewModel data)
        {
            await _jobCardService.Insert(data);
            return Ok();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetJobCard(int id)
        {
            return Ok(await _jobCardService.GetJobCard(id));
        }
        [HttpPut]
        public async Task<IActionResult> Update(JobCardViewModel data)
        {
            await _jobCardService.Update(data);
            return Ok();
        }
        [HttpGet("generate/JobNumber")]
        public async Task<IActionResult> GenerateJobNumber()
        {
            
            return Ok(await _jobCardService.GenerateJobNumber());
        }
    }
}


