﻿using Gms.Business.Entity;
using Gms.Business.Service;
using Gms.DAL.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMS.Endpoint.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class ProductCategoryController : ControllerBase
    {
        private IProductCategoryService _productCategoryService;

        public ProductCategoryController(IProductCategoryService productCategoryService)
        {
            _productCategoryService = productCategoryService;
        }

        public async Task<IActionResult> GetAllUnit([FromQuery] PagedDatasource pagedDataSource)
        {
            var unitModels = await _productCategoryService.GetProductAllCategory(pagedDataSource);
            return Ok(unitModels);
        }
        [HttpPost]
        public async Task<IActionResult> Post(ProductCategoryModel data)
        {
            await _productCategoryService.Save(data);
            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _productCategoryService.Delete(id);
            return Ok();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            ProductCategoryModel unit = await _productCategoryService.Get(id);
            return Ok(unit);
        }
        [HttpPut]
        public async Task<IActionResult> Update(ProductCategoryModel data)
        {
            await _productCategoryService.Update(data);
            return Ok();
        }

    }
}
