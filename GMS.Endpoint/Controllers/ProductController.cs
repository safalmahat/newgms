﻿using Gms.Business.Entity;
using Gms.Business.Service;
using Gms.DAL.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMS.Endpoint.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class ProductController : ControllerBase
    {
        private IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        public async Task<IActionResult> GetAllUnit([FromQuery] PagedDatasource pagedDataSource)
        {
            var unitModels = await _productService.GetAllProduct(pagedDataSource);
            return Ok(unitModels);
        }
        [HttpPost]
        public async Task<IActionResult> Post(ProductModel data)
        {
            await _productService.Save(data);
            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _productService.Delete(id);
            return Ok();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            ProductModel unit = await _productService.Get(id);
            return Ok(unit);
        }
        [HttpPut]
        public async Task<IActionResult> Update(ProductModel data)
        {
            await _productService.Update(data);
            return Ok();
        }
        [HttpGet("available")]
        public async Task<IActionResult> GetAvailableProduct()
        {
            return Ok(await _productService.GetAvailableProduct());
        }

    }
}
