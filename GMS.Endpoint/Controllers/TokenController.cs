﻿using Gms.Business.Entity;
using Gms.Business.Service;
using Gms.DAL.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vilk.ERP.UserManagement.Services;

namespace GMS.Endpoint.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TokenController : ControllerBase
    {
       
        private ITokenService _tokenService;

        public TokenController(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }

        [HttpPost]
        [Microsoft.AspNetCore.Authorization.AllowAnonymous]
        public async Task<IActionResult> Post([FromBody] LoginModel value)
        {
            var authenticationTokenModel = await _tokenService.CreateToken(value);
            return Ok(authenticationTokenModel);
        }
        [HttpDelete("{token}")]
        public IActionResult Delete(string token)
        {
            _tokenService.Delete(token);
            return Ok();
        }
    }

}
