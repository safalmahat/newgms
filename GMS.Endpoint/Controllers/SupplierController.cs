﻿using Gms.Business.Entity;
using Gms.Business.Service;
using Gms.DAL.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMS.Endpoint.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class SupplierController : ControllerBase
    {
        private ISupplierService _supplierService;

        public SupplierController(ISupplierService supplierService)
        {
            _supplierService = supplierService;
        }

        [HttpGet()]
        public async Task<IActionResult> GetAll([FromQuery] PagedDatasource pagedDataSource)
        {
            var unitModels = await _supplierService.Get(pagedDataSource);
            return Ok(unitModels);
        }
        [HttpPost]
        public async Task<IActionResult> Post(SupplierModel data)
        {
            await _supplierService.Save(data);
            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _supplierService.Delete(id);
            return Ok();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            SupplierModel supplier = await _supplierService.Get(id);
            return Ok(supplier);
        }
        [HttpPut]
        public async Task<IActionResult> Update(SupplierModel data)
        {
            await _supplierService.Update(data);
            return Ok();
        }

    }
}
