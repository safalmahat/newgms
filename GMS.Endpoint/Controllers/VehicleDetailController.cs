﻿using Gms.Business.Entity;
using Gms.Business.Service;
using Gms.DAL.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMS.Endpoint.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VehicleDetailController : ControllerBase
    {
        private readonly IVechileDetailService _vechileDetailService;
        public VehicleDetailController(IVechileDetailService vechileDetailService)
        {
            _vechileDetailService = vechileDetailService;
        }
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] PagedDatasource pagedDatasource)
        {
            return Ok(await _vechileDetailService.GetAllVechiles(pagedDatasource));
        }
        [HttpPost]
        public async Task<IActionResult> Post(VechileDetails data)
        {
            data.CreatedDate = System.DateTime.Now;
            data.UpdatedDate = System.DateTime.Now;
            await _vechileDetailService.Save(data);
            return Ok();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _vechileDetailService.Delete(id);
            return Ok();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _vechileDetailService.GetById(id));
        }
        [HttpPut]
        public async Task<IActionResult> Update(VechileDetails data)
        {
            await _vechileDetailService.Update(data);
            return Ok();
        }
    }

}
