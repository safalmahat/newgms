﻿using Gms.Business.Entity;
using Gms.Business.Entity.ViewModel;
using Gms.Business.Service;
using Gms.DAL.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMS.Endpoint.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PurchaseController : ControllerBase
    {
        private readonly IPurchaseService _purchaseService;
        public PurchaseController(IPurchaseService purchaseService)
        {
            _purchaseService = purchaseService;
        }
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] PagedDatasource pagedDatasource)
        {
            return Ok(await _purchaseService.Get(pagedDatasource));
        }
        [HttpGet("{id}/details")]
        public async Task<IActionResult> GetPurchaseDetails(int id)
        {
            return Ok(await _purchaseService.GetPurchaseDetails(id));
        }
        [HttpPost]
        public async Task<IActionResult> Post(PurchaseViewModel data)
        {
            await _purchaseService.Save(data);
            return Ok();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPurchase(int id)
        {
            return Ok(await _purchaseService.GetPurchase(id));
        }
        [HttpPut]
        public async Task<IActionResult> Update(PurchaseViewModel data)
        {
            await _purchaseService.Update(data);
            return Ok();
        }
        [HttpGet("generate/purchaseNumber")]
        public async Task<IActionResult> GeneratePurchaseNumber()
        {
            
            return Ok(await _purchaseService.GeneratePurchaseNumber());
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _purchaseService.Delete(id);
            return Ok();
        }
    }
}


