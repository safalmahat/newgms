import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { CustomerComponent } from './customer/customer.component';
import { AddCustomerComponent } from './customer/add-customer/add-customer.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { LoginComponent } from './login/login.component';
import { JobCardComponent } from './job-card/job-card.component';
import { AddJobCardComponent } from './job-card/add-job-card/add-job-card.component';
import { InlineAddCustomerComponent } from './customer/inline-add-customer/inline-add-customer.component';
import { InlineAddVehicleComponent } from './vehicle/inline-add-vehicle/inline-add-vehicle.component';
import { InlineAddJobComponent } from './job-card/inline-add-job/inline-add-job.component';
import { AuthGuard } from '../@core/guards/auth-guards';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'user',
      loadChildren: () => import('./user-management/user-management.module')
        .then(m => m.UserManagementModule),
        canActivate: [AuthGuard],
    },
    {
      path: 'inventory',
      loadChildren: () => import('./inventory-management/intentory-management.module')
        .then(m => m.IntentoryManagementModule),
        canActivate: [AuthGuard],
    },
    {
      path: 'dashboard',
      component: ECommerceComponent,
    },
    {
      path: 'customer-list',
      component: CustomerComponent,
    },
    {
      path: 'add-customer',
      component: InlineAddCustomerComponent,
    },
    {
      path: 'add-vehicle',
      component: InlineAddVehicleComponent,
    },
    {
      path: 'vehicle-list',
      component: VehicleComponent,
    },
    {
      path: 'add-job',
      component: InlineAddJobComponent,
    },
    {
      path: 'job-list',
      component: JobCardComponent,
    },

    {
      path: 'iot-dashboard',
      component: DashboardComponent,
    },
    {
      path: 'layout',
      loadChildren: () => import('./layout/layout.module')
        .then(m => m.LayoutModule),
    },
    {
      path: 'forms',
      loadChildren: () => import('./forms/forms.module')
        .then(m => m.FormsModule),
    },
    {
      path: 'ui-features',
      loadChildren: () => import('./ui-features/ui-features.module')
        .then(m => m.UiFeaturesModule),
    },
    {
      path: 'modal-overlays',
      loadChildren: () => import('./modal-overlays/modal-overlays.module')
        .then(m => m.ModalOverlaysModule),
    },
    {
      path: 'extra-components',
      loadChildren: () => import('./extra-components/extra-components.module')
        .then(m => m.ExtraComponentsModule),
    },
    {
      path: 'maps',
      loadChildren: () => import('./maps/maps.module')
        .then(m => m.MapsModule),
    },
    {
      path: 'charts',
      loadChildren: () => import('./charts/charts.module')
        .then(m => m.ChartsModule),
    },
    {
      path: 'editors',
      loadChildren: () => import('./editors/editors.module')
        .then(m => m.EditorsModule),
    },
    {
      path: 'tables',
      loadChildren: () => import('./tables/tables.module')
        .then(m => m.TablesModule),
    },
    {
      path: 'miscellaneous',
      loadChildren: () => import('./miscellaneous/miscellaneous.module')
        .then(m => m.MiscellaneousModule),
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
