import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataPager } from '../../model/data-paget';
import { Roles } from '../../model/role';
import { Sorting } from '../../model/sorting-model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }
  save(data)
  {
    return this.httpClient.post("User",data);
  }
  update(data)
  {
    return this.httpClient.put("User",data);
  }
  updatePassword(data)
  {
    return this.httpClient.post("User/updatePassword",data);
  }
  getUser({
    dataPager,
    sortQuery,
  }: {
    dataPager: DataPager;
    sortQuery: Sorting;

  }): Observable<any[]> {
    return this.httpClient.get<any[]>(
      "User",
      {
        params: {
          PageNumber: dataPager.pageNumber.toString(),
          PageSize: dataPager.pageSize.toString(),
          SortByOrder: sortQuery.sortOrder.toString(),
          SortByColumn: sortQuery.columnName,
          IsReqired: dataPager.isRequired.toString()
        },
      }
    );
  }
  delete(id) {
    return this.httpClient.delete('User');
  }
  getUserById(id)
  {
    return this.httpClient.get<any>(`user/${id}`);
  }
  deleteToken() {
    let tokenValue = JSON.parse(sessionStorage.currentUser).token;
   return this.httpClient.delete('Token/' + `${tokenValue}`);
  }
  getAllRoles()
{
  return this.httpClient.get<Roles[]>('user/roles');
}
}
