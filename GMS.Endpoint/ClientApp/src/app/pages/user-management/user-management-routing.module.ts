import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { UserListComponent } from './component/user-list/user-list.component';
import { AddUserComponent } from './component/user-list/add-user/add-user.component';
import { InlineAddUserComponent } from './component/inline-add-user/inline-add-user.component';
import { UserComponent } from './user.component';

const routes: Routes = [{
    path: '',
    component: UserComponent,
    children: [
        {
          path: 'add-user',
          component:InlineAddUserComponent
        },
        {
            path: 'user-list',
            component:UserListComponent
          },
    ]
},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserManagementRoutingModule {
}
