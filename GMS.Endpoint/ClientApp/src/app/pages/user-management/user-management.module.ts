import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserListComponent } from './component/user-list/user-list.component';
import { UserManagementRoutingModule } from './user-management-routing.module';
import { TableModule } from 'primeng/table';
import { AddUserComponent } from './component/user-list/add-user/add-user.component';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { AccordionModule } from 'primeng/accordion';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DropdownModule } from 'primeng/dropdown';
import { PanelModule } from 'primeng/panel';
import { CalendarModule } from 'primeng/calendar';
import { TabViewModule } from 'primeng/tabview';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { InlineAddUserComponent } from './component/inline-add-user/inline-add-user.component';
import { UserComponent } from './user.component';
import { MultiSelectModule } from 'primeng/multiselect';



@NgModule({
  declarations: [UserComponent,UserListComponent, AddUserComponent, InlineAddUserComponent],
  imports: [
    CommonModule,
    UserManagementRoutingModule,
    TableModule,
    DialogModule,
    ButtonModule,
    AccordionModule,
    TableModule,
    DialogModule,
    InputTextModule,
    ReactiveFormsModule,
    ConfirmDialogModule,
    DropdownModule,
    FormsModule,
    PanelModule,
    CalendarModule,
    TabViewModule,
    InputTextareaModule,
    AutoCompleteModule,
    CheckboxModule,
    UserManagementRoutingModule,
    MultiSelectModule
  ]
})
export class UserManagementModule { }
