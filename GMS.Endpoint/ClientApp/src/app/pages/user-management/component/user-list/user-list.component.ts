import { Component, ElementRef, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Confirmation, ConfirmationService } from 'primeng/api';
import { ColumnSortOrderType } from '../../../model/custom-sort-order-type';
import { DataPager } from '../../../model/data-paget';
import { Sorting } from '../../../model/sorting-model';
import { DataPagerService } from '../../../services/data-pager.service';
import { UserService } from '../../services/user.service';
import { AddUserComponent } from './add-user/add-user.component';

@Component({
  selector: 'ngx-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  tableScrollHeight: string = '';
  @ViewChild("elRef", { static: true }) elRef: ElementRef;
  @ViewChild('addNewSection', { read: ElementRef, static: true }) addNewSection: ElementRef;
  @ViewChild('mainContent', { read: ElementRef, static: true }) mainContent: ElementRef;
  gridColumns: any[];
  userList: any[];
  first = 0;
  recordsPerPage = 10;
  totalRecords = 0;
  totalGridRecords = 0;
  rowsPerPageOptions = [];
  selectedRowItem: any;
  currentPageNumber: number;
  pager: DataPager;
  sortColumn: Sorting;

  display: boolean = false;
  @ViewChild("userAddForm", { static: true })
  userAddForm: AddUserComponent;
  constructor(private userService:UserService, private dataPagerService:DataPagerService,
    private confirmationService:ConfirmationService
    ) { }

  ngOnInit(): void { this.gridColumns = [
    { field: 'firstName', header: 'First Name' },
    { field: 'lastName', header: 'Last Name' },
    { field: 'userName', header: 'User Name' },
    { field: 'email', header: 'Email' },
  ];
  this.sortColumn = {
    columnName: "",
    sortOrder: ColumnSortOrderType.Ascending,
  };
  this.loadDataLazy({
    first: this.first,
    rows: this.recordsPerPage,
  });

}
loadDataLazy(event: any): void {
  this.recordsPerPage = event.rows;
  this.first = event.first;
  this.currentPageNumber = this.dataPagerService.getCurrentPage(
    event.first,
    event.rows
  );
  this.getUser();

}
loadPageData(event: any): void {
  this.first = event.first;
  this.recordsPerPage = event.rows;
}
onPage(event) {
  this.currentPageNumber = this.dataPagerService.getCurrentPage(
    event.first,
    event.rows
  );
  this.first = event.first;
  this.recordsPerPage = event.rows;
  this.loadDataLazy({
    first: this.first,
    rows: this.recordsPerPage,
  });
  return;
}
onRowClick(order) {
  this.selectedRowItem = order;
}
getUser() {
  this.pager = {
    pageNumber: this.currentPageNumber,
    pageSize: this.recordsPerPage,
    recordsCount: 0,
    isRequired:true,
  };
  this.userService.getUser({ dataPager: this.pager, sortQuery: this.sortColumn }).subscribe((resp) => {
    this.userList = resp;
    if (resp.length > 0) {
      this.totalGridRecords = resp[0]["rowCount"];
    }
    else
      this.totalGridRecords = 0;
  });
}
customSort(event) {
  this.sortColumn = {
    columnName: event.field,
    sortOrder: 0
  }
  if (event.order == -1 || event.order == 0) {
    this.sortColumn.sortOrder = 1;
  }
  else
    this.sortColumn.sortOrder = 2;
  this.first = (this.currentPageNumber - 1) * this.recordsPerPage;
  this.loadDataLazy({
    first: this.first,
    rows: this.recordsPerPage,
  });
};
onAddClick() {
  this.userAddForm.userFormGroup.reset();
  this.display = true;

}
onAddFormClose(event) {
  this.display = false;
  this.loadDataLazy({
    first: this.first,
    rows: this.recordsPerPage,
  });
}
onEditClick(id) {
  this.display = true;
 // this.userAddForm.(id,"edit");
}
onViewClick(id) {
  this.display = true;
  //this.vehicleAddForm.getCustomerById(id,"view");
}
onDeleteClick(id) {
  this.confirmationService.confirm({
    message: 'Are you sure that you want to delete this record?',
    accept: () => {
      this.userService.delete(id).subscribe(resp => {
        this.loadDataLazy({
          first: this.first,
          rows: this.recordsPerPage,
        });
      })
      //Actual logic to perform a confirmation
    }
  });
}
setScrollHeight() {
 // this.tableScrollHeight = `${(this.mainContent.nativeElement.scrollHeight - this.addNewSection.nativeElement.scrollHeight - 150)}`;
   this.tableScrollHeight = `${this.elRef.nativeElement.offsetHeight -
     (this.elRef.nativeElement.offsetTop - 30)
     }`;
}
ngAfterViewInit(): void {
  setTimeout(() => {
    this.setScrollHeight();
  }, 10);
}
ngOnChanges(changes: SimpleChanges): void {
  if (changes['tableScrollHeight']) {
    this.setScrollHeight();

  }
}

}
