import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { Roles } from '../../../../model/role';
import { UserService } from '../../../services/user.service';
@Component({
  selector: 'ngx-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class AddUserComponent implements OnInit {
  userFormGroup: FormGroup;
  submitButtonText: string = "Save";
  isLoading: boolean = false;
  isSubmitted: boolean = false;
  @Output() displayChange = new EventEmitter();
  @Output() onSaveChange = new EventEmitter();
  isPasswordMatch: boolean = false;
  userNameAlreadyExist: string = "";
  roleListItem: Roles[];
  constructor(private fb: FormBuilder, private userService: UserService) { }

  ngOnInit(): void {
    this.userFormGroup = this.fb.group({
     Id: '',
      FirstName: ['', [Validators.required]],
      LastName: ['', [Validators.required]],
      UserName: ['', [Validators.required]],
      Hash: ['', [Validators.required]],
      ReHash: ['', [Validators.required]],
      Email: ['', [Validators.email]],
      Roles:['',[Validators.required]]
    });
    this.checkPasswordMatch();
    this.loadUserRoleDropdown();
  }
  loadUserRoleDropdown() {
    this.userService.getAllRoles().subscribe(resp => {
      this.roleListItem = resp;
    })
  }
  onCancelClick() {
    this.displayChange.emit(true);
  }
  checkPasswordMatch() {
    let actualPswd = this.userFormGroup.get('Hash').value;
    let matchPswd = this.userFormGroup.get('ReHash').value;
    if (actualPswd == matchPswd)
      this.isPasswordMatch = true;
    else
      this.isPasswordMatch = false;
  }
  onSaveClick() {
    debugger;
    this.isSubmitted = true;
    if (this.userFormGroup.invalid) {
      return;
    }
    if (!this.isPasswordMatch) {
      return;
    }
    this.userService
      .save(this.userFormGroup.value)
      .subscribe(
        (resp) => {
          this.displayChange.emit(true);
          this.onSaveChange.emit(true);
        },
        (err: HttpErrorResponse) => {
          if (err.error.length === 1 && err.status === 400) {
            this.userNameAlreadyExist = err.error[0].errorMessage;
          }
        }
      );
  }

}
