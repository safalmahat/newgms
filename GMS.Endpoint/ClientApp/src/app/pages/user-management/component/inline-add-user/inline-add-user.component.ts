import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-inline-add-user',
  templateUrl: './inline-add-user.component.html',
  styleUrls: ['./inline-add-user.component.scss']
})
export class InlineAddUserComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  onAddSaveChange(event)
  {
    this.router.navigate(['../pages/user/add-user']);
  }
}
