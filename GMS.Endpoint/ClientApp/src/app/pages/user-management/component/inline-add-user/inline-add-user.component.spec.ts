import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InlineAddUserComponent } from './inline-add-user.component';

describe('InlineAddUserComponent', () => {
  let component: InlineAddUserComponent;
  let fixture: ComponentFixture<InlineAddUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InlineAddUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineAddUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
