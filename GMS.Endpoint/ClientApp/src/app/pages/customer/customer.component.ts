import { AfterViewInit, Component, ElementRef, OnInit, SimpleChanges, ViewChild, ViewEncapsulation } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { ColumnSortOrderType } from '../model/custom-sort-order-type';
import { DataPager } from '../model/data-paget';
import { Sorting } from '../model/sorting-model';
import { CustomerService } from '../services/customer.service';
import { DataPagerService } from '../services/data-pager.service';
import { AddCustomerComponent } from './add-customer/add-customer.component';

@Component({
  selector: 'ngx-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CustomerComponent implements OnInit,AfterViewInit {
  tableScrollHeight: string = '';
  @ViewChild("elRef", { static: true }) elRef: ElementRef;
  @ViewChild('addNewSection', { read: ElementRef, static: true }) addNewSection: ElementRef;
  @ViewChild('mainContent', { read: ElementRef, static: true }) mainContent: ElementRef;
  gridColumns: any[];
  customerList: any[];
  first = 0;
  recordsPerPage = 10;
  totalRecords = 0;
  totalGridRecords = 0;
  rowsPerPageOptions = [];
  selectedRowItem: any;
  currentPageNumber: number;
  pager: DataPager;
  sortColumn: Sorting;

  display: boolean = false;
  @ViewChild("customerAddForm", { static: true })
  vehicleAddForm: AddCustomerComponent;
  constructor(private customerService: CustomerService, private dataPagerService: DataPagerService,
    private confirmationService: ConfirmationService
  ) {
    this.rowsPerPageOptions = [12, 20, 30];
  }

  ngOnInit(): void {
    this.gridColumns = [
      { field: 'fullName', header: 'Full Name' },
      { field: 'phoneNumber', header: 'Phone Number' },
      { field: 'mobileNumber', header: 'Mobile Number' },
      { field: 'email', header: 'Email' },
      { field: 'address', header: 'Address' },

    ];
    this.sortColumn = {
      columnName: "",
      sortOrder: ColumnSortOrderType.Ascending,
    };
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });

  }
  loadDataLazy(event: any): void {
    this.recordsPerPage = event.rows;
    this.first = event.first;
    this.currentPageNumber = this.dataPagerService.getCurrentPage(
      event.first,
      event.rows
    );
    this.getCustomer();

  }
  loadPageData(event: any): void {
    this.first = event.first;
    this.recordsPerPage = event.rows;
  }
  onPage(event) {
    this.currentPageNumber = this.dataPagerService.getCurrentPage(
      event.first,
      event.rows
    );
    this.first = event.first;
    this.recordsPerPage = event.rows;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
    return;
  }
  onRowClick(order) {
    this.selectedRowItem = order;
  }
  getCustomer() {
    this.pager = {
      pageNumber: this.currentPageNumber,
      pageSize: this.recordsPerPage,
      recordsCount: 0,
      isRequired:true,
    };
    this.customerService.getAllCustomer({ dataPager: this.pager, sortQuery: this.sortColumn }).subscribe((resp) => {
      this.customerList = resp;
      if (resp.length > 0) {
        this.totalGridRecords = resp[0]["rowCount"];
      }
      else
        this.totalGridRecords = 0;
    });
  }
  customSort(event) {
    debugger;
    this.sortColumn = {
      columnName: event.field,
      sortOrder: 0
    }
    if (event.order == -1 || event.order == 0) {
      this.sortColumn.sortOrder = 1;
    }
    else
      this.sortColumn.sortOrder = 2;
    this.first = (this.currentPageNumber - 1) * this.recordsPerPage;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  };
  onAddClick() {
    this.vehicleAddForm.customerFormGroup.reset();
    this.display = true;

  }
  onAddFormClose(event) {
    this.display = false;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  }
  onEditClick(id) {
    this.display = true;
    this.vehicleAddForm.getCustomerById(id,"edit");
  }
  onViewClick(id) {
    this.display = true;
    this.vehicleAddForm.getCustomerById(id,"view");
  }
  onDeleteClick(id) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete this record?',
      accept: () => {
        this.customerService.delete(id).subscribe(resp => {
          this.loadDataLazy({
            first: this.first,
            rows: this.recordsPerPage,
          });
        })
        //Actual logic to perform a confirmation
      }
    });
  }
  setScrollHeight() {
   // this.tableScrollHeight = `${(this.mainContent.nativeElement.scrollHeight - this.addNewSection.nativeElement.scrollHeight - 150)}`;
     this.tableScrollHeight = `${this.elRef.nativeElement.offsetHeight -
       (this.elRef.nativeElement.offsetTop - 30)
       }`;
  }
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.setScrollHeight();
    }, 10);
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['tableScrollHeight']) {
      this.setScrollHeight();

    }
  }
}
