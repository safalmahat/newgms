import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerService } from '../../services/customer.service';

@Component({
  selector: 'ngx-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddCustomerComponent implements OnInit {
  @Output() displayChange = new EventEmitter();
  @Output() onSaveChange = new EventEmitter();
  customerFormGroup: FormGroup;
  isSubmitted: boolean = false;
  disableSaveBtn: boolean = false;
  submitButtonText: string = "Save";
  isLoading: boolean = false;
  constructor(private fb: FormBuilder, private customerService: CustomerService) {
    this.customerFormGroup = this.fb.group({
      Id: '',
      FullName: ['', [Validators.required]],
      MobileNumber: '',
      PhoneNumber: '',
      Email: ['', [Validators.email]],
      Address: ''
    })

  }

  ngOnInit(): void {
    this.customerFormGroup.reset();
  }
  onSaveClick() {
    this.isLoading = true;
    this.submitButtonText = "Processing";
    this.isSubmitted = true;
    if (!this.customerFormGroup.valid) {
      this.isLoading = false;
      this.submitButtonText = "Save";
      return;
    }

    this.customerService.save(this.customerFormGroup.value).subscribe(resp => {
      this.displayChange.emit(false);
      this.onSaveChange.emit(true);
      this.isLoading = false;
      this.submitButtonText = "Save";
    })
  }
  onCancelClick() {
    this.displayChange.emit(false);
    this.onSaveChange.emit(true);
  }
  getCustomerById(id, action) {
    this.customerService.getCustomerById(id).subscribe(resp => {
      this.customerFormGroup.patchValue({
        Id: resp.id,
        FullName: resp.fullName,
        MobileNumber: resp.mobileNumber,
        PhoneNumber: resp.phoneNumber,
        Email: resp.email,
        Address: resp.address
      });
    })
    if (action == "view") {
      this.customerFormGroup.get("Id").disable();
      this.customerFormGroup.get("FullName").disable();
      this.customerFormGroup.get("MobileNumber").disable();
      this.customerFormGroup.get("PhoneNumber").disable();
      this.customerFormGroup.get("Email").disable();
      this.customerFormGroup.get("Address").disable();
      this.disableSaveBtn = true;
    }
    else {
      this.customerFormGroup.get("Id").enable();
      this.customerFormGroup.get("FullName").enable();
      this.customerFormGroup.get("MobileNumber").enable();
      this.customerFormGroup.get("PhoneNumber").enable();
      this.customerFormGroup.get("Email").enable();
      this.customerFormGroup.get("Address").enable();
      this.disableSaveBtn = true;
    }
  }
}
