import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-inline-add-customer',
  templateUrl: './inline-add-customer.component.html',
  styleUrls: ['./inline-add-customer.component.scss']
})
export class InlineAddCustomerComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  onAddSaveChange(event)
  {
    //this.router.navigate(['/customer-list']);
    this.router.navigate(['../pages/customer-list']);
  }
}
