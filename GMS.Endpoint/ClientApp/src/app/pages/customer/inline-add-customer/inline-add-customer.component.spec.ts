import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InlineAddCustomerComponent } from './inline-add-customer.component';

describe('InlineAddCustomerComponent', () => {
  let component: InlineAddCustomerComponent;
  let fixture: ComponentFixture<InlineAddCustomerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InlineAddCustomerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineAddCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
