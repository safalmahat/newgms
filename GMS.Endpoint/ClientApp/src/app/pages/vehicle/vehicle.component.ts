import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { ColumnSortOrderType } from '../model/custom-sort-order-type';
import { DataPager } from '../model/data-paget';
import { Sorting } from '../model/sorting-model';
import { DataPagerService } from '../services/data-pager.service';
import { VehicleService } from '../services/vehicle.service';
import { AddVehicleComponent } from './add-vehicle/add-vehicle.component';

@Component({
  selector: 'ngx-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class VehicleComponent implements OnInit {
  tableScrollHeight: string = '';
  gridColumns: any[];
  vehicleList: any[];
  first = 0;
  recordsPerPage = 25;
  totalRecords = 0;
  totalGridRecords = 0;
  rowsPerPageOptions = [];
  selectedRowItem: any;
  currentPageNumber: number;
  pager: DataPager;
  sortColumn: Sorting;

  display: boolean = false;
  @ViewChild("vehicleAddForm", { static: true })
  vehicleAddForm: AddVehicleComponent;
  constructor(private vehicleService: VehicleService, private dataPagerService: DataPagerService,
    private confirmationService: ConfirmationService
  ) {
    this.rowsPerPageOptions = [12, 20, 30];
  }
  ngOnInit(): void {
    this.gridColumns = [
      { field: 'Customer_Name', header: 'Customer Name' },
      { field: 'Registration_Number', header: 'Registration Number' },
      { field: 'Manufacturer', header: 'Manufacturer' },
      { field: 'Mfd_Year', header: 'Mfd Year' },
    ];
    this.sortColumn = {
      columnName: "",
      sortOrder: ColumnSortOrderType.Ascending,
    };
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });

  }
  loadDataLazy(event: any): void {
    this.recordsPerPage = event.rows;
    this.first = event.first;
    this.currentPageNumber = this.dataPagerService.getCurrentPage(
      event.first,
      event.rows
    );
    this.getVehicle();

  }
  loadPageData(event: any): void {
    this.first = event.first;
    this.recordsPerPage = event.rows;
  }
  onPage(event) {
    this.currentPageNumber = this.dataPagerService.getCurrentPage(
      event.first,
      event.rows
    );
    this.first = event.first;
    this.recordsPerPage = event.rows;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
    return;
  }
  onRowClick(order) {
    this.selectedRowItem = order;
  }
  getVehicle() {
    this.pager = {
      pageNumber: this.currentPageNumber,
      pageSize: this.recordsPerPage,
      recordsCount: 0,
      isRequired:true,
    };
    this.vehicleService.getAllVehicle({ dataPager: this.pager, sortQuery: this.sortColumn }).subscribe((resp) => {
      this.vehicleList = resp;
      if (resp.length > 0) {
        this.totalGridRecords = resp[0]["ROW_COUNT"];
      }
      else
        this.totalGridRecords = 0;
    });
  }
  customSort(event) {
    this.sortColumn = {
      columnName: event.field,
      sortOrder: 0
    }
    if (event.order == -1 || event.order == 0) {
      this.sortColumn.sortOrder = 1;
    }
    else
      this.sortColumn.sortOrder = 2;
    this.first = (this.currentPageNumber - 1) * this.recordsPerPage;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  };
  onAddClick() {
    this.vehicleAddForm.vehicleFormGroup.reset();
    this.display = true;

  }
  onAddFormClose(event) {
    this.display = false;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  }
  onEditClick(id) {
    this.display = true;
    this.vehicleAddForm.getVechicleById(id,"edit");
  }
  onViewClick(id) {
    this.display = true;
    this.vehicleAddForm.getVechicleById(id,"view");
  }
  onDeleteClick(id) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete this record?',
      accept: () => {
        this.vehicleService.delete(id).subscribe(resp => {
          this.loadDataLazy({
            first: this.first,
            rows: this.recordsPerPage,
          });
        })
        //Actual logic to perform a confirmation
      }
    });
  }


}
