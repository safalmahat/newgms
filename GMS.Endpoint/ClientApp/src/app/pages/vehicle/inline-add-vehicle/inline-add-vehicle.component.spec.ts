import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InlineAddVehicleComponent } from './inline-add-vehicle.component';

describe('InlineAddVehicleComponent', () => {
  let component: InlineAddVehicleComponent;
  let fixture: ComponentFixture<InlineAddVehicleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InlineAddVehicleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineAddVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
