import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-inline-add-vehicle',
  templateUrl: './inline-add-vehicle.component.html',
  styleUrls: ['./inline-add-vehicle.component.scss']
})
export class InlineAddVehicleComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  onSaveChange(event)
  {
    this.router.navigate(['../pages/vehicle-list']);
  }
}
