import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { truncate } from 'igniteui-angular-core';
import { SelectItem } from 'primeng/api';
import { ColumnSortOrderType } from '../../model/custom-sort-order-type';
import { DataPager } from '../../model/data-paget';
import { Sorting } from '../../model/sorting-model';
import { CustomerService } from '../../services/customer.service';
import { VehicleService } from '../../services/vehicle.service';

@Component({
  selector: 'ngx-add-vehicle',
  templateUrl: './add-vehicle.component.html',
  styleUrls: ['./add-vehicle.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddVehicleComponent implements OnInit {

  @Output() displayChange = new EventEmitter();
  @Output() onSaveChange = new EventEmitter();
  vehicleFormGroup: FormGroup;
  isSubmitted: boolean = false;
  customerItem: any[];
  showCustomerNotFoundMessage: boolean = false;
  disableSaveBtn: boolean = false;
  transmissionTypeItem: SelectItem[];
  isLoading = false;
  submitButtonText = "Save";
  constructor(private fb: FormBuilder, private vehicleService: VehicleService, private customerService: CustomerService) {
    this.vehicleFormGroup = this.fb.group({
      Id: "",
      RegistrationNumber: ['', [Validators.required]],
      Manufacturer: '',
      Model: '',
      Variant: ['', []],
      Options: '',
      MfdYear: '',
      TransmitionType: '',
      FuelType: '',
      BodyType: '',
      CustomerId: '',
      CustomerName: '',
    })
    this.loadCustomerDropdown();
    this.transmissionTypeItem = [];
    this.loadTransmissionType();
  }
  loadCustomerDropdown() {
    let dtPager = new DataPager();
    dtPager.pageNumber = 1,
      dtPager.pageSize = 1,
      dtPager.recordsCount = 0,
      dtPager.isRequired = false;
    let sortColumn = new Sorting();
    sortColumn.columnName = "";
    sortColumn.sortOrder = ColumnSortOrderType.Ascending;

    this.customerService.getAllCustomer({ dataPager: dtPager, sortQuery: sortColumn }).subscribe(res => {
      this.customerItem = res;
    });
  }
  loadTransmissionType() {
    this.vehicleService.getTransmitionType().subscribe(res => {
      if (res.length > 0) {
        this.transmissionTypeItem = res.map((r) => {
          return {
            label: r.name,
            value: r.value
          }
        })
      }
    })
  }

  ngOnInit(): void {
  }
  onSaveClick() {
    this.isLoading=true;
    this.submitButtonText="Processing";
    this.isSubmitted = true;
    this.showCustomerNotFoundMsg();
    if (!this.vehicleFormGroup.valid)
    {
    this.isLoading=false;
    this.submitButtonText="Save";
      return;
    }
    if(this.vehicleFormGroup.get("CustomerId").value=="" || this.vehicleFormGroup.get("CustomerId").value==null)
    {
      this.showCustomerNotFoundMessage = true;
      this.isLoading=false;
      this.submitButtonText="Save";
        return;
    }
      
    this.vehicleService.save(this.vehicleFormGroup.value).subscribe(resp => {
      this.displayChange.emit(false);
      this.onSaveChange.emit(true);
      this.isLoading=false;
      this.submitButtonText="Save";
    })
  }
  onCancelClick() {
    this.displayChange.emit(false);
  }
  getVechicleById(id, action) {
    this.vehicleService.get(id).subscribe(resp => {
      this.vehicleFormGroup.patchValue({
        Id: resp.id,
        RegistrationNumber: resp.registrationNumber,
        Manufacturer: resp.manufacturer,
        Model: resp.model,
        Variant: resp.variant,
        Options: resp.options,
        MfdYear: resp.mfdYear,
        TransmitionType: resp.transmitionType,
        FuelType: resp.fuelType,
        BodyType: resp.bodyType,
        CustomerId: resp.customerId
      });
      //set customername
      var customer = this.customerItem.filter(data => {
        return data.id == resp.customerId
      })
      this.vehicleFormGroup.get('CustomerName').setValue(customer[0]);
    })
    if (action == "view") {
      this.vehicleFormGroup.get("Id").disable();
      this.vehicleFormGroup.get("RegistrationNumber").disable();
      this.vehicleFormGroup.get("Manufacturer").disable();
      this.vehicleFormGroup.get("Model").disable();
      this.vehicleFormGroup.get("Variant").disable();
      this.vehicleFormGroup.get("Options").disable();
      this.vehicleFormGroup.get("MfdYear").disable();
      this.vehicleFormGroup.get("TransmitionType").disable();
      this.vehicleFormGroup.get("FuelType").disable();
      this.vehicleFormGroup.get("BodyType").disable();
      this.vehicleFormGroup.get("CustomerId").disable();
      this.vehicleFormGroup.get("CustomerName").disable();
      this.disableSaveBtn = true;
    }
    else {
      this.vehicleFormGroup.get("Id").enable();
      this.vehicleFormGroup.get("RegistrationNumber").enable();
      this.vehicleFormGroup.get("Manufacturer").enable();
      this.vehicleFormGroup.get("Model").enable();
      this.vehicleFormGroup.get("Variant").enable();
      this.vehicleFormGroup.get("Options").enable();
      this.vehicleFormGroup.get("MfdYear").enable();
      this.vehicleFormGroup.get("TransmitionType").enable();
      this.vehicleFormGroup.get("FuelType").enable();
      this.vehicleFormGroup.get("BodyType").enable();
      this.vehicleFormGroup.get("CustomerId").enable();
      this.vehicleFormGroup.get("CustomerName").enable();
      this.disableSaveBtn = false;
    }

  }

  search(event) {
    let query = event.query;
    let dtPager = new DataPager();
    dtPager.pageNumber = 1,
      dtPager.pageSize = 1,
      dtPager.recordsCount = 0,
      dtPager.isRequired = false;
    let sortColumn = new Sorting();
    sortColumn.columnName = "";
    sortColumn.sortOrder = ColumnSortOrderType.Ascending;

    this.customerService.getAllCustomer({ dataPager: dtPager, sortQuery: sortColumn }).subscribe(res => {
      this.customerItem = this.filterCustomer(query, res);
      this.vehicleFormGroup.get("CustomerId").setValue("");
    });
  }
  filterCustomer(query, countries: any[]): any[] {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered: any[] = [];
    for (let i = 0; i < countries.length; i++) {
      let country = countries[i];
      if (country.fullName.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(country);
      }
    }
    return filtered;
  }
  onCustomerNameSelect(customer) {
    this.vehicleFormGroup.get("CustomerId").setValue(customer.id);
  }
  showCustomerNotFoundMsg() {
    if (this.vehicleFormGroup.get("CustomerId").value == "") {
      this.showCustomerNotFoundMessage = true;
    }
    else
      this.showCustomerNotFoundMessage = false;
  }
}
