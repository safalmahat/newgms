import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouteStateService } from '../services/route-state.service';
import { SessionService } from '../services/session.service';
import { UserContextService } from '../services/user-context.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userName: string;

  password: string;

  locale: string;

  version: string;

  msgs: any[];

  loginErrorMsg: string = "";
  constructor(
    private router: Router,
    private userService: UserService,
    private routeStateService: RouteStateService,
    private sessionService: SessionService,
    private userContextService: UserContextService,
  ) { }

  ngOnInit() {
    debugger;
    this.userName = "";
    this.password = "";
    this.locale = this.sessionService.getItem("ng-prime-language");
    this.msgs = [
      { severity: "info", detail: "UserName: admin" },
      { severity: "info", detail: "Password: password" },
    ];
  }

  onClickLogin() {
    this.userService
      .getUserByUserNameAndPassword(this.userName, this.password)
      .subscribe(
        (resp) => {
          if (resp) {
            debugger;
            this.userContextService.setUser(resp);
            //get user setting

            this.routeStateService.add(
              "Dashboard",
              "/pages/dashboard",
              null,
              true
            );
          }
        },
        (err: HttpErrorResponse) => {
          if (err.error.length === 1 && err.status === 400) {
            this.loginErrorMsg = err.error[0].errorMessage;
          }
        }
      );
  }
  onPasswordKeyUp(event) {
    if (event.keyCode === 13) {
      // Cancel the default action, if needed
      event.preventDefault();
      this.onClickLogin();
    }
  }
  onLanguageChange($event) {
    this.locale = $event.target.value;
    if (
      this.locale == undefined ||
      this.locale == null ||
      this.locale.length == 0
    ) {
      this.locale = "en";
    }

    this.sessionService.setItem("ng-prime-language", this.locale);
  }

  onForgetPasswordClick() {
    this.router.navigate(["/forget-password"]);
  }
}
