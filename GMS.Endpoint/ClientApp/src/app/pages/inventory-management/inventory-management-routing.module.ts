import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { InventoryComponent } from './inventory.component';
import { SupplierListComponent } from './components/supplier-list/supplier-list.component';
import { InlineAddSupplierComponent } from './components/supplier-list/inline-add-supplier/inline-add-supplier.component';
import { UnitComponent } from './components/unit/unit.component';
import { ProductCategoryComponent } from './components/product-category/product-category.component';
import { ProductComponent } from './components/product/product.component';
import { PurchaseComponent } from './components/purchase/purchase.component';
import { InlineAddPurchaseComponent } from './components/purchase/inline-add-purchase/inline-add-purchase.component';
import { InlineAddProductComponent } from './components/product/inline-add-product/inline-add-product.component';

const routes: Routes = [{
    path: '',
    component: InventoryComponent,
    children: [
        {
          path: 'add-supplier',
         component:InlineAddSupplierComponent
        },
        {
            path: 'supplier-list',
            component:SupplierListComponent
           
        },
        {
            path: 'unit-list',
            component:UnitComponent
        },
        {
            path: 'category-list',
            component:ProductCategoryComponent
        },
        {
            path: 'product-add',
            component:InlineAddProductComponent
        },
        {
            path: 'product-list',
            component:ProductComponent
        },
        {
            path: 'purchase-add',
            component:InlineAddPurchaseComponent
        },
        {
            path: 'purchase-list',
            component:PurchaseComponent
        },
    ]
},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class InventoryManagementRoutingModule {
}
