import { Component, ElementRef, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { ColumnSortOrderType } from '../../../model/custom-sort-order-type';
import { DataPager } from '../../../model/data-paget';
import { Sorting } from '../../../model/sorting-model';
import { DataPagerService } from '../../../services/data-pager.service';
import { Purchase } from '../../model/purchase';
import { PurchaseService } from '../../services/purchase.service';
import { AddPurchaseComponent } from './add-purchase/add-purchase.component';

@Component({
  selector: 'ngx-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.scss']
})
export class PurchaseComponent implements OnInit {
  tableScrollHeight: string = '';
  @ViewChild("elRef", { static: true }) elRef: ElementRef;
  @ViewChild('addNewSection', { read: ElementRef, static: true }) addNewSection: ElementRef;
  @ViewChild('mainContent', { read: ElementRef, static: true }) mainContent: ElementRef;
  gridColumns: any[];
  purchaseList: any[];
  first = 0;
  recordsPerPage = 25;
  totalRecords = 0;
  totalGridRecords = 0;
  rowsPerPageOptions = [];
  selectedRowItem: any;
  currentPageNumber: number;
  pager: DataPager;
  sortColumn: Sorting;

  display: boolean = false;
  @ViewChild("addPurchaseComponent", { static: true })
  addPurchaseComponent: AddPurchaseComponent;

  jobDetailList: any[];
  constructor(private purchaseService: PurchaseService, private dataPagerService: DataPagerService,
    private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.gridColumns = [
      { field: 'PurchaseDate', header: 'Purchase Date' },
      { field: 'PurchaseNo', header: 'Number' },
      { field: 'supplierName', header: 'Supplier Name' },
      { field: 'TotalBillAmount', header: 'Bill Amount' },
      { field: 'TotalPaidAmount', header: 'Paid Amount' },
      { field: 'TotalDueAmount', header: 'Due Amount' }
    ];
    this.sortColumn = {
      columnName: "",
      sortOrder: ColumnSortOrderType.Ascending,
    };
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });

  }
  loadDataLazy(event: any): void {
    this.recordsPerPage = event.rows;
    this.first = event.first;
    this.currentPageNumber = this.dataPagerService.getCurrentPage(
      event.first,
      event.rows
    );
    this.getJob();

  }
  loadPageData(event: any): void {
    this.first = event.first;
    this.recordsPerPage = event.rows;
  }
  onPage(event) {
    this.currentPageNumber = this.dataPagerService.getCurrentPage(
      event.first,
      event.rows
    );
    this.first = event.first;
    this.recordsPerPage = event.rows;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
    return;
  }
  getJob() {
    this.pager = {
      pageNumber: this.currentPageNumber,
      pageSize: this.recordsPerPage,
      recordsCount: 0,
      isRequired:true,
    };
    this.purchaseService.getAll({ dataPager: this.pager, sortQuery: this.sortColumn }).subscribe((resp) => {
      this.purchaseList = resp;
      if (resp.length > 0) {
        this.totalGridRecords = resp[0]["ROW_COUNT"];
      }
      else
        this.totalGridRecords = 0;
    });
  }
  customSort(event) {
    this.sortColumn = {
      columnName: event.field,
      sortOrder: 0
    }
    if (event.order == -1 || event.order == 0) {
      this.sortColumn.sortOrder = 1;
    }
    else
      this.sortColumn.sortOrder = 2;
    this.first = (this.currentPageNumber - 1) * this.recordsPerPage;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  };
  onAddClick() {

    this.display = true;
    this.addPurchaseComponent.ngOnInit();

  }

  onEditClick(id) {
    this.display = true;
    this.addPurchaseComponent.getPurchase(id,'edit');
    this.addPurchaseComponent.isEditButtonClicked = false;
    this.addPurchaseComponent.isAddGoalsButtonClicked = false;
  }

  onViewClick(id) {
    this.display = true;
    this.addPurchaseComponent.getPurchase(id,'view');
    this.addPurchaseComponent.isEditButtonClicked = false;
    this.addPurchaseComponent.isAddGoalsButtonClicked = false;
  }
  onDeleteClick(id) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete this record?',
      accept: () => {
        this.purchaseService.delete(id).subscribe(resp => {
          this.loadDataLazy({
            first: this.first,
            rows: this.recordsPerPage,
          });
        })
        //Actual logic to perform a confirmation
      }
    });
  }
  setScrollHeight() {
    // this.tableScrollHeight = `${(this.mainContent.nativeElement.scrollHeight - this.addNewSection.nativeElement.scrollHeight - 150)}`;
    this.tableScrollHeight = `${this.elRef.nativeElement.offsetHeight -
      (this.elRef.nativeElement.offsetTop - 100)
      }`;
  }
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.setScrollHeight();
    }, 10);
  }
  onRowClick(job) {
    this.selectedRowItem = job;
  }
  onAddFormClose(event) {
    this.display = false;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['tableScrollHeight']) {
      this.setScrollHeight();

    }
  }

}
