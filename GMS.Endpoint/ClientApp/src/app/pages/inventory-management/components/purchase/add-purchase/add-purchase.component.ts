import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { Table } from 'primeng/table';
import { DataPager } from '../../../../model/data-paget';
import { Product } from '../../../model/product';
import { PurchaseDetail } from '../../../model/purchase-detail';
import { PurchaseViewModel } from '../../../model/purchase-view-model';
import { ProductService } from '../../../services/product.service';
import { PurchaseService } from '../../../services/purchase.service';
import { SupplierService } from '../../../services/supplier.service';
import { UnitService } from '../../../services/unit.service';

@Component({
  selector: 'ngx-add-purchase',
  templateUrl: './add-purchase.component.html',
  styleUrls: ['./add-purchase.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddPurchaseComponent implements OnInit {
  date: any;
  @Output() displayChange = new EventEmitter();
  @Output() onSaveChange = new EventEmitter();
  paymentMethod: SelectItem[];
  productList: Product[] = [];
  productListItem: SelectItem[];
  unitListItem: SelectItem[];
  supplierItem: SelectItem[];
  gridColumns: any;
  purchaseDetailList: PurchaseDetail[] = [];
  isEditButtonClicked: boolean = false;
  isAddGoalsButtonClicked: boolean = false;
  purchaseDetailForm: FormGroup;
  purchaseForm: FormGroup;
  isRowSavedClick: boolean = false;
  purchaseViewModel: PurchaseViewModel;
  isSubmitted: boolean = false;
  pager: DataPager;
  @ViewChild('purchaseDetailTable', { static: false }) purchaseDetailTable: Table;
  isLoading: boolean = false;
  submitButtonText: string = "Save";
  disableAddComplainBtn: boolean = false;
  disableSavebtn: boolean = false;
  IsEditRowbtnDisable: boolean = false;
  IsRemoveRowbtnDisable: boolean = false;
  constructor(private supplierService: SupplierService,
    private fb: FormBuilder,
    private productService: ProductService,
    private unitService: UnitService,
    private purchaseService: PurchaseService,
    private datePipe: DatePipe,
  ) {
    this.paymentMethod = [
      { label: 'Card', value: 'Card' },
      { label: 'Cash', value: 'Cash' },
      { label: 'Cheque', value: 'Cheque' },

    ];
    this.gridColumns = [
      { field: "productName", header: "Product" },
      { field: "unitName", header: "Unit" },
      { field: "quantity", header: "Quantity" },
      { field: "price", header: "Price" },
      { field: "totalPrice", header: "Total Price" },
    ];
    this.purchaseForm = this.fb.group({
      Id: 0,
      PurchaseDate: ['', [Validators.required]],
      PurchaseNo: ['', [Validators.required]],
      TotalBillAmount: '',
      TotalPaidAmount: '',
      TotalDueAmount: '',
      PaymentMethod: '',
      SupplierId: ['', [Validators.required]],
    });
    this.datePipe = new DatePipe('en-US');
    this.createPurchaseDetailForm();
    this.purchaseForm.setControl("purchaseDetail", this.purchaseDetailForm);
    this.purchaseForm.get('PaymentMethod').setValue('Cash');
    this.generatePurchaseNumber();
  }
  createPurchaseDetailForm() {
    this.purchaseDetailForm = this.fb.group({
      id: 0,
      productId: ['', [Validators.required]],
      unitId: ['', [Validators.required]],
      quantity: '',
      price: '',
      totalPrice: ''
    });

  }
  ngOnInit(): void {
    this.loadProductDropdown();
    this.loadUnitDropdown();
    this.loadSupplierDropdown();
  }
  onClose() {
    this.displayChange.emit(false);
  }
  loadSupplierDropdown() {
    let pager = {
      pageNumber: 1,
      pageSize: 1,
      recordsCount: 0,
      isRequired: false
    };
    let sortColumn = {
      columnName: "",
      sortOrder: 1
    }
    this.supplierService.getAll({ dataPager: pager, sortQuery: sortColumn }).subscribe(res => {
      if (res.length > 0) {
        this.supplierItem = res.map((r) => {
          return {
            label: r.name,
            value: r.id
          };
        });
      }
    });
  }
  loadProductDropdown() {
    let pager = {
      pageNumber: 1,
      pageSize: 1,
      recordsCount: 0,
      isRequired: false
    };
    let sortColumn = {
      columnName: "",
      sortOrder: 1
    }

    this.productService.getAll({ dataPager: pager, sortQuery: sortColumn }).subscribe(res => {
      if (res.length > 0) {
        this.productListItem = res.map((r) => {
          return {
            label: r.Name,
            value: {
              id: r.Id,
              name: r.Name
            }
          };
        });
      }
    });
  }
  loadUnitDropdown() {
    let pager = {
      pageNumber: 1,
      pageSize: 1,
      recordsCount: 0,
      isRequired: false
    };
    let sortColumn = {
      columnName: "",
      sortOrder: 1
    }

    this.unitService.getAll({ dataPager: pager, sortQuery: sortColumn }).subscribe(res => {
      if (res.length > 0) {
        this.unitListItem = res.map((r) => {
          return {
            label: r.name,
            value: {
              id: r.id,
              name: r.name
            }
          };
        });
      }
    });
  }
  addNewPurchaseDetail() {

  }
  getGridHeight(): any {
    return 'calc(97.3vh - 170px)';
  }
  onRowEditInit(purchaseDetail: PurchaseDetail) {
    this.isRowSavedClick = true;
    if (purchaseDetail.id > 0) {
      this.purchaseForm.get('purchaseDetail').get('price').setValue(purchaseDetail.price);
      this.purchaseForm.get('purchaseDetail').get('productId').setValue({ name: purchaseDetail.productName, id: purchaseDetail.productId });
      this.purchaseForm.get('purchaseDetail').get('unitId').setValue({ name: purchaseDetail.unitName, id: purchaseDetail.unitId });
      this.purchaseForm.get('purchaseDetail').get('quantity').setValue(purchaseDetail.quantity);
      this.purchaseForm.get('purchaseDetail').get('totalPrice').setValue(purchaseDetail.totalPrice);
    }
    this.purchaseDetailTable.initRowEdit(purchaseDetail);
  }

  onRowEditSave(purchaseDetailInfo: PurchaseDetail, i) {
    if (this.purchaseDetailForm.valid) {
      purchaseDetailInfo.id = purchaseDetailInfo.id,
        purchaseDetailInfo.productId = this.purchaseDetailForm.get('productId').value.id,
        purchaseDetailInfo.productName = this.purchaseDetailForm.get('productId').value.name,
        purchaseDetailInfo.unitId = this.purchaseDetailForm.get('unitId').value.id,
        purchaseDetailInfo.unitName = this.purchaseDetailForm.get('unitId').value.name,
        purchaseDetailInfo.price = this.purchaseDetailForm.get('price').value,
        purchaseDetailInfo.totalPrice = this.purchaseDetailForm.get('totalPrice').value,
        purchaseDetailInfo.quantity = this.purchaseDetailForm.get('quantity').value
      this.isAddGoalsButtonClicked = false;
      this.isEditButtonClicked = false;
      this.isRowSavedClick = false;

      let totalBillAmount = 0;
      this.purchaseDetailList.forEach(element => {
        totalBillAmount = totalBillAmount + element.totalPrice;
      });
      this.purchaseForm.get('TotalBillAmount').setValue(totalBillAmount);
    }
    this.onPaidAmountChange();
  }
  onProductDropdownValueChange() {

  }

  addNewRow() {
    if (!this.isAddGoalsButtonClicked) {
      this.isAddGoalsButtonClicked = true;
      let newPurchaseDetail = new PurchaseDetail();
      newPurchaseDetail.id = 0;
      newPurchaseDetail['rowIndex'] = this.purchaseDetailList.length + 1
      newPurchaseDetail.productId = 0;
      newPurchaseDetail.unitId = 0;
      newPurchaseDetail.quantity = 0;
      newPurchaseDetail.price = 0;
      newPurchaseDetail.totalPrice = 0
      this.purchaseDetailList.unshift(newPurchaseDetail);
      this.purchaseDetailTable.initRowEdit(newPurchaseDetail);
      this.purchaseDetailForm.reset();
    }
  }
  deleteGoal(purchaseDetail: PurchaseDetail) {
    const i = this.purchaseDetailList.indexOf(purchaseDetail, 0);
    if (i > -1) {
      this.purchaseDetailList.splice(i, 1);
    }
  }
  reset(purchaseDetail?: PurchaseDetail) {
    this.isRowSavedClick = false;
    if (purchaseDetail != null) {
      if (purchaseDetail.id == 0) {
        this.purchaseDetailList.shift();
      }
      else {
        this.purchaseDetailTable.cancelRowEdit(purchaseDetail);
      }
    }
    this.purchaseDetailForm.reset();
    this.isAddGoalsButtonClicked = false;
    this.isEditButtonClicked = false;
  }
  onCancelClick(purchaseDetail: PurchaseDetail, index) {
    this.reset(purchaseDetail);

  }
  onFormCancelClick() {
    this.displayChange.emit(true);
  }

  onSaveClick() {
    this.isLoading = true;
    this.submitButtonText = "Processing";
    this.isSubmitted = true;
    if (!this.purchaseDetailForm.valid) {
      this.isLoading = false;
      this.submitButtonText = "Save";
      return;
    }
    let purchase = this.purchaseForm.getRawValue();
    let formattedPurchaseDetails = [];
    this.purchaseDetailList.forEach(x => {
      let purchaseDetail = new PurchaseDetail();
      purchaseDetail.id = x.id;
      purchaseDetail.productId = x.productId;
      purchaseDetail.unitId = x.unitId;
      purchaseDetail.quantity = +x.quantity;
      purchaseDetail.price = +x.price;
      purchaseDetail.totalPrice = +x.totalPrice;
      formattedPurchaseDetails.push(purchaseDetail);
    })
    let finalData = {
      Purchase: {
        Id: purchase.Id == null ? 0 : purchase.Id,
        PurchaseDate: purchase.PurchaseDate,
        PurchaseNo: purchase.PurchaseNo,
        TotalBillAmount: +purchase.TotalBillAmount,
        TotalPaidAmount: +purchase.TotalPaidAmount,
        TotalDueAmount: +purchase.TotalDueAmount,
        PaymentMethod: purchase.PaymentMethod,
        SupplierId: purchase.SupplierId
      },
      PurchaseDetails: formattedPurchaseDetails
    }
    this.purchaseService.save(finalData).subscribe(res => {
      this.isLoading = true;
      this.submitButtonText = "Processing";
      this.displayChange.emit(false);
      this.onSaveChange.emit(true);
    })
  }
  getPurchase(id, action) {
    this.purchaseService.getPurchase(id).subscribe(resp => {
      this.purchaseViewModel = resp;
      //set the value to form and grid
      this.purchaseDetailList = this.purchaseViewModel.purchaseDetails;
      this.purchaseDetailList.forEach((element, index) => {
        element["rowIndex"] = index;
      });
      this.purchaseForm.patchValue({
        Id: this.purchaseViewModel.purchase.id,
        PurchaseDate: this.datePipe.transform(new Date(this.purchaseViewModel.purchase.purchaseDate), 'MM/dd/yyyy'),
        PurchaseNo: this.purchaseViewModel.purchase.purchaseNo,
        TotalBillAmount: this.purchaseViewModel.purchase.totalBillAmount,
        TotalPaidAmount: this.purchaseViewModel.purchase.totalPaidAmount,
        TotalDueAmount: this.purchaseViewModel.purchase.totalDueAmount,
        PaymentMethod: this.purchaseViewModel.purchase.paymentMethod,
        SupplierId: this.purchaseViewModel.purchase.supplierId
      });
      if (action == "view") {
        this.disableAddComplainBtn = true;
        this.disableSavebtn = true;
        this.IsEditRowbtnDisable = true;
        this.IsRemoveRowbtnDisable = true;
        this.purchaseForm.get('PurchaseDate').disable();
        this.purchaseForm.get('PurchaseNo').disable();
        this.purchaseForm.get('TotalBillAmount').disable();
        this.purchaseForm.get('TotalPaidAmount').disable();
        this.purchaseForm.get('TotalDueAmount').disable();
        this.purchaseForm.get('PaymentMethod').disable();
        this.purchaseForm.get('SupplierId').disable();
      }
      else {
        this.disableAddComplainBtn = false;
        this.disableSavebtn = false;
        this.IsEditRowbtnDisable = false;
        this.IsRemoveRowbtnDisable = false;
        this.purchaseForm.get('PurchaseDate').enable();
        this.purchaseForm.get('PurchaseNo').enable();
        this.purchaseForm.get('TotalBillAmount').enable();
        this.purchaseForm.get('TotalPaidAmount').enable();
        this.purchaseForm.get('TotalDueAmount').enable();
        this.purchaseForm.get('PaymentMethod').enable();
        this.purchaseForm.get('SupplierId').enable();
      }
    });
  }
  onChange() {
    let totalPrice = 0;
    let quantity = +this.purchaseDetailForm.get('quantity').value;
    let price = +this.purchaseDetailForm.get('price').value;
    totalPrice = quantity * price;
    this.purchaseDetailForm.get('totalPrice').setValue(totalPrice);
  }
  generatePurchaseNumber() {
    this.purchaseService.generatePurchaseNumber().subscribe(resp => {
      this.purchaseForm.get('PurchaseNo').setValue(resp);
    })
  }
  onPaidAmountChange() {
    let billAmount = +this.purchaseForm.get('TotalBillAmount').value;
    let paidAmount = +this.purchaseForm.get('TotalPaidAmount').value;
    let dueAmount = billAmount - paidAmount;
    this.purchaseForm.get('TotalDueAmount').setValue(dueAmount);
  }

}
