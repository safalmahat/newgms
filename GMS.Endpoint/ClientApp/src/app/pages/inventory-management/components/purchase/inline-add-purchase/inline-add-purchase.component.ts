import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-inline-add-purchase',
  templateUrl: './inline-add-purchase.component.html',
  styleUrls: ['./inline-add-purchase.component.scss']
})
export class InlineAddPurchaseComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  onAddSaveChange(event)
  {
    this.router.navigate(['../pages/inventory/purchase-list']);
  }
}
