import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InlineAddPurchaseComponent } from './inline-add-purchase.component';

describe('InlineAddPurchaseComponent', () => {
  let component: InlineAddPurchaseComponent;
  let fixture: ComponentFixture<InlineAddPurchaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InlineAddPurchaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineAddPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
