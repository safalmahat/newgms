import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductCategoryService } from '../../../services/product-category.service';

@Component({
  selector: 'ngx-add-product-category',
  templateUrl: './add-product-category.component.html',
  styleUrls: ['./add-product-category.component.scss']
})
export class AddProductCategoryComponent implements OnInit {
  @Output() displayChange = new EventEmitter();
  @Output() onSaveChange = new EventEmitter();
  productCategoryFormGroup: FormGroup;
  isSubmitted: boolean = false;
  disableSaveBtn: boolean = false;
  submitButtonText: string = "Save";
  isLoading: boolean = false;
  btnDisable:boolean=false;
  constructor(private fb:FormBuilder,private productCategoryService:ProductCategoryService) { }

  ngOnInit(): void {
    this.productCategoryFormGroup = this.fb.group({
      Id: '',
      Name: ['', [Validators.required]],
      Description:['', [Validators.required]],
    })
    this.productCategoryFormGroup.reset();
  }
  onSaveClick() {
    this.isLoading = true;
    this.submitButtonText = "Processing";
    this.isSubmitted = true;
    if (!this.productCategoryFormGroup.valid) {
      this.isLoading = false;
      this.submitButtonText = "Save";
      return;
    }

    this.productCategoryService.save(this.productCategoryFormGroup.value).subscribe(resp => {
      this.displayChange.emit(false);
      this.onSaveChange.emit(true);
      this.isLoading = false;
      this.submitButtonText = "Save";
    })
  }
  onCancelClick() {
    this.displayChange.emit(false);
    this.onSaveChange.emit(true);
  }
  getData(id,action)
  {
    this.productCategoryService.get(id).subscribe(resp => {
       this.productCategoryFormGroup.patchValue({
         Id:resp.id,
         Name:resp.name,
         Description:resp.description
       })
    })
    if(action=="view")
    {
      this.btnDisable=true;
      this.productCategoryFormGroup.get("Name").disable();
      this.productCategoryFormGroup.get("Description").disable();
    }
    else{
      this.btnDisable=false;
      this.productCategoryFormGroup.get("Name").enable();
      this.productCategoryFormGroup.get("Description").enable();
    }
  }
}
