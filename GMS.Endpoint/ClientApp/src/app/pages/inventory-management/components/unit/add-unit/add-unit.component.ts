import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UnitService } from '../../../services/unit.service';

@Component({
  selector: 'ngx-add-unit',
  templateUrl: './add-unit.component.html',
  styleUrls: ['./add-unit.component.scss']
})
export class AddUnitComponent implements OnInit {
  @Output() displayChange = new EventEmitter();
  @Output() onSaveChange = new EventEmitter();
  unitFormGroup: FormGroup;
  isSubmitted: boolean = false;
  disableSaveBtn: boolean = false;
  submitButtonText: string = "Save";
  isLoading: boolean = false;
  constructor(private fb:FormBuilder, private unitService:UnitService) { }


  ngOnInit(): void {
    this.unitFormGroup = this.fb.group({
      Id: '',
      Name: ['', [Validators.required]],
      Code:['', [Validators.required]],
    })
    this.unitFormGroup.reset();
  }
  onSaveClick() {
    this.isLoading = true;
    this.submitButtonText = "Processing";
    this.isSubmitted = true;
    if (!this.unitFormGroup.valid) {
      this.isLoading = false;
      this.submitButtonText = "Save";
      return;
    }

    this.unitService.save(this.unitFormGroup.value).subscribe(resp => {
      this.displayChange.emit(false);
      this.onSaveChange.emit(true);
      this.isLoading = false;
      this.submitButtonText = "Save";
    })
  }
  onCancelClick() {
    this.displayChange.emit(false);
    this.onSaveChange.emit(true);
  }
  getData(id,action)
  {
    this.unitService.get(id).subscribe(resp => {
       this.unitFormGroup.patchValue({
         Id:resp.id,
         Name:resp.name,
         Code:resp.code
       })
    })
    if(action=="view")
    {
      this.unitFormGroup.get("Name").disable();
      this.unitFormGroup.get("Code").disable();
    }
    else{
      this.unitFormGroup.get("Name").enable();
      this.unitFormGroup.get("Code").enable();
    }
  }
}
