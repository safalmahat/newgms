import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SupplierService } from '../../../services/supplier.service';

@Component({
  selector: 'ngx-add-supplier',
  templateUrl: './add-supplier.component.html',
  styleUrls: ['./add-supplier.component.scss']
})
export class AddSupplierComponent implements OnInit {
  @Output() displayChange = new EventEmitter();
  @Output() onSaveChange = new EventEmitter();
  supplierFormGroup: FormGroup;
  isSubmitted: boolean = false;
  disableSaveBtn: boolean = false;
  submitButtonText: string = "Save";
  isLoading: boolean = false;
  constructor(private fb:FormBuilder, private supplierService:SupplierService) { }

  ngOnInit(): void {
    this.supplierFormGroup = this.fb.group({
      Id: '',
      Name: ['', [Validators.required]],
      ContactPerson:'',
      MobileNumber: '',
      PhoneNumber: '',
      Email: ['', [Validators.email]],
      Address: ''
    })
    this.supplierFormGroup.reset();
  }
  onSaveClick() {
    this.isLoading = true;
    this.submitButtonText = "Processing";
    this.isSubmitted = true;
    if (!this.supplierFormGroup.valid) {
      this.isLoading = false;
      this.submitButtonText = "Save";
      return;
    }

    this.supplierService.save(this.supplierFormGroup.value).subscribe(resp => {
      this.displayChange.emit(false);
      this.onSaveChange.emit(true);
      this.isLoading = false;
      this.submitButtonText = "Save";
    })
  }
  onCancelClick() {
    this.displayChange.emit(false);
    this.onSaveChange.emit(true);
  }
  getData(id,action)
  {
    this.supplierService.get(id).subscribe(resp => {
       this.supplierFormGroup.patchValue({
         Id:resp.id,
         Name:resp.name,
         ContactPerson:resp.contactPerson,
         PhoneNumber:resp.phoneNumber,
         MobileNumber:resp.mobileNumber,
         Email:resp.email,
         Address:resp.address
       })
    })
    if(action=="view")
    {
      this.supplierFormGroup.get("Name").disable();
      this.supplierFormGroup.get("ContactPerson").disable();
      this.supplierFormGroup.get("PhoneNumber").disable();
      this.supplierFormGroup.get("MobileNumber").disable();
      this.supplierFormGroup.get("Email").disable();
      this.supplierFormGroup.get("Address").disable();
    }
    else{
      this.supplierFormGroup.get("Name").enable();
      this.supplierFormGroup.get("ContactPerson").enable();
      this.supplierFormGroup.get("PhoneNumber").enable();
      this.supplierFormGroup.get("MobileNumber").enable();
      this.supplierFormGroup.get("Email").enable();
      this.supplierFormGroup.get("Address").enable();
    }
  }
}
