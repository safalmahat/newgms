import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-inline-add-supplier',
  templateUrl: './inline-add-supplier.component.html',
  styleUrls: ['./inline-add-supplier.component.scss']
})
export class InlineAddSupplierComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  onAddSaveChange(event)
  {
    //this.router.navigate(['/customer-list']);
    this.router.navigate(['../pages/inventory/supplier-list']);
  }
}
