import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InlineAddSupplierComponent } from './inline-add-supplier.component';

describe('InlineAddSupplierComponent', () => {
  let component: InlineAddSupplierComponent;
  let fixture: ComponentFixture<InlineAddSupplierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InlineAddSupplierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineAddSupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
