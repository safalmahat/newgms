import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { ColumnSortOrderType } from '../../../model/custom-sort-order-type';
import { DataPager } from '../../../model/data-paget';
import { Sorting } from '../../../model/sorting-model';
import { DataPagerService } from '../../../services/data-pager.service';
import { Supplier } from '../../model/supplier';
import { SupplierService } from '../../services/supplier.service';
import { AddSupplierComponent } from './add-supplier/add-supplier.component';
@Component({
  selector: 'ngx-supplier-list',
  templateUrl: './supplier-list.component.html',
  styleUrls: ['./supplier-list.component.scss']
})
export class SupplierListComponent implements OnInit,AfterViewInit {
  supplierList: Supplier[] = [];
  columns: any[];
  tableScrollHeight: string;
  display: boolean = false;
  @ViewChild("elRef", { static: true }) elRef: ElementRef;
  @ViewChild("supplierAddForm", { static: true })
  supplierAddForm: AddSupplierComponent;
  selectedRowItem:any;

  recordsPerPage = 50;
  rowsPerPageOptions = [];
  currentPageNumber: number;
  pager: DataPager;
  first = 0;
  totalGridRecords: number;
  currentShowingRecords: string;
  showingText = 'Showing ';
  sortColumn: Sorting;
  displayFilterComponent: boolean = false;
  filterQuery: string = "";
  gridName: string = 'supplier_grid';
  
  constructor(private supplierService: SupplierService,
    private confirmationService:ConfirmationService,
    private dataPagerService: DataPagerService,
    ) {
    this.columns = [
      { field: "name", header: "Name" },
      { field: "address", header: "Address" },
      { field: "contactPerson", header: "Contact Person" },
      { field: "phoneNumber", header: "Phone Number" },
      { field: "email", header: "Email" },
    ];
    this.sortColumn = {
      columnName: "",
      sortOrder: ColumnSortOrderType.Ascending,
    };
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  }
  ngOnInit() {
    //this.loadData();
  }
  loadDataLazy(event: any): void {
    this.recordsPerPage = event.rows;
    this.first = event.first;
    this.currentPageNumber = this.dataPagerService.getCurrentPage(
      event.first,
      event.rows
    );
    this.getSupplier();
  
  }
  loadPageData(event: any): void {
    this.first = event.first;
    this.recordsPerPage = event.rows;
  }
  onPage(event) {
    this.currentPageNumber = this.dataPagerService.getCurrentPage(
      event.first,
      event.rows
    );
    this.first = event.first;
    this.recordsPerPage = event.rows;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
    return;
  }
  updateCurrentRecordStats() {
    if (this.totalGridRecords > 0) {
      this.currentShowingRecords = `${
        (this.currentPageNumber - 1) * this.recordsPerPage + 1
        } - ${
        this.recordsPerPage * this.currentPageNumber > this.totalGridRecords
          ? this.totalGridRecords
          : this.recordsPerPage * this.currentPageNumber
        }`;
    } else {
      this.currentShowingRecords = "0";
    }
  }

  onFilterQueryUpdate(event) {
    this.displayFilterComponent = false;
    this.filterQuery = event;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  }
  onAddClick()
  {
    this.display=true;
    this.supplierAddForm.ngOnInit();
  this.supplierAddForm.isSubmitted=false;
  
  }
  onEditClick(id)
  {
    this.display=true;
    this.supplierAddForm.getData(id,'edit');
    this.supplierAddForm.isSubmitted=false;
  }
  onViewClick(id)
  {
    this.display=true;
    this.supplierAddForm.getData(id,'view');
    this.supplierAddForm.isSubmitted=false;
  }
  onDeleteClick(id)
  {
    this.confirmationService.confirm({
      message: "Are you sure that you want to delete Unit?",
      accept: () => {
        this.supplierService.delete(id).subscribe((resp) => {
          this.loadDataLazy({
            first: this.first,
            rows: this.recordsPerPage,
          });
        });
      },
    });
  }
  onDialogClose(event) {
    this.display = event;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  }
  getSupplier() {
    this.pager = {
      pageNumber: this.currentPageNumber,
      pageSize: this.recordsPerPage,
      recordsCount: 0,
      isRequired:true,
    };
    this.supplierService.getAll({ dataPager: this.pager, sortQuery: this.sortColumn }).subscribe((resp) => {
      this.supplierList = resp;
      if (resp.length > 0) {
        this.totalGridRecords = resp[0]["rowCount"];
      }
      else
        this.totalGridRecords = 0;
    });
  }
  customSort(event) {
    this.sortColumn = {
      columnName: event.field,
      sortOrder: 0
    }
    if (event.order == -1 || event.order == 0) {
      this.sortColumn.sortOrder = 1;
    }
    else
      this.sortColumn.sortOrder = 2;
    this.first = (this.currentPageNumber - 1) * this.recordsPerPage;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  };
  setScrollHeight() {
    this.tableScrollHeight = `${
      this.elRef.nativeElement.offsetHeight - this.elRef.nativeElement.offsetTop
    }`;
  }
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.setScrollHeight();
    }, 10);
  }
  onRowClick(supplier)
  {
    this.selectedRowItem=supplier;
    
  }
}
