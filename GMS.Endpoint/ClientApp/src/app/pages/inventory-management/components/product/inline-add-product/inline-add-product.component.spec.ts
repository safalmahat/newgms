import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InlineAddProductComponent } from './inline-add-product.component';

describe('InlineAddProductComponent', () => {
  let component: InlineAddProductComponent;
  let fixture: ComponentFixture<InlineAddProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InlineAddProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineAddProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
