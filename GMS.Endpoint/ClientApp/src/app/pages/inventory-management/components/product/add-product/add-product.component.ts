import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { ProductCategoryService } from '../../../services/product-category.service';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'ngx-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  @Output() displayChange = new EventEmitter();
  @Output() onSaveChange = new EventEmitter();
  productFormGroup: FormGroup;
  isSubmitted: boolean = false;
  disableSaveBtn: boolean = false;
  submitButtonText: string = "Save";
  isLoading: boolean = false;
  btnDisable:boolean=false;
  productCategoryListItem:SelectItem[];
  constructor(private fb:FormBuilder, private productService:ProductService, private productCategory:ProductCategoryService) { }


  ngOnInit(): void {
    this.productFormGroup = this.fb.group({
      Id: '',
      CategoryId:['',[Validators.required]],
      ActualPrice:'',
      DiscountPercentage:'',
      Price:['',[Validators.required]],
      Name: ['', [Validators.required]],
      Code:'',
      Description:''
    })
    this.productFormGroup.reset();
    this.getAllCategory();
  }
  getAllCategory() {
    let pager = {
      pageNumber: 1,
      pageSize: 1,
      recordsCount: 0,
      isRequired: false
    };
    let sortColumn = {
      columnName: "",
      sortOrder: 1
    }
    this.productCategory.getAll({ dataPager: pager, sortQuery: sortColumn }).subscribe((resp) => {
      if (resp.length > 0) {
        this.productCategoryListItem = resp.map((r) => {
          return {
            label: r.name,
            value: r.id
          };
        });
      }
    });
  }
  onSaveClick() {
    this.isLoading = true;
    this.submitButtonText = "Processing";
    this.isSubmitted = true;
    if (!this.productFormGroup.valid) {
      this.isLoading = false;
      this.submitButtonText = "Save";
      return;
    }

    this.productService.save(this.productFormGroup.value).subscribe(resp => {
      this.displayChange.emit(false);
      this.onSaveChange.emit(true);
      this.isLoading = false;
      this.submitButtonText = "Save";
    })
  }
  onCancelClick() {
    this.displayChange.emit(false);
    this.onSaveChange.emit(true);
  }
  getData(id,action)
  {
    this.productService.get(id).subscribe(resp => {
       this.productFormGroup.patchValue({
         Id:resp.id,
         Name:resp.name,
         Code:resp.code,
         CategoryId:resp.categoryId,
         ActualPrice:resp.actualPrice,
         DiscountPercentage:resp.discountPercentage,
         Price:resp.price,
         Description:resp.description
       })
    })
    if(action=="view")
    {
      this.btnDisable=true;
      this.productFormGroup.get("Name").disable();
      this.productFormGroup.get("Code").disable();
      this.productFormGroup.get("CategoryId").disable();
      this.productFormGroup.get("ActualPrice").disable();
      this.productFormGroup.get("DiscountPercentage").disable();
      this.productFormGroup.get("Price").disable();
      this.productFormGroup.get("Description").disable();
    }
    else{
      this.btnDisable=false;
      this.productFormGroup.get("Name").enable();
      this.productFormGroup.get("Code").enable();
      this.productFormGroup.get("CategoryId").enable();
      this.productFormGroup.get("ActualPrice").enable();
      this.productFormGroup.get("DiscountPercentage").enable();
      this.productFormGroup.get("Price").enable();
      this.productFormGroup.get("Description").enable();
    }
  }
  onDiscountChange()
  {
    let actualPrice=+this.productFormGroup.get('ActualPrice').value;
    let discountPercentage=+this.productFormGroup.get('DiscountPercentage').value;
    let price=actualPrice- (actualPrice * discountPercentage)/100;
    this.productFormGroup.get('Price').setValue(Math.floor(price));
  }
}
