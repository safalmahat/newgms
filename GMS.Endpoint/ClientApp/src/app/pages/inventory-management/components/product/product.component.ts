import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { ColumnSortOrderType } from '../../../model/custom-sort-order-type';
import { DataPager } from '../../../model/data-paget';
import { Sorting } from '../../../model/sorting-model';
import { DataPagerService } from '../../../services/data-pager.service';
import { ProductService } from '../../services/product.service';
import { AddProductComponent } from './add-product/add-product.component';


@Component({
  selector: 'ngx-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  unitList: any[] = [];
  columns: any[];
  tableScrollHeight: string;
  display: boolean = false;
  @ViewChild("elRef", { static: true }) elRef: ElementRef;
  @ViewChild("addProductComponent", { static: true })
  addProductComponent: AddProductComponent;
  selectedRowItem: any;

  recordsPerPage = 50;
  rowsPerPageOptions = [];
  currentPageNumber: number;
  pager: DataPager;
  first = 0;
  totalGridRecords: number;
  currentShowingRecords: string;
  showingText = 'Showing ';
  sortColumn: Sorting;
  constructor(private dataPagerService:DataPagerService,private productService:ProductService,private confirmationService:ConfirmationService) { }


  ngOnInit(): void {
    this.columns = [
      { field: "category", header: "Product Category" },
      { field: "Name", header: "Name" },
      { field: "Code", header: "Code" },
      { field: "Price", header: "Price" },
      { field: "Status", header: "Status" },
    ];
    this.sortColumn = {
      columnName: "",
      sortOrder: ColumnSortOrderType.Ascending,
    };
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  }
  loadDataLazy(event: any): void {
    this.recordsPerPage = event.rows;
    this.first = event.first;
    this.currentPageNumber = this.dataPagerService.getCurrentPage(
      event.first,
      event.rows
    );
    this.getSupplier();
  
  }
  loadPageData(event: any): void {
    this.first = event.first;
    this.recordsPerPage = event.rows;
  }
  onPage(event) {
    this.currentPageNumber = this.dataPagerService.getCurrentPage(
      event.first,
      event.rows
    );
    this.first = event.first;
    this.recordsPerPage = event.rows;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
    return;
  }
  updateCurrentRecordStats() {
    if (this.totalGridRecords > 0) {
      this.currentShowingRecords = `${
        (this.currentPageNumber - 1) * this.recordsPerPage + 1
        } - ${
        this.recordsPerPage * this.currentPageNumber > this.totalGridRecords
          ? this.totalGridRecords
          : this.recordsPerPage * this.currentPageNumber
        }`;
    } else {
      this.currentShowingRecords = "0";
    }
  }
  onEditClick(id)
  {
    this.display=true;
    this.addProductComponent.getData(id,'edit');
    this.addProductComponent.isSubmitted=false;
  }
  onViewClick(id)
  {
    this.display=true;
    this.addProductComponent.getData(id,'view');
    this.addProductComponent.isSubmitted=false;
  }
  onDeleteClick(id)
  {
    this.confirmationService.confirm({
      message: "Are you sure that you want to delete Product?",
      accept: () => {
        this.productService.delete(id).subscribe((resp) => {
          this.loadDataLazy({
            first: this.first,
            rows: this.recordsPerPage,
          });
        });
      },
    });
  }
  onDialogClose(event) {
    this.display = event;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  }
  getSupplier() {
    this.pager = {
      pageNumber: this.currentPageNumber,
      pageSize: this.recordsPerPage,
      recordsCount: 0,
      isRequired:true,
    };
    this.productService.getAll({ dataPager: this.pager, sortQuery: this.sortColumn }).subscribe((resp) => {
      this.unitList = resp;
      if (resp.length > 0) {
        this.totalGridRecords = resp[0]["rowCount"];
      }
      else
        this.totalGridRecords = 0;
    });
  }
  customSort(event) {
    this.sortColumn = {
      columnName: event.field,
      sortOrder: 0
    }
    if (event.order == -1 || event.order == 0) {
      this.sortColumn.sortOrder = 1;
    }
    else
      this.sortColumn.sortOrder = 2;
    this.first = (this.currentPageNumber - 1) * this.recordsPerPage;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  };
  setScrollHeight() {
    this.tableScrollHeight = `${
      this.elRef.nativeElement.offsetHeight - this.elRef.nativeElement.offsetTop
    }`;
  }
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.setScrollHeight();
    }, 10);
  }
  onRowClick(supplier)
  {
    this.selectedRowItem=supplier;
    
  }
  onAddClick() {
    this.display = true;
    this.addProductComponent.ngOnInit();
    this.addProductComponent.isSubmitted = false;

  }
}
