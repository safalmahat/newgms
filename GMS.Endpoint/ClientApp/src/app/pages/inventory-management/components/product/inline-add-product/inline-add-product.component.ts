import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-inline-add-product',
  templateUrl: './inline-add-product.component.html',
  styleUrls: ['./inline-add-product.component.scss']
})
export class InlineAddProductComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  onAddSaveChange(event)
  {
    this.router.navigate(['../pages/inventory/product-list']);
  }

}
