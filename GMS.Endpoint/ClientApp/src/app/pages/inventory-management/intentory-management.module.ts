import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { AccordionModule } from 'primeng/accordion';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DropdownModule } from 'primeng/dropdown';
import { PanelModule } from 'primeng/panel';
import { CalendarModule } from 'primeng/calendar';
import { TabViewModule } from 'primeng/tabview';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { InventoryComponent } from './inventory.component';
import { InventoryManagementRoutingModule } from './inventory-management-routing.module';
import { SupplierListComponent } from './components/supplier-list/supplier-list.component';
import { AddSupplierComponent } from './components/supplier-list/add-supplier/add-supplier.component';
import { SupplierService } from './services/supplier.service';
import { InlineAddSupplierComponent } from './components/supplier-list/inline-add-supplier/inline-add-supplier.component';
import { UnitComponent } from './components/unit/unit.component';
import { AddUnitComponent } from './components/unit/add-unit/add-unit.component';
import { ProductCategoryComponent } from './components/product-category/product-category.component';
import { AddProductCategoryComponent } from './components/product-category/add-product-category/add-product-category.component';
import { ProductComponent } from './components/product/product.component';
import { AddProductComponent } from './components/product/add-product/add-product.component';
import { PurchaseComponent } from './components/purchase/purchase.component';
import { AddPurchaseComponent } from './components/purchase/add-purchase/add-purchase.component';
import { InlineAddPurchaseComponent } from './components/purchase/inline-add-purchase/inline-add-purchase.component';
import { InlineAddProductComponent } from './components/product/inline-add-product/inline-add-product.component';



@NgModule({
  declarations: [InventoryComponent,SupplierListComponent, AddSupplierComponent, InlineAddSupplierComponent, UnitComponent, AddUnitComponent, ProductCategoryComponent, AddProductCategoryComponent, ProductComponent, AddProductComponent, PurchaseComponent, AddPurchaseComponent, InlineAddPurchaseComponent, InlineAddProductComponent],
  imports: [
    CommonModule,
    InventoryManagementRoutingModule,
    TableModule,
    DialogModule,
    ButtonModule,
    AccordionModule,
    TableModule,
    DialogModule,
    InputTextModule,
    ReactiveFormsModule,
    ConfirmDialogModule,
    DropdownModule,
    FormsModule,
    PanelModule,
    CalendarModule,
    TabViewModule,
    InputTextareaModule,
    AutoCompleteModule,
    CheckboxModule,
  ],
  providers:[SupplierService]
})
export class IntentoryManagementModule { }
