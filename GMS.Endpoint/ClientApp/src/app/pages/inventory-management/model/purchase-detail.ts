export class PurchaseDetail
{
    id?:number;
    productId?:number;
    productName?:string;
    purchaseId?:number;
    unitId:number;
    unitName?:string;
    quantity:number;
    price:number;
    totalPrice:number;
}