export class Purchase
{
    id:number;
    purchaseDate:string;
    purchaseNo:string;
    totalBillAmount:number;
    totalPaidAmount:number;
    totalDueAmount:number;
    supplierName:string;
    paymentMethod:string;
    supplierId:number;
}