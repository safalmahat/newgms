export class Supplier {
    id: number;
    name: string;
    address: string;
    contactPerson: string;
    phoneNumber: string;
    mobileNumber: string;
    email: string;
    status: boolean;
}