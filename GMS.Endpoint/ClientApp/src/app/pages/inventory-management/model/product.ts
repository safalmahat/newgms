export class Product {
    id?: number;
    categoryId?:number;
    code?:string;
    name?:string;
    thumbnail?:string;
    actualPrice?:number;
    discountPercentage?:number;
    price?:number;
    stock?:number;
    description?:string;
    status?:boolean;
}