import { Purchase } from "./purchase";
import { PurchaseDetail } from "./purchase-detail";

export class PurchaseViewModel
{
    purchase:Purchase;
    purchaseDetails:PurchaseDetail[];
}