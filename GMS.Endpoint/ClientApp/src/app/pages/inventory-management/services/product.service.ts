import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataPager } from '../../model/data-paget';
import { Sorting } from '../../model/sorting-model';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient: HttpClient) { }

  getAll({
    dataPager,
    sortQuery,
  }: {
    dataPager: DataPager;
    sortQuery: Sorting;

  }): Observable<any[]>  {
    return this.httpClient.get<any[]>(
      "Product",
      {
        params: {
          PageNumber: dataPager.pageNumber.toString(),
          PageSize: dataPager.pageSize.toString(),
          SortByOrder: sortQuery.sortOrder.toString(),
          SortByColumn: sortQuery.columnName,
          IsRequired: dataPager.isRequired.toString()
        },
      }
    );
  }
  save(data)
  {
    debugger;
    if(data.Id>0)
    return this.httpClient.put("Product",data);
    else
    return this.httpClient.post("Product",data);
  }
  get(id)
  {
    return this.httpClient.get<Product>("Product/"+id);
  }
  delete(id)
  {
    return this.httpClient.delete("Product/"+id);
  }
  getAvailablProduct()
  {
    return this.httpClient.get<Product[]>("Product/available");
  }
}
