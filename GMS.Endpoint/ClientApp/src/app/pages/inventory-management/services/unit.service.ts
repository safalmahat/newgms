import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataPager } from '../../model/data-paget';
import { Sorting } from '../../model/sorting-model';
import { Unit } from '../model/unit';

@Injectable({
  providedIn: 'root'
})
export class UnitService {

  constructor(private httpClient:HttpClient) { }
  getAll({
    dataPager,
    sortQuery,
  }: {
    dataPager: DataPager;
    sortQuery: Sorting;

  }): Observable<any[]> {
    return this.httpClient.get<any[]>(
      "Unit",
      {
        params: {
          PageNumber: dataPager.pageNumber.toString(),
          PageSize: dataPager.pageSize.toString(),
          SortByOrder: sortQuery.sortOrder.toString(),
          SortByColumn: sortQuery.columnName,
          IsRequired: dataPager.isRequired.toString()
        },
      }
    );
  }
  save(data)
  {
    if(data.Id>0)
    return this.httpClient.put("Unit",data);
    else
    return this.httpClient.post("Unit",data);
  }
  get(id)
  {
    return this.httpClient.get<Unit>("Unit/"+id);
  }
  delete(id)
  {
    return this.httpClient.delete("Unit/"+id);
  }
}
