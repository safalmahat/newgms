import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataPager } from '../../model/data-paget';
import { PurchaseViewModel } from '../model/purchase-view-model';
import { Sorting } from '../../model/sorting-model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PurchaseService {

  constructor(private httpClient: HttpClient) { }
  save(data) {
    if (data.Purchase.Id > 0)
      return this.httpClient.put('Purchase', data);
    else
      return this.httpClient.post('Purchase', data);
  }
  getAll({
    dataPager,
    sortQuery,
  }: {
    dataPager: DataPager;
    sortQuery: Sorting;

  }): Observable<any[]> {
    return this.httpClient.get<any[]>(
      "Purchase",
      {
        params: {
          PageNumber: dataPager.pageNumber.toString(),
          PageSize: dataPager.pageSize.toString(),
          SortByOrder: sortQuery.sortOrder.toString(),
          SortByColumn: sortQuery.columnName,
          IsReqired: dataPager.isRequired.toString()
        },
      }
    );
  }
  getPurchaseDetail(purchaseId) {
    return this.httpClient.get<any[]>(`Purchase/${purchaseId}/Detail`);
  }
  getPurchase(id) {
    return this.httpClient.get<PurchaseViewModel>(`Purchase/${id}`);
  }
  generatePurchaseNumber() {
    return this.httpClient.get<number>(`Purchase/generate/purchaseNumber`);
  }
  delete(id) {
    return this.httpClient.delete(`Purchase/${id}`);
  }
}
