import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Supplier } from '../model/supplier';
import { DataPager } from '../../model/data-paget';
import { Sorting } from '../../model/sorting-model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {

  constructor(private httpClient: HttpClient) { }

  getAll({
    dataPager,
    sortQuery,
  }: {
    dataPager: DataPager;
    sortQuery: Sorting;

  }): Observable<any[]> {
    return this.httpClient.get<any[]>(
      "Supplier",
      {
        params: {
          PageNumber: dataPager.pageNumber.toString(),
          PageSize: dataPager.pageSize.toString(),
          SortByOrder: sortQuery.sortOrder.toString(),
          SortByColumn: sortQuery.columnName,
          IsRequired: dataPager.isRequired.toString()
        },
      }
    );
  }
  save(data)
  {
    if(data.Id>0)
    return this.httpClient.put("Supplier",data);
    else
    return this.httpClient.post("Supplier",data);
  }
  get(id)
  {
    return this.httpClient.get<Supplier>("Supplier/"+id);
  }
  delete(id)
  {
    return this.httpClient.delete("Supplier/"+id);
  }
}
