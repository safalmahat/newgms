import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataPager } from '../../model/data-paget';
import { Sorting } from '../../model/sorting-model';
import { ProductCategory } from '../model/product-category';

@Injectable({
  providedIn: 'root'
})
export class ProductCategoryService {

  constructor(private httpClient:HttpClient) { }
  getAll({
    dataPager,
    sortQuery,
  }: {
    dataPager: DataPager;
    sortQuery: Sorting;

  }): Observable<any[]> {
    return this.httpClient.get<any[]>(
      "ProductCategory",
      {
        params: {
          PageNumber: dataPager.pageNumber.toString(),
          PageSize: dataPager.pageSize.toString(),
          SortByOrder: sortQuery.sortOrder.toString(),
          SortByColumn: sortQuery.columnName,
          IsRequired: dataPager.isRequired.toString()
        },
      }
    );
  }
  save(data)
  {
    if(data.Id>0)
    return this.httpClient.put("ProductCategory",data);
    else
    return this.httpClient.post("ProductCategory",data);
  }
  get(id)
  {
    return this.httpClient.get<ProductCategory>("ProductCategory/"+id);
  }
  delete(id)
  {
    return this.httpClient.delete("ProductCategory/"+id);
  }
}
