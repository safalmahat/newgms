import { NbMenuItem } from '@nebular/theme';
function checkRoles(roles) {
  debugger;
  let isAssigned=false;
  if (roles.length > 0) {
    for (var i = 0; i < roles.length; i++) {
      let assignedRoles = JSON.parse(sessionStorage.currentUser).roles;
      let index = assignedRoles.indexOf(roles[i]);
      if (index > -1) {
        isAssigned= true;
        break;
      }

      else {
        isAssigned= false;
      }
    }
   return isAssigned;
  }
  else
    return false;
}
export const MENU_ITEMS: NbMenuItem[] = [

  {
    title: 'Customer',
    hidden: !checkRoles(['Admin', 'Manager','Receptionist']),
    icon: 'people-outline',
    children: [
      {
        title: 'Add Customer',
        hidden: !checkRoles(['Admin','Manager']),
        icon: 'fa-user-o',
        link: '/pages/add-customer',
      },
      {
        title: 'List Customer',
        hidden: !checkRoles(['Admin','Manager','Receptionist']),
        icon: 'people-outline',
        link: '/pages/customer-list',
      },
    ],
  },
  {
    title: 'Vehicle',
    hidden: !checkRoles(['Admin','Manager','Receptionist']),
    icon: 'car-outline',
    children: [
      {
        title: 'Add Vehicle',
        hidden: !checkRoles(['Admin','Manager']),
        icon: 'car-outline',
        link: '/pages/add-vehicle',
      },
      {
        title: 'List Vehicle',
        hidden: !checkRoles(['Admin','Manager','Receptionist']),
        icon: 'car-outline',
        link: '/pages/vehicle-list',
      },
    ],
  },
  {
    title: 'Job Card',
    hidden: !checkRoles(['Admin','Manager','Receptionist']),
    icon: 'credit-card-outline',
    children: [
      {
        title: 'Add Job',
        hidden: !checkRoles(['Admin','Manager','Receptionist']),
        icon: 'credit-card-outline',
        link: '/pages/add-job',
      },
      {
        title: 'List Job',
        hidden: !checkRoles(['Admin','Manager','Receptionist']),
        icon: 'credit-card-outline',
        link: '/pages/job-list',
      },
    ],
  },
  {
    title: 'User Management',
    hidden: !checkRoles(['Admin']),
    icon: 'people-outline',
    children: [
      {
        title: 'Add User',
        hidden: !checkRoles(['Admin']),
        icon: 'person-add-outline',
        link: '/pages/user/add-user',
      },
      {
        title: 'List User',
        hidden: !checkRoles(['Admin']),
        icon: 'people-outline',
        link: '/pages/user/user-list',
      }
    ],
  },
  {
    title: 'Inventory Management',
    hidden: !checkRoles(['Admin','Manager']),
    icon: 'home-outline',
    children: [
      {
        title: 'Master',
        hidden: !checkRoles(['Admin','Manager']),
        icon: 'lock-outline',
        children: [
          {
            title: 'List Unit',
            hidden: !checkRoles(['Admin','Manager']),
            icon: 'lock-outline',
            link: '/pages/inventory/unit-list',
          },
          {
            title: 'List Product Category',
            hidden: !checkRoles(['Admin','Manager']),
            icon: 'lock-outline',
            link: '/pages/inventory/category-list',
          }
        ]
      },
      {
        title: 'Supplier',
        hidden: !checkRoles(['Admin','Manager']),
        icon: 'car-outline',
        children: [
          {
            title: 'Add Supplier',
            hidden: !checkRoles(['Admin','Manager']),
            icon: 'car-outline',
            link: '/pages/inventory/add-supplier',
          },
          {
            title: 'List Supplier',
            hidden: !checkRoles(['Admin','Manager']),
            icon: 'car-outline',
            link: '/pages/inventory/supplier-list',
          }
        ]
      },
      {
        title: 'Product',
        hidden: !checkRoles(['Admin','Manager']),
        icon: 'inbox-outline',
        children: [
          {
            title: 'Add Product',
            hidden: !checkRoles(['Admin','Manager']),
            icon: 'inbox-outline',
            link: '/pages/inventory/product-add',
          },
          {
            title: 'List Product',
            hidden: !checkRoles(['Admin','Manager']),
            icon: 'inbox-outline',
            link: '/pages/inventory/product-list',
          }
        ]
      },
      {
        title: 'Purchase',
        hidden: !checkRoles(['Admin','Manager']),
        icon: 'inbox-outline',
        children: [
          {
            title: 'Add Purchase',
            hidden: !checkRoles(['Admin','Manager']),
            icon: 'inbox-outline',
            link: '/pages/inventory/purchase-add',
          },
          {
            title: 'List Purchase',
            hidden: !checkRoles(['Admin','Manager']),
            icon: 'inbox-outline',
            link: '/pages/inventory/purchase-list',
          }
        ]
      }

    ],
  },
];
