import { NgModule } from '@angular/core';
import { NbMenuModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { CustomerComponent } from './customer/customer.component';
import { AddCustomerComponent } from './customer/add-customer/add-customer.component';
import {ButtonModule} from 'primeng/button';
import {AccordionModule} from 'primeng/accordion';
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {InputTextModule} from 'primeng/inputtext';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { VehicleComponent } from './vehicle/vehicle.component';
import { AddVehicleComponent } from './vehicle/add-vehicle/add-vehicle.component';
import { DropdownModule } from 'primeng/dropdown';
import { LoginComponent } from './login/login.component';
import { PanelModule } from 'primeng/panel';
import { JobCardComponent } from './job-card/job-card.component';
import { AddJobCardComponent } from './job-card/add-job-card/add-job-card.component';
import { JobCompliantDetailComponent } from './job-card/job-compliant-detail/job-compliant-detail.component';
import { JobInventoryDetailComponent } from './job-card/job-inventory-detail/job-inventory-detail.component';
import { CalendarModule } from 'primeng/calendar';
import { TabViewModule } from 'primeng/tabview';
import { InlineAddCustomerComponent } from './customer/inline-add-customer/inline-add-customer.component';
import { InlineAddVehicleComponent } from './vehicle/inline-add-vehicle/inline-add-vehicle.component';
import { InlineAddJobComponent } from './job-card/inline-add-job/inline-add-job.component';
import { IgxRadialGaugeModule } from 'igniteui-angular-gauges';
import { InputTextareaModule } from 'primeng/inputtextarea';
import {AutoCompleteModule} from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { UserManagementRoutingModule } from './user-management/user-management-routing.module';
@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    ButtonModule,
    AccordionModule,
    TableModule,
    DialogModule,
    InputTextModule,
    ReactiveFormsModule,
    ConfirmDialogModule,
    DropdownModule,
    FormsModule,
    PanelModule,
    CalendarModule,
    TabViewModule,
    IgxRadialGaugeModule,
    InputTextareaModule,
    AutoCompleteModule,
    CheckboxModule,
    UserManagementRoutingModule
  ],
  declarations: [
    PagesComponent,
    CustomerComponent,
    AddCustomerComponent,
    VehicleComponent,
    AddVehicleComponent,
    LoginComponent,
    JobCardComponent,
    AddJobCardComponent,
    JobCompliantDetailComponent,
    JobInventoryDetailComponent,
    InlineAddCustomerComponent,
    InlineAddVehicleComponent,
    InlineAddJobComponent,
    
  ],
  providers:[ConfirmationService]
})
export class PagesModule {
}
