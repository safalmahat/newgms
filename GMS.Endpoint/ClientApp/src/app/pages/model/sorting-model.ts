import { ColumnSortOrderType } from "./custom-sort-order-type";

export class Sorting {
    columnName: string;
    sortOrder: ColumnSortOrderType;
    constructor() { }
 }
