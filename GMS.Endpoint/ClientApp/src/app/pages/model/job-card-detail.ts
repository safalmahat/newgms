export class jobCardDetail{
    id:number;
    jobId:number;
    complainDescription:string
}

export class JobInventoryDetail{
    id:number;
    jobId:number;
    product:string;
    productId?:number;
    price:number;
    unit:number;
    amount:number;
}
export class JobInventoryCheckDetail{
    id:number;
    jobId:number;
    item:string;
    unit:number;
}
export class JobCardLabourDetail{
    id:number;
    jobId:number;
    description:string;
    quantity:number;
    unit:string;
    amount:number;
    totalAmount:number;
}