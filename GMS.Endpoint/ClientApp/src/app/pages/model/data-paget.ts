export class DataPager {
    pageNumber: number;
    pageSize: number;
    recordsCount: number; 
    filterQuery?:string; 
    isRequired?:boolean;   
    constructor() { }
 }