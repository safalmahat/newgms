import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobCompliantDetailComponent } from './job-compliant-detail.component';

describe('JobCompliantDetailComponent', () => {
  let component: JobCompliantDetailComponent;
  let fixture: ComponentFixture<JobCompliantDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobCompliantDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobCompliantDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
