import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobInventoryDetailComponent } from './job-inventory-detail.component';

describe('JobInventoryDetailComponent', () => {
  let component: JobInventoryDetailComponent;
  let fixture: ComponentFixture<JobInventoryDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobInventoryDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobInventoryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
