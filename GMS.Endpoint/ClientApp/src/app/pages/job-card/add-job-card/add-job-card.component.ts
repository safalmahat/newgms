import { AfterViewInit, Component, EventEmitter, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Table } from 'primeng/table';
import { SelectItem } from 'primeng/api';
import { jobCardDetail, JobCardLabourDetail, JobInventoryCheckDetail, JobInventoryDetail } from '../../model/job-card-detail';
import { VehicleService } from '../../services/vehicle.service';
import { JobCardService } from '../../services/job-card.service';
import { DatePipe } from '@angular/common';
import { IgxRadialGaugeComponent, IgxRadialGaugeRangeComponent, RadialGaugeBackingShape, RadialGaugeNeedleShape, RadialGaugePivotShape, RadialGaugeScaleOversweepShape } from 'igniteui-angular-gauges';
import { SweepDirection } from 'igniteui-angular-core';
import { ProductService } from '../../inventory-management/services/product.service';

@Component({
  selector: 'ngx-add-job-card',
  templateUrl: './add-job-card.component.html',
  styleUrls: ['./add-job-card.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddJobCardComponent implements OnInit, AfterViewInit {
  @ViewChild("radialGauge", { static: true })
  public radialGauge: IgxRadialGaugeComponent;

  gridColumns: any;
  jobCardForm: FormGroup;
  jobCardDetailForm: FormGroup;
  jobCardInventoryCheckDetailForm: FormGroup;
  jobCardInventoryDetailForm: FormGroup;
  jobCardLabourForm: FormGroup;
  vehicleListItem: SelectItem[];
  jobDetailsList: any[] = [];
  jobInventoryDetailsList: any[] = [];
  inventoryCheckList: any[] = [];
  labourList: any[] = [];
  isEditButtonClicked: boolean = false;
  isInventoryEditButtonClicked: boolean = false;
  isInventoryCheckEditButtonClicked: boolean = false;

  isAddGoalsButtonClicked: boolean = false;
  isAddinventoryButtonClicked: boolean = false;
  isAddinventoryCheckButtonClicked: boolean = false;

  isAddLabourButtonClicked: boolean = false;
  isLabourEditButtonClicked: boolean = false;

  isDisableInventoryAndLabourTab: boolean = false;

  @ViewChild("jobDetailTable", { static: false }) jobDetailTable: Table;
  @ViewChild("jobInventoryDetailTable", { static: false }) jobInventoryDetailTable: Table;
  @ViewChild("jobInventoryCheckDetailTable", { static: false }) jobInventoryCheckDetailTable: Table;
  @ViewChild("jobLabourDetailTable", { static: false }) jobLabourDetailTable: Table;
  @Output() displayChange = new EventEmitter();
  @Output() onSaveChange = new EventEmitter();
  disableAddComplainBtn: boolean = false;
  isRowEditBtnDisable: boolean = false;
  isRowDeleteBtnDisable: boolean = false;
  masterDataItems: SelectItem[];
  productSelectItems: SelectItem[];
  fuelGuageValue: number = 0;
  submitButtonText: string = "Save";
  isLoading: boolean = false;
  constructor(private fb: FormBuilder,
    private vehicleService: VehicleService,
    private jobCardService: JobCardService,
    private datePipe: DatePipe,
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    this.jobCardForm = this.fb.group({
      Id: 0,
      CreatedDate: ["", [Validators.required]],
      JobNumber: "",
      VechileId: "",
      KmReading: "",
      IsServicing: ""
    });
    this.jobCardInventoryDetailForm = this.fb.group({
      id: '',
      jobId: '',
      productId: '',
      unit: '',
      price: '',
      amount: '',
      totalBillAmount: ''
    });
    this.jobCardDetailForm = this.fb.group({
      Id: "",
      JobId: "",
      ComplainDescription: ""
    });
    this.jobCardInventoryCheckDetailForm = this.fb.group({
      Id: "",
      jobId: '',
      Item: "",
      Unit: ""
    });
    this.jobCardLabourForm = this.fb.group({
      Id: "",
      jobId: '',
      Description: "",
      Quantity: '',
      Unit: "",
      Amount: '',
      TotalAmount: '',
    });
    this.jobCardForm.setControl(
      "jobDetail",
      this.jobCardDetailForm
    );
    this.jobCardForm.setControl(
      "jobInventoryDetail",
      this.jobCardInventoryDetailForm
    );
    this.jobCardForm.setControl(
      "jobInventoryCheckDetail",
      this.jobCardInventoryCheckDetailForm
    );
    this.jobCardForm.setControl(
      "jobCardLabourForm",
      this.jobCardLabourForm
    );
    this.jobDetailsList = [];
    this.inventoryCheckList = [];
    this.jobInventoryDetailsList = [];
    this.labourList = [];
    this.getAllVechile();
    this.getMasterData();
    this.getProduct();
    this.isDisableInventoryAndLabourTab = true;
    this.jobCardService.generateJobNumber().subscribe(resp => {
      this.jobCardForm.get("JobNumber").setValue(resp);
      this.jobCardForm.get("JobNumber").disable();
    })
  }
  getMasterData() {
    this.vehicleService.getMasterData().subscribe((resp) => {
      if (resp.length > 0) {
        this.masterDataItems = resp.map((r) => {
          return {
            label: r.name,
            value: r.name
          };
        });
      }
    });
  }
  getAllVechile() {
    let pager = {
      pageNumber: 1,
      pageSize: 1,
      recordsCount: 0,
      isRequired: false
    };
    let sortColumn = {
      columnName: "",
      sortOrder: 1
    }
    this.vehicleService.getAllVehicle({ dataPager: pager, sortQuery: sortColumn }).subscribe((resp) => {
      if (resp.length > 0) {
        this.vehicleListItem = resp.map((r) => {
          return {
            label: r.Registration_Number,
            value: r.Id
          };
        });
      }
    });
  }
  getProduct() {
    this.productService.getAvailablProduct().subscribe((resp) => {
      if (resp.length > 0) {
        this.productSelectItems = resp.map((r) => {
          return {
            label: r.name,
            value: {
              id: r.id,
              name: r.name,
              price: r.price
            }
          };
        });
      }
    });
  }
  addNewRow() {
    if (!this.isAddGoalsButtonClicked) {
      this.isAddGoalsButtonClicked = true;
      let newOrderDetail = new jobCardDetail();
      newOrderDetail.id = 0;
      newOrderDetail["rowIndex"] = this.jobDetailsList.length + 1;
      newOrderDetail.complainDescription = "";
      newOrderDetail.jobId = 0;
      this.jobDetailsList.unshift(newOrderDetail);
      this.jobDetailTable.initRowEdit(newOrderDetail);
      this.jobCardDetailForm.reset();
    }
  }
  addNewInventory() {
    debugger;
    if (!this.isAddinventoryButtonClicked) {
      this.isAddinventoryButtonClicked = true;
      let newOrderDetail = new JobInventoryDetail();
      newOrderDetail.id = 0;
      newOrderDetail["rowIndex"] = this.jobInventoryDetailsList.length + 1;
      newOrderDetail.product = "";
      newOrderDetail.jobId = 0;
      newOrderDetail.productId = 0;
      newOrderDetail.amount = 0;
      newOrderDetail.price = 0;
      newOrderDetail.unit = 0;
      this.jobInventoryDetailsList.unshift(newOrderDetail);
      this.jobInventoryDetailTable.initRowEdit(newOrderDetail);
      this.jobCardInventoryDetailForm.reset();
    }
  }
  addInventoryCheck() {
    if (!this.isAddinventoryCheckButtonClicked) {
      this.isAddinventoryCheckButtonClicked = true;
      let newOrderDetail = new JobInventoryCheckDetail();
      newOrderDetail.id = 0;
      newOrderDetail["rowIndex"] = this.inventoryCheckList.length + 1;
      newOrderDetail.item = "";
      newOrderDetail.jobId = 0;
      newOrderDetail.unit = 0;
      this.inventoryCheckList.unshift(newOrderDetail);
      this.jobInventoryCheckDetailTable.initRowEdit(newOrderDetail);
      this.jobCardInventoryCheckDetailForm.reset();
    }
  }
  addNewLabour() {
    if (!this.isAddLabourButtonClicked) {
      this.isAddLabourButtonClicked = true;
      let newOrderDetail = new JobCardLabourDetail();
      newOrderDetail.id = 0;
      newOrderDetail["rowIndex"] = this.labourList.length + 1;
      newOrderDetail.description = "";
      newOrderDetail.jobId = 0;
      newOrderDetail.unit = '';
      newOrderDetail.amount = 0;
      newOrderDetail.totalAmount = 0;
      newOrderDetail.jobId = 0;
      this.labourList.unshift(newOrderDetail);
      this.jobLabourDetailTable.initRowEdit(newOrderDetail);
      this.jobCardLabourForm.reset();
    }
  }
  onSaveClick() {
    this.submitButtonText = "Processing";
    this.isLoading = true;
    let jobCard = {
      Id: this.jobCardForm.get('Id').value,
      JobNumber: this.jobCardForm.get('JobNumber').value,
      KmReading: this.jobCardForm.get('KmReading').value,
      VehicleId: this.jobCardForm.get('VechileId').value,
      CreatedDate: this.jobCardForm.get('CreatedDate').value,
      FuelQuantity: this.radialGauge.value
    };
    let jobList = [];
    let jobInventoryDetailList = [];
    this.jobDetailsList.forEach(element => {
      let jobDetail = {
        ComplainDescription: element.complainDescription,
        Id: element.id,
        JobId: element.jobid
      };
      jobList.push(jobDetail);
    });
    debugger;
    this.jobInventoryDetailsList.forEach(element => {
      let jobInventoryDetail = {
        product: element.product,
        price: element.price,
        amount: element.amount,
        unit: element.unit,
        id: element.id,
        jobId: element.jobId
      };
      jobInventoryDetailList.push(jobInventoryDetail);
    });
    let inventoryCheckList = [];
    this.inventoryCheckList.forEach(element => {
      let jobInventoryCheckDetail = {
        Item: element.item,
        Unit: element.unit,
        Id: element.id,
        JobId: element.jobId
      };
      inventoryCheckList.push(jobInventoryCheckDetail);
    });
    let labourList = [];
    this.labourList.forEach(element => {
      let labourDetail = {
        Item: element.description,
        Unit: element.unit,
        Quantity: element.quantity,
        Amount: element.amount,
        TotalAmount: element.totalAmount,
        Id: element.id,
        JobId: element.jobId
      };
      labourList.push(labourDetail);
    });
    let finalData = {
      jobCard: jobCard,
      JobCardDetails: jobList,
      JobCardInventoryDetails: jobInventoryDetailList,
      JobCardInventoryCheckDetails: inventoryCheckList,
      JobCardLabourDetails: labourList
    }
    this.jobCardService.save(finalData).subscribe(resp => {
      this.displayChange.emit(true);
      this.onSaveChange.emit(true);
      this.submitButtonText = "Save";
      this.isLoading = false;
    })
  }
  onClose() {

  }
  onRowEditInit(jobDetail: jobCardDetail) {
    this.isEditButtonClicked = true;
    if (jobDetail.id > 0) {
      this.jobCardForm
        .get("jobDetail")
        .get("ComplainDescription")
        .setValue(jobDetail.complainDescription);
    }
    this.jobDetailTable.initRowEdit(jobDetail);
  }
  onInventoryRowEditInit(jobDetail: JobInventoryDetail) {
    this.isInventoryEditButtonClicked = true;
    if (jobDetail.id > 0) {
      this.jobCardForm
        .get("jobInventoryDetail")
        .get("product")
        .setValue(jobDetail.product);
      this.jobCardForm
        .get("jobInventoryDetail")
        .get("unit")
        .setValue(jobDetail.unit);
      this.jobCardForm
        .get("jobInventoryDetail")
        .get("price")
        .setValue(jobDetail.price);
      this.jobCardForm
        .get("jobInventoryDetail")
        .get("amount")
        .setValue(jobDetail.amount);
    }
    this.jobInventoryDetailTable.initRowEdit(jobDetail);
  }
  onInventoryCheckRowEditInit(jobDetail: JobInventoryCheckDetail) {
    this.isInventoryCheckEditButtonClicked = true;
    if (jobDetail.id > 0) {
      this.jobCardForm
        .get("jobInventoryCheckDetail")
        .get("item")
        .setValue(jobDetail.item);
      this.jobCardForm
        .get("jobInventoryCheckDetail")
        .get("unit")
        .setValue(jobDetail.unit);
    }
    this.jobInventoryDetailTable.initRowEdit(jobDetail);
  }
  onlabourRowEditInit(jobDetail: JobCardLabourDetail) {
    this.isLabourEditButtonClicked = true;
    if (jobDetail.id > 0) {
      this.jobCardForm
        .get("jobCardLabourForm")
        .get("Description")
        .setValue(jobDetail.description);
      this.jobCardForm
        .get("jobCardLabourForm")
        .get("Quantity")
        .setValue(jobDetail.quantity);
      this.jobCardForm
        .get("jobCardLabourForm")
        .get("Unit")
        .setValue(jobDetail.unit);
      this.jobCardForm
        .get("jobCardLabourForm")
        .get("Amount")
        .setValue(jobDetail.amount);
      this.jobCardForm
        .get("jobCardLabourForm")
        .get("TotalAmount")
        .setValue(jobDetail.totalAmount);
    }
    this.jobInventoryDetailTable.initRowEdit(jobDetail);
  }
  deleteRow(jobDetail) {
    const i = this.jobDetailsList.indexOf(jobDetail, 0);
    if (i > -1) {
      this.jobDetailsList.splice(i, 1);
    }
  }
  deleteInventoryRow(inventory) {
    const i = this.jobInventoryDetailsList.indexOf(inventory, 0);
    if (i > -1) {
      this.jobInventoryDetailsList.splice(i, 1);
    }
  }
  deleteInventoryCheckRow(inventory) {
    const i = this.inventoryCheckList.indexOf(inventory, 0);
    if (i > -1) {
      this.inventoryCheckList.splice(i, 1);
    }
  }
  deleteLabourRow(inventory) {
    const i = this.labourList.indexOf(inventory, 0);
    if (i > -1) {
      this.labourList.splice(i, 1);
    }
  }
  onRowEditSave(jobDetail) {
    if (this.jobCardDetailForm.valid) {
      (jobDetail.id = jobDetail.id),
        (jobDetail.complainDescription = this.jobCardDetailForm.get('ComplainDescription').value);
      this.isAddGoalsButtonClicked = false;
      this.isEditButtonClicked = false;
    }
    this.isEditButtonClicked = false;
  }
  onInventoryRowEditSave(jobInventoryDetail) {
    if (this.jobCardInventoryDetailForm.valid) {
      (jobInventoryDetail.id = jobInventoryDetail.id),
        (jobInventoryDetail.productId = this.jobCardInventoryDetailForm.get('productId').value.value);
      (jobInventoryDetail.product = this.jobCardInventoryDetailForm.get('productId').value.name);
      (jobInventoryDetail.unit = this.jobCardInventoryDetailForm.get('unit').value);
      (jobInventoryDetail.price = this.jobCardInventoryDetailForm.get('price').value);
      (jobInventoryDetail.amount = this.jobCardInventoryDetailForm.get('amount').value);
      this.isAddinventoryButtonClicked = false;
      this.isInventoryEditButtonClicked = false;
    }
    this.isInventoryEditButtonClicked = false;
  }
  onInventoryCheckRowEditSave(jobInventoryDetail) {
    if (this.jobCardInventoryDetailForm.valid) {
      (jobInventoryDetail.id = jobInventoryDetail.id),
        (jobInventoryDetail.item = this.jobCardInventoryCheckDetailForm.get('Item').value);
      (jobInventoryDetail.unit = this.jobCardInventoryCheckDetailForm.get('Unit').value);
      this.isAddinventoryCheckButtonClicked = false;
      this.isInventoryCheckEditButtonClicked = false;
    }
    this.isInventoryCheckEditButtonClicked = false;
  }
  onLabourRowEditSave(jobLabourDetail) {
    if (this.jobCardLabourForm.valid) {
      (jobLabourDetail.id = jobLabourDetail.id),
        (jobLabourDetail.description = this.jobCardLabourForm.get('Description').value);
      (jobLabourDetail.unit = this.jobCardLabourForm.get('Unit').value);
      (jobLabourDetail.quantity = this.jobCardLabourForm.get('Quantity').value);
      (jobLabourDetail.amount = this.jobCardLabourForm.get('Amount').value);
      (jobLabourDetail.totalAmount = this.jobCardLabourForm.get('TotalAmount').value);
      this.isAddLabourButtonClicked = false;
      this.isLabourEditButtonClicked = false;
    }
    this.isLabourEditButtonClicked = false;
  }
  onCancelClick() {
    this.displayChange.emit(true);
  }
  onChange() {
    let unit = this.jobCardInventoryDetailForm.get("unit").value;
    let price = this.jobCardInventoryDetailForm.get("price").value;
    let amount = unit * price;
    this.jobCardInventoryDetailForm.get("amount").setValue(amount);
  }
  onLabourChange() {
    let quantity = this.jobCardLabourForm.get("Quantity").value;
    let amount = this.jobCardLabourForm.get("Amount").value;
    let totalAmount = quantity * amount;
    this.jobCardLabourForm.get("TotalAmount").setValue(totalAmount);
  }
  getJobById(id, action) {
    this.jobCardService.getJobCardById(id).subscribe(resp => {
      this.jobDetailsList = resp.jobCardDetails;
      this.jobInventoryDetailsList = resp.jobCardInventoryDetails;
      this.inventoryCheckList = resp.jobCardInventoryCheckDetails;
      this.jobCardForm.patchValue({
        VechileId: resp.jobCard.vehicleId,
        Id: resp.jobCard.id,
        JobNumber: resp.jobCard.jobNumber,
        KmReading: resp.jobCard.kmReading,
        CreatedDate: this.datePipe.transform(new Date(resp.jobCard.createdDate.split("T")[0]), 'MM/dd/yyyy'),
      });
      this.radialGauge.value = resp.jobCard.fuelQuantity;
      this.fuelGuageValue = resp.jobCard.fuelQuantity;
      this.disableInventoryAndLabourTab();
    })
    if (action == 'view') {
      this.jobCardForm.get('VechileId').disable();
      this.jobCardForm.get('JobNumber').disable();
      this.jobCardForm.get('KmReading').disable();
      this.jobCardForm.get('CreatedDate').disable();
      this.disableAddComplainBtn = true;
      this.isRowEditBtnDisable = true;
      this.isRowDeleteBtnDisable = true;

    }
    else {
      this.jobCardForm.get('VechileId').enable();
      this.jobCardForm.get('JobNumber').enable();
      this.jobCardForm.get('KmReading').enable();
      this.jobCardForm.get('CreatedDate').enable();
      this.disableAddComplainBtn = false;
      this.isRowEditBtnDisable = false;
      this.isRowDeleteBtnDisable = false;
    }

  }
  disableInventoryAndLabourTab() {
    debugger;
    if (this.jobCardForm.get("Id").value > 0)
      this.isDisableInventoryAndLabourTab = false;
    else
      this.isDisableInventoryAndLabourTab = true;
  }

  ngAfterViewInit(): void {

    // enabling animation duration (in milliseconds)
    this.radialGauge.transitionDuration = 500;
    this.AnimateToGauge3();
  }

  public AnimateToGauge3(): void {

    this.radialGauge.height = "180px";
    this.radialGauge.width = "100%";

    this.radialGauge.minimumValue = 0;
    this.radialGauge.maximumValue = 50;
    this.radialGauge.value = this.fuelGuageValue;
    this.radialGauge.interval = 5;

    // setting appearance of labels
    this.radialGauge.labelInterval = 5;
    this.radialGauge.labelExtent = 0.71;
    this.radialGauge.font = "10px Verdana,Arial";

    // setting custom needle
    this.radialGauge.isNeedleDraggingEnabled = true;
    this.radialGauge.needleEndExtent = 0.5;
    this.radialGauge.needleShape = RadialGaugeNeedleShape.Triangle;
    this.radialGauge.needleEndWidthRatio = 0.03;
    this.radialGauge.needleStartWidthRatio = 0.05;
    this.radialGauge.needlePivotShape = RadialGaugePivotShape.CircleOverlay;
    this.radialGauge.needlePivotWidthRatio = 0.15;
    this.radialGauge.needleBaseFeatureWidthRatio = 0.15;
    this.radialGauge.needleBrush = "#79797a";
    this.radialGauge.needleOutline = "#79797a";
    this.radialGauge.needlePivotBrush = "#79797a";
    this.radialGauge.needlePivotOutline = "#79797a";

    // setting appearance of major/minor ticks
    this.radialGauge.minorTickCount = 4;
    this.radialGauge.minorTickEndExtent = 0.625;
    this.radialGauge.minorTickStartExtent = 0.6;
    this.radialGauge.minorTickStrokeThickness = 1;
    this.radialGauge.minorTickBrush = "#79797a";
    this.radialGauge.tickStartExtent = 0.6;
    this.radialGauge.tickEndExtent = 0.65;
    this.radialGauge.tickStrokeThickness = 2;
    this.radialGauge.tickBrush = "#79797a";

    // setting extent of gauge scale
    this.radialGauge.scaleStartAngle = 120;
    this.radialGauge.scaleEndAngle = 60;
    this.radialGauge.scaleBrush = "#d6d6d6";
    this.radialGauge.scaleOversweepShape = RadialGaugeScaleOversweepShape.Fitted;
    this.radialGauge.scaleSweepDirection = SweepDirection.Clockwise;
    this.radialGauge.scaleEndExtent = 0.57;
    this.radialGauge.scaleStartExtent = 0.5;

    // setting appearance of backing dial
    this.radialGauge.backingBrush = "#fcfcfc";
    this.radialGauge.backingOutline = "#d6d6d6";
    this.radialGauge.backingStrokeThickness = 5;
    this.radialGauge.backingShape = RadialGaugeBackingShape.Circular;

    // setting custom gauge ranges
    const range1 = new IgxRadialGaugeRangeComponent();
    range1.startValue = 5;
    range1.endValue = 15;
    const range2 = new IgxRadialGaugeRangeComponent();
    range2.startValue = 15;
    range2.endValue = 35;
    const range3 = new IgxRadialGaugeRangeComponent();
    range3.startValue = 35;
    range3.endValue = 45;
    this.radialGauge.rangeBrushes = ["#F86232", "#DC3F76", "#7446B9"];
    this.radialGauge.rangeOutlines = ["#F86232", "#DC3F76", "#7446B9"];
    this.radialGauge.ranges.clear();
    this.radialGauge.ranges.add(range1);
    this.radialGauge.ranges.add(range2);
    this.radialGauge.ranges.add(range3);
    // setting extent of all gauge ranges
    for (let i = 0; i < this.radialGauge.ranges.count; i++) {
      const range = this.radialGauge.ranges.item(i);
      range.innerStartExtent = 0.5;
      range.innerEndExtent = 0.5;
      range.outerStartExtent = 0.57;
      range.outerEndExtent = 0.57;
    }
  }
  onIsServicingChange(event) {
    if (event.checked) {
      this.jobCardService.getServicingComplainData().subscribe(resp => {
        if (resp.length > 0) {
          resp.forEach(element => {
            let existingServicingItem = this.jobDetailsList.filter(job => {
              return job.complainDescription == element.value;
            });
            if (existingServicingItem.length == 0)
              this.jobDetailsList.push({ id: 0, rowIndex: this.jobDetailsList.length + 1, complainDescription: element.value });
          });
        }
      });
    }
    else {
      this.jobCardService.getServicingComplainData().subscribe(resp => {
        if (resp.length > 0) {
          resp.forEach(element => {
            let existingServicingItem = this.jobDetailsList.filter(job => {
              return job.complainDescription == element.value;
            });
            if (existingServicingItem.length > 0) {
              let object = this.jobDetailsList.filter(resp => {
                return resp.complainDescription == existingServicingItem[0].complainDescription;
              })
              const i = this.jobDetailsList.indexOf(object[0], 0);
              if (i > -1) {
                this.jobDetailsList.splice(i, 1);
              }
            }
          });
        }
      });
    }
  }
  onInventaryProductChange(inventory, event) {
    debugger;
    this.jobCardInventoryDetailForm.get('price').setValue(event.value.price);
    inventory.price = event.value.price;
  }
  calculateTotalBillAmount() {
    if (this.jobInventoryDetailsList.length > 0) {

    }
  }
}
