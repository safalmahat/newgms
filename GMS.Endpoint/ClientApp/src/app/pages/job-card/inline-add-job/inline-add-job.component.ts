import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'ngx-inline-add-job',
  templateUrl: './inline-add-job.component.html',
  styleUrls: ['./inline-add-job.component.scss']
})
export class InlineAddJobComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  onSaveChange(event)
  {
    this.router.navigate(['../pages/job-list']);
  }
}
