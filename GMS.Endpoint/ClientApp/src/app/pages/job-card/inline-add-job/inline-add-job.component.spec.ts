import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InlineAddJobComponent } from './inline-add-job.component';

describe('InlineAddJobComponent', () => {
  let component: InlineAddJobComponent;
  let fixture: ComponentFixture<InlineAddJobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InlineAddJobComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineAddJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
