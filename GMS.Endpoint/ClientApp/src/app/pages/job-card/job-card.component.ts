
import { AfterViewInit, Component, ElementRef, OnInit, SimpleChanges, ViewChild, ViewEncapsulation } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { ColumnSortOrderType } from '../model/custom-sort-order-type';
import { DataPager } from '../model/data-paget';
import { Sorting } from '../model/sorting-model';
import { DataPagerService } from '../services/data-pager.service';
import { JobCardService } from '../services/job-card.service';
import { AddJobCardComponent } from './add-job-card/add-job-card.component';

@Component({
  selector: 'ngx-job-card',
  templateUrl: './job-card.component.html',
  styleUrls: ['./job-card.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class JobCardComponent implements OnInit, AfterViewInit {
  tableScrollHeight: string = '';
  @ViewChild("elRef", { static: true }) elRef: ElementRef;
  @ViewChild('addNewSection', { read: ElementRef, static: true }) addNewSection: ElementRef;
  @ViewChild('mainContent', { read: ElementRef, static: true }) mainContent: ElementRef;
  gridColumns: any[];
  jobList: any[];
  first = 0;
  recordsPerPage = 25;
  totalRecords = 0;
  totalGridRecords = 0;
  rowsPerPageOptions = [];
  selectedRowItem: any;
  currentPageNumber: number;
  pager: DataPager;
  sortColumn: Sorting;

  display: boolean = false;
  @ViewChild("jobCardAddForm", { static: true })
  jobCardAddForm: AddJobCardComponent;

  jobDetailList: any[];
  constructor(private jobCardService: JobCardService, private dataPagerService: DataPagerService,
    private confirmationService: ConfirmationService
  ) {
    this.rowsPerPageOptions = [12, 20, 30];

  }

  ngOnInit(): void {
    this.gridColumns = [
      { field: 'Job_Number', header: 'Job Number' },
      { field: 'Registration_Number', header: 'Registration Number' },
      { field: 'Customer_Name', header: 'Customer Name' },
      { field: 'Created_On', header: 'Created Date' }
    ];
    this.sortColumn = {
      columnName: "",
      sortOrder: ColumnSortOrderType.Ascending,
    };
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });

  }
  loadDataLazy(event: any): void {
    this.recordsPerPage = event.rows;
    this.first = event.first;
    this.currentPageNumber = this.dataPagerService.getCurrentPage(
      event.first,
      event.rows
    );
    this.getJob();

  }
  loadPageData(event: any): void {
    this.first = event.first;
    this.recordsPerPage = event.rows;
  }
  onPage(event) {
    this.currentPageNumber = this.dataPagerService.getCurrentPage(
      event.first,
      event.rows
    );
    this.first = event.first;
    this.recordsPerPage = event.rows;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
    return;
  }
  getJob() {
    this.pager = {
      pageNumber: this.currentPageNumber,
      pageSize: this.recordsPerPage,
      recordsCount: 0,
    };
    this.jobCardService.getAllCustomer({ dataPager: this.pager, sortQuery: this.sortColumn }).subscribe((resp) => {
      this.jobList = resp;
      if (resp.length > 0) {
        this.totalGridRecords = resp[0]["ROW_COUNT"];
      }
      else
        this.totalGridRecords = 0;
    });
  }
  customSort(event) {
    this.sortColumn = {
      columnName: event.field,
      sortOrder: 0
    }
    if (event.order == -1 || event.order == 0) {
      this.sortColumn.sortOrder = 1;
    }
    else
      this.sortColumn.sortOrder = 2;
    this.first = (this.currentPageNumber - 1) * this.recordsPerPage;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  };
  onAddClick() {

    this.display = true;
    this.jobCardAddForm.ngOnInit();

  }

  onEditClick(id) {
    this.display = true;
    this.jobCardAddForm.isEditButtonClicked = false;
    this.jobCardAddForm.isAddGoalsButtonClicked = false;
    this.jobCardAddForm.isInventoryEditButtonClicked = false;
    this.jobCardAddForm.isAddinventoryButtonClicked = false;
    this.jobCardAddForm.isAddinventoryCheckButtonClicked = false;
    this.jobCardAddForm.isInventoryCheckEditButtonClicked=false;
    this.jobCardAddForm.disableAddComplainBtn=false;
    this.jobCardAddForm.disableInventoryAndLabourTab();
    this.jobCardAddForm.getJobById(id,'edit');
  }
  
  onViewClick(id) {
    this.display = true;
    this.jobCardAddForm.isEditButtonClicked = false;
    this.jobCardAddForm.isAddGoalsButtonClicked = false;
    this.jobCardAddForm.isInventoryEditButtonClicked = false;
    this.jobCardAddForm.isAddinventoryButtonClicked = false;
    this.jobCardAddForm.getJobById(id,'view');
  }
  onDeleteClick(id) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete this record?',
      accept: () => {
        this.jobCardService.delete(id).subscribe(resp => {
          this.loadDataLazy({
            first: this.first,
            rows: this.recordsPerPage,
          });
        })
        //Actual logic to perform a confirmation
      }
    });
  }
  setScrollHeight() {
    // this.tableScrollHeight = `${(this.mainContent.nativeElement.scrollHeight - this.addNewSection.nativeElement.scrollHeight - 150)}`;
    this.tableScrollHeight = `${this.elRef.nativeElement.offsetHeight -
      (this.elRef.nativeElement.offsetTop - 100)
      }`;
  }
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.setScrollHeight();
    }, 10);
  }
  onRowClick(job) {
    this.selectedRowItem=job;
    this.jobCardService.getJobDetailsById(job.Id).subscribe(resp => {
      this.jobDetailList = resp;
    })
  }
  onAddFormClose()
  {
    this.display = false;
    this.loadDataLazy({
      first: this.first,
      rows: this.recordsPerPage,
    });
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['tableScrollHeight']) {
      this.setScrollHeight();

    }
  }
}
