import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataPager } from '../model/data-paget';
import { Sorting } from '../model/sorting-model';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private httpClient:HttpClient) { }
  save(data)
  {
    debugger;
    if(data.Id>0)
    {
      return this.httpClient.put('VehicleDetail',data);
    }
    else
    return this.httpClient.post('VehicleDetail',data);
  }
  get(id)
  {
    return this.httpClient.get<any>(`VehicleDetail/${id}`);
  }
  delete(id)
  {
    return this.httpClient.delete(`VehicleDetail/${id}`);
  }
  getAllVehicle({
    dataPager,
    sortQuery,
  }: {
    dataPager: DataPager;
    sortQuery: Sorting;
  
  }): Observable<any[]> {
    debugger;
    return this.httpClient.get<any[]>(
      "VehicleDetail",
      {
        params: {
          PageNumber: dataPager.pageNumber.toString(),
          PageSize: dataPager.pageSize.toString(),
          SortByOrder: sortQuery.sortOrder.toString(),
          SortByColumn: sortQuery.columnName,
          IsRequired: dataPager.isRequired.toString()
        },
      }
    );
  }
  getMasterData()
  {
    const params = new HttpParams()
  .set('category', 'InventoryCheck');
 
    var param="InventoryCheck";
    return this.httpClient.get<any>(`MasterData/${param}`);
  }
  getTransmitionType()
  {
    
    var param="TransmitionType";
    return this.httpClient.get<any>(`MasterData/${param}`);
  }
}
