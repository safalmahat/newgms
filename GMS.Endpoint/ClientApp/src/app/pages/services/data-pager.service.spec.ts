import { TestBed } from '@angular/core/testing';

import { DataPagerService } from './data-pager.service';

describe('DataPagerService', () => {
  let service: DataPagerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataPagerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
