import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Roles } from '../model/role';
import { User } from '../model/user';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  users: User[] = [];

  constructor(private httpClient: HttpClient) {
    // let user = {
    //     userId: 1, userName: "admin", password: "password", emailId: "admin@admin.com", birthDate: new Date('10/28/1992')
    // };
    // this.users.push(user);
  }

  /**
   * get user by user name and password
   * @param userName
   * @param password
   */
  getUserByUserNameAndPassword(userName: string, password: string) {
    let user = {
      userName: userName,
      password: password,
    };
    return this.httpClient.post("Token", user);
  }
  EmailPassword(_email: string) {
    return this.httpClient.get("user/ForgetPassword?email=" + _email);
  }
  deleteToken() {
    let tokenValue = JSON.parse(sessionStorage.currentUser).token;
   return this.httpClient.delete('Token/' + `${tokenValue}`);
  }
getAllRoles()
{
  return this.httpClient.get<Roles[]>('user/roles');
}
}
