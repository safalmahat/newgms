import { Injectable } from '@angular/core';
import { DataPager } from '../model/data-paget';

@Injectable({
  providedIn: 'root'
})
export class DataPagerService {

  constructor() { }
  getTotalPages(dataPager: DataPager): number {
    return dataPager.pageSize !== 0 ? Math.floor(dataPager.recordsCount / dataPager.pageSize) : 1;
  }
  getCurrentPage(currentRecords: number, recordsPerPage: number): number {
    return currentRecords === 0 ? 1 : Math.ceil(currentRecords / recordsPerPage) + 1;
  }
}
