import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataPager } from '../model/data-paget';
import { Sorting } from '../model/sorting-model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private httpClient: HttpClient) { }
  getCustomerById(id) {
    return this.httpClient.get<any>(`CustomerDetail/${id}`);
  }
  save(data) {
    if (data.Id > 0) {
      return this.httpClient.put('CustomerDetail', data);
    }
    else
      return this.httpClient.post('CustomerDetail', data);
  }
  delete(id) {
    return this.httpClient.delete(`CustomerDetail/${id}`);
  }
  getAllCustomer({
    dataPager,
    sortQuery,
  }: {
    dataPager: DataPager;
    sortQuery: Sorting;

  }): Observable<any[]> {
    return this.httpClient.get<any[]>(
      "CustomerDetail",
      {
        params: {
          PageNumber: dataPager.pageNumber.toString(),
          PageSize: dataPager.pageSize.toString(),
          SortByOrder: sortQuery.sortOrder.toString(),
          SortByColumn: sortQuery.columnName,
          IsReqired: dataPager.isRequired.toString()
        },
      }
    );
  }

}
