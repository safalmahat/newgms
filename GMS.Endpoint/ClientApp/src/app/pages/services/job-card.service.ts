import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataPager } from '../model/data-paget';
import { Sorting } from '../model/sorting-model';

@Injectable({
  providedIn: 'root'
})
export class JobCardService {

  constructor(private httpClient: HttpClient) { }
  save(data) {
    if (data.jobCard.Id > 0) {
      return this.httpClient.put(`JobCard`, data);
    }
    else
      return this.httpClient.post(`JobCard`, data);

  }
  getAllCustomer({
    dataPager,
    sortQuery,
  }: {
    dataPager: DataPager;
    sortQuery: Sorting;

  }): Observable<any[]> {
    return this.httpClient.get<any[]>(
      "JobCard",
      {
        params: {
          PageNumber: dataPager.pageNumber.toString(),
          PageSize: dataPager.pageSize.toString(),
          SortByOrder: sortQuery.sortOrder.toString(),
          SortByColumn: sortQuery.columnName,
        },
      }
    );
  }
  delete(id) {
    return this.httpClient.delete(`JobCard/${id}`);
  }
  getJobDetailsById(id) {
    return this.httpClient.get<any[]>(`JobCard/${id}/details`);
  }
  getJobCardById(id) {
    return this.httpClient.get<any>(`JobCard/${id}`);
  }
  generateJobNumber() {
    return this.httpClient.get<number>(`JobCard/generate/JobNumber`);
  }
  getServicingComplainData()
  {
    
    var param="ServicingComplain";
    return this.httpClient.get<any>(`MasterData/${param}`);
  }
}
