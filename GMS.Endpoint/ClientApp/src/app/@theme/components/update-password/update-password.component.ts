import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { UserService } from '../../../pages/user-management/services/user.service';

@Component({
  selector: 'ngx-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {
  updatePasswordForm: FormGroup;
  isSubmitted:boolean=false;
  isPasswordMatch:boolean=false;
  @Output() displayChange = new EventEmitter();
  submitButtonText:string="Save";
  isLoading:boolean=false;
  invalidOldPassword:string="";
  constructor(private fb: FormBuilder, private userService:UserService, private router:Router,
    private messageService:MessageService
    ) { }

  ngOnInit(): void {
    this.updatePasswordForm = this.fb.group({
      Id: '',
      Hash: ['', [Validators.required]],
      NewHash:['', [Validators.required]],
      ReNewHash: ['', [Validators.required]],
    });
  }
  onSaveClick()
  {
      let userId = JSON.parse(sessionStorage.currentUser).userId;
    this.isSubmitted = true;
    this.isLoading = true;
    this.submitButtonText = "Processing";
    if (!this.updatePasswordForm.valid) {
      this.isSubmitted = false;
      this.isLoading = false;
      this.submitButtonText = "Save";
      return;
    }
    if(!this.isPasswordMatch)
    {
      return;
    }
    this.updatePasswordForm.get("Id").setValue(userId);
    this.userService
    .updatePassword(this.updatePasswordForm.value)
    .subscribe(
      (resp) => {
        this.displayChange.emit(true);
        this.showSuccess();
        this.invalidOldPassword="";
        this.userService.deleteToken().subscribe(resp => {
          this.router.navigate(['../auth/login']);
          window.sessionStorage.clear();
          this.isSubmitted = false;
          this.isLoading = false;
          this.submitButtonText = "Save";

        })
       
      },
      (err: HttpErrorResponse) => {
        debugger;
        if (err.error.length === 1 && err.status === 400) {
          this.invalidOldPassword = err.error[0].errorMessage;
          this.isSubmitted = false;
          this.isLoading = false;
          this.submitButtonText = "Save";
        }
      }
    );
  }
  onCancelClick()
  {
this.displayChange.emit(true);
  }
  checkPasswordMatch() {
    let actualPswd = this.updatePasswordForm.get('NewHash').value;
    let matchPswd = this.updatePasswordForm.get('ReNewHash').value;
    if (actualPswd == matchPswd)
      this.isPasswordMatch = true;
    else
      this.isPasswordMatch = false;
  }
  showSuccess() {
    this.messageService.add({severity:'success', summary: 'Your password has been updated'});
}

}
