import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { UserService } from '../../../pages/user-management/services/user.service';

@Component({
  selector: 'ngx-edit-user-profile',
  templateUrl: './edit-user-profile.component.html',
  styleUrls: ['./edit-user-profile.component.scss']
})
export class EditUserProfileComponent implements OnInit {
  editProfileForm: FormGroup;
  isSubmitted: boolean = false;
  submitButtonText: string = 'Save';
  isLoading: boolean = false;
  @Output() displayChange = new EventEmitter();
  constructor(private fb: FormBuilder, private userService: UserService,
    private messageService:MessageService
    ) { }

  ngOnInit(): void {
    this.editProfileForm = this.fb.group({
      Id: '',
      UserName: ['', [Validators.required]],
      FirstName: ['', [Validators.required]],
      Email: ['', [Validators.required, Validators.email]],
      LastName: ['', [Validators.required]],
    });
    this.getUserDetail();
  }
  onSaveClick() {
    this.isSubmitted = true;
    this.isLoading = true;
    this.submitButtonText = "Processing";
    if (!this.editProfileForm.valid) {
      this.isSubmitted = false;
      this.isLoading = false;
      this.submitButtonText = "Save";
      return;
    }
    this.userService.update(this.editProfileForm.value).subscribe(res => {
      this.showSuccess();
      this.displayChange.emit(true);
      this.isSubmitted = false;
      this.isLoading = false;
      this.submitButtonText = "Save";
    });
  }

  onCancelClick() {
this.displayChange.emit(true);
  }
  getUserDetail() {
    if (sessionStorage.currentUser != null) {
      let userId = JSON.parse(sessionStorage.currentUser).userId;
      this.userService.getUserById(userId).subscribe(resp => {
        if (resp != null) {
          this.editProfileForm.patchValue({
            UserName: resp.userName,
            FirstName: resp.firstName,
            Email: resp.email,
            LastName: resp.lastName,
            Id: resp.id
          })
        }
        this.isLoading=false;
        this.isSubmitted=false;
        this.submitButtonText="Save";
      })
    }
  }
  showSuccess() {
    this.messageService.add({severity:'success', summary: 'Profile updated successfully.'});
}
}
