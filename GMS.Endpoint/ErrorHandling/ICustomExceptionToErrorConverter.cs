﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace GMS.Endpoint.ErrorHandling
{
    public interface ICustomExceptionToErrorConverter
    {
        /// <summary>
        /// Implementation should return true only for exception types that can be handled by ConvertExceptionToError method
        /// </summary>
        /// <param name="exceptionType"></param>
        /// <returns></returns>
        bool CanConvertException(Type exceptionType);
        /// <summary>
        /// Converts exception into collection of Error objects and sets HTTP response status code
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        (IEnumerable<Error> errors, HttpStatusCode httpStatusCode) ConvertExceptionToError(Exception exception);
    }
}
