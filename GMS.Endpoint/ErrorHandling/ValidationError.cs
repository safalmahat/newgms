﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMS.Endpoint.ErrorHandling
{
    public class ValidationError : Error
    {
        public string PropertyName { get; set; }
    }
}
