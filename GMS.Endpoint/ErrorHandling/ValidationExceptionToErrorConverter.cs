﻿
using FluentValidation;
using Gms.Business.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace GMS.Endpoint.ErrorHandling
{
      public class ValidationExceptionToErrorConverter : ICustomExceptionToErrorConverter
    {
        public bool CanConvertException(Type exceptionType)
        {
            var httpStatusCode = MatchExceptionTypeToHttpStatusCode(exceptionType);

            if (httpStatusCode == HttpStatusCode.InternalServerError)
            {
                return false;
            }

            return true;
        }

        public (IEnumerable<Error> errors, HttpStatusCode httpStatusCode) ConvertExceptionToError(Exception exception)
        {
            var errors = new List<Error>();
            HttpStatusCode httpStatusCode = MatchExceptionTypeToHttpStatusCode(exception.GetType());

            if (exception is ValidationException validationException)
            {
                foreach (var validationFailure in validationException.Errors)
                {
                    var canConvertErrorCodeToInt = int.TryParse(validationFailure.ErrorCode, out int errorCode);

                    errors.Add(new ValidationError
                    {
                        ErrorCode = canConvertErrorCodeToInt ? Convert.ToInt32(validationFailure.ErrorCode) : (int)ErrorCode.ValidationError,
                        ErrorMessage = validationFailure.ErrorMessage,
                        PropertyName = validationFailure.PropertyName
                    });
                }
            }

            return (errors, httpStatusCode);
        }

        private static HttpStatusCode MatchExceptionTypeToHttpStatusCode(Type exceptionType)
        {
            HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError;

            if (typeof(ValidationException) == exceptionType)
            {
                httpStatusCode = HttpStatusCode.BadRequest;
            }

            return httpStatusCode;
        }
    }
}
