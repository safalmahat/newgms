﻿using Gms.Business.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
namespace GMS.Endpoint.ErrorHandling
{
    /// <summary>
    /// Implements ICustomExceptionToErrorConverter for custom exceptions that have error code and error message
    /// It uses exception's message property the error's message and exception HResult as error's code.
    /// </summary>
    public class SimpleExceptionToErrorConverter : ICustomExceptionToErrorConverter
    {
        public bool CanConvertException(Type exceptionType)
        {
            var httpStatusCode = MatchExceptionTypeToHttpStatusCode(exceptionType);

            if (httpStatusCode == HttpStatusCode.InternalServerError)
            {
                return false;
            }

            return true;
        }

        public (IEnumerable<Error> errors, HttpStatusCode httpStatusCode) ConvertExceptionToError(Exception exception)
        {
            var errors = new List<Error>();
            HttpStatusCode httpStatusCode = MatchExceptionTypeToHttpStatusCode(exception.GetType());

            errors.Add(new Error
            {
                ErrorCode = exception.HResult,
                ErrorMessage = exception.Message
            });

            return (errors, httpStatusCode);
        }

        private static HttpStatusCode MatchExceptionTypeToHttpStatusCode(Type exceptionType)
        {
            HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError;

            if (typeof(BadRequestException) == exceptionType)
            {
                httpStatusCode = HttpStatusCode.BadRequest;
            }
            else if (typeof(LockedResourceException) == exceptionType)
            {
                httpStatusCode = HttpStatusCode.Locked;
            }
            else if (typeof(UnauthorizedException) == exceptionType)
            {
                httpStatusCode = HttpStatusCode.Unauthorized;
            }
            //else if (typeof(ForbiddenException) == exceptionType)
            //{
            //    httpStatusCode = HttpStatusCode.Forbidden;
            //}
            return httpStatusCode;
        }
    }
}
