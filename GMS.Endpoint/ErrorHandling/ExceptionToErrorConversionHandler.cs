﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace GMS.Endpoint.ErrorHandling
{
    public interface IExceptionToErrorConversionHandler
    {
        (IEnumerable<Error> errors, HttpStatusCode httpStatusCode) ConvertExceptionToError(Exception exception);
    }

    public class ExceptionToErrorConversionHandler : IExceptionToErrorConversionHandler
    {
        private ILogger _logger;
        private IEnumerable<ICustomExceptionToErrorConverter> _exceptionToErrorConverters;

        public ExceptionToErrorConversionHandler(
            ILoggerFactory loggerFactory,
            IEnumerable<ICustomExceptionToErrorConverter> exceptionToErrorConverters
            )
        {
            _logger = loggerFactory.CreateLogger(typeof(ErrorHandlingMiddleware).FullName);
            _exceptionToErrorConverters = exceptionToErrorConverters;
        }

        public (IEnumerable<Error> errors, HttpStatusCode httpStatusCode) ConvertExceptionToError(Exception exception)
        {
            //In order to convert exception into error ICustomExceptionToErrorConverter has to be implemented
            //Or custom exception can be added to any existing converter (explicitly or via common interface)
            var exceptionHandler = _exceptionToErrorConverters.SingleOrDefault(o => o.CanConvertException(exception.GetType()));

            if (exceptionHandler != null)
            {
                _logger.LogInformation(exception, exception.Message); //Logged as info since the exception is known validation error
                return exceptionHandler.ConvertExceptionToError(exception);
            }
            else
            {
                //any exception that has no ICustomExceptionToErrorConverter will be returned as InternalServerError
                //and there should be none - all exceptions should be converted into client friendly errors
                _logger.LogError(exception, "Error during request processing. This exception type requires converter to client friendly error object.");
                return (null, HttpStatusCode.InternalServerError);
            }
        }
    }
}
