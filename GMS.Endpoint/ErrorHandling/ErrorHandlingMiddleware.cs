﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GMS.Endpoint.ErrorHandling
{
    public interface IErrorHandlingMiddleware { }
    public class ErrorHandlingMiddleware : IMiddleware, IErrorHandlingMiddleware
    {
        private IExceptionToErrorConversionHandler _exceptionToErrorConversionHandler;
        private IActionResultExecutor<ObjectResult> _actionResultExecutor;
        public readonly ActionDescriptor EmptyActionDescriptor = new ActionDescriptor();

        public ErrorHandlingMiddleware(
            IExceptionToErrorConversionHandler exceptionHandldler,
            IActionResultExecutor<ObjectResult> actionResultExecutor)
        {
            _exceptionToErrorConversionHandler = exceptionHandldler;
            _actionResultExecutor = actionResultExecutor;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception exception)
            {
                var (errors, httpStatusCode) = _exceptionToErrorConversionHandler.ConvertExceptionToError(exception);

                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)httpStatusCode;

                var routeData = context.GetRouteData();
                var actionContext = new ActionContext(context, routeData, EmptyActionDescriptor);
                await _actionResultExecutor.ExecuteAsync(actionContext, new ObjectResult(errors));
            }
        }
    }
}
