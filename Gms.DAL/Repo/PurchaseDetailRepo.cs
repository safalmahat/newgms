﻿using Gms.DAL.DTO;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gms.DAL.Repo
{
    public interface IPurchaseDetailRepo
    {
        Task Save(PurchaseDetailDto data, IDbConnection con = null);
        Task Update(PurchaseDetailDto data, IDbConnection con = null);
        Task<List<PurchaseDetailDto>> GetPurchaseDetailsByPurchaseId(int purchaseId);
    }
    class PurchaseDetailRepo : IPurchaseDetailRepo
    {
        private IDapperManager _dapperManager;
        public PurchaseDetailRepo(IDapperManager dapperManager)
        {
            _dapperManager = dapperManager;
        }
        public async Task Save(PurchaseDetailDto data, IDbConnection con = null)
        {
            string query = @"insert into PurchaseDetail(PurchaseId,ProductId,Quantity,UnitId,Price,TotalPrice,created_by,created_on) values(@PurchaseId,@ProductId,@Quantity,@UnitId,@Price,@TotalPrice,@created_by,@created_on)";
            await _dapperManager.ExecuteAsync(con, query, data);
        }

        public async Task Update(PurchaseDetailDto data, IDbConnection con = null)
        {
            string query = @"update PurchaseDetail set ProductId=@ProductId,quantity=@quantity,UnitId=@UnitId,price=@price,TotalPrice=@TotalPrice,updated_by=@updated_by,updated_on=@updated_on where id=@id";
            await _dapperManager.ExecuteAsync(con, query, data);
        }
        public async Task<List<PurchaseDetailDto>> GetPurchaseDetailsByPurchaseId(int purchaseId)
        {
            string query = @"select pd.*, p.name as productName, u.name as unitName from purchase ps
                left join PurchaseDetail pd on ps.id=pd.PurchaseId
                left join product p on p.id=pd.ProductId
                left join units u on u.id=pd.UnitId where pd.PurchaseId=@purchase_id";
            var result = await _dapperManager.QueryAsync<PurchaseDetailDto>(query, new { purchase_id = purchaseId });
            return result.ToList();
        }
    }
}
