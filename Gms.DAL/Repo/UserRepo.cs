﻿using Gms.DAL.Component;
using Gms.DAL.DTO;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gms.DAL.Repo
{
    public interface IUserRepo
    {
        Task<UserDto> GetUser(string userName);
        Task<UserDto> GetUserById(int userId);
        Task<UserDto> GetUserByEmail(string email);
        Task<int> SaveUser(UserDto userModel);
        Task UpdateUser(UserDto updateModel);
        Task<List<UserDto>> GetAllUsers(PagedDatasource pagedDatasource);
        Task UpdatePassword(UserDto updateModel);
        Task<UserDto> CheckUserExist(string userName);
        Task<string> GetPassword(int? userId);
        Task<List<RolesDto>> GetAllRoles();
        Task InsertUserInRoles(UserRolesDto data);
        Task TaskDeleteAllRoles(int userId);
        Task<List<string>> GetAllRolesByUserId(int userId);
    }
    public class UserRepo: IUserRepo
    {
        private IDapperManager _dapperManager;
        private readonly IPaginationQueryBuilder _paginationQueryBuilder;
        public UserRepo(IDapperManager dapperManager, IPaginationQueryBuilder paginationQueryBuilder)
        {
            _dapperManager = dapperManager;
            _paginationQueryBuilder = paginationQueryBuilder;
        }

        public async Task<UserDto> CheckUserExist(string userName)
        {
            return await _dapperManager.QuerySingleOrDefaultAsync<UserDto>("select * from [UserInfo] where user_name=@userName", new { userName = userName });
        }

        public async Task<List<UserDto>> GetAllUsers(PagedDatasource pagedDatasource)
        {
            if (pagedDatasource.ISREQUIRED)
            {
                var dt = await _dapperManager.QueryAsync<UserDto>("SELECT * FROM USERINFO");
                return dt.ToList();
            }
            else
            {
                string query = @"select  M.*,count(1) OVER() AS ROW_COUNT FROM(SELECT * FROM USERINFO) M";
                query = string.Format(query + "{0}", pagedDatasource.FILTERQUERY);
                string pagedQuery = _paginationQueryBuilder.ToPagedQuery(query, pagedDatasource);
                var data = await _dapperManager.QueryAsync<UserDto>(pagedQuery, new
                {
                    PAGENUMBER = pagedDatasource.PAGENUMBER,
                    PAGESIZE = pagedDatasource.PAGESIZE,
                    SORTBYCOLUMN = pagedDatasource.SORTBYCOLUMN,
                });
                return data.ToList();
            }
        }

        public async Task<UserDto> GetUser(string userName)
        {
            string query = "SELECT * FROM USERINFO WHERE USER_NAME = @User_Name";
            var result = await _dapperManager.QuerySingleOrDefaultAsync<UserDto>(query, new { User_Name = userName });
            return result;
        }

        public Task<UserDto> GetUserByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public async Task<UserDto> GetUserById(int userId)
        {
            string query = @"select * from userinfo where id=@Id";
            var result = await _dapperManager.QuerySingleOrDefaultAsync<UserDto>(query, new { Id = userId });
            return result;
        }

        public async Task<int> SaveUser(UserDto userModel)
        {
            string query = @"INSERT INTO UserInfo(user_name, hash, first_name, last_name, 
			email,password_failure_count, created_by,created_on,updated_by,updated_on) VALUES(@user_name, @hash, @first_name, 
		    @last_name, @email,@password_failure_count, @created_by,@created_on,@updated_by,@updated_on) SELECT SCOPE_IDENTITY()";

            int insertedUserId = await _dapperManager.QuerySingleOrDefaultAsync<int>(query, userModel);
            return insertedUserId;
        }

        public async Task UpdateUser(UserDto updateModel)
        {
            string query = @"UPDATE UserInfo SET first_name = @first_name, last_name = @last_name, 
			email = @email, User_Name=@User_Name WHERE id = @id;";

            await _dapperManager.ExecuteAsync(query, updateModel);
        }
        public async Task UpdatePassword(UserDto updateModel)
        {
            string query = @"UPDATE UserInfo SET hash = @hash WHERE id = @id;";
            await _dapperManager.ExecuteAsync(query, updateModel);
        }
        public async Task<string> GetPassword(int? userId)
        {
            string query = @"select hash from UserInfo WHERE id = @id;";
          return  await _dapperManager.QuerySingleOrDefaultAsync<string>(query, new { id = userId });
        }
        public async Task<List<RolesDto>> GetAllRoles()
        {
            string query = @"select * from Roles WHERE status=1";
            var data= await _dapperManager.QueryAsync<RolesDto>(query);
            return data.ToList();
        }
        public async Task InsertUserInRoles(UserRolesDto data)
        {
            string query = @"insert into UserRoles(UserId,RoleId) values(@UserId,@RoleId)";
            await _dapperManager.ExecuteAsync(query, data);
        }
        public async Task TaskDeleteAllRoles(int userId)
        {
            string query = @"delete from UserRoles where UserId=@UserId";
            await _dapperManager.ExecuteAsync(query, new { UserId= userId });
        }
        public async Task<List<string>> GetAllRolesByUserId(int userId)
        {
            string query = @"select r.name  from UserRoles ur 
left join Roles r on ur.RoleId=r.id where ur.UserId=@UserId";
           var data= await _dapperManager.QueryAsync<string>(query, new { UserId = userId });
            return data.ToList();
        }
    }
}
