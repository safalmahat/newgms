﻿using Gms.DAL.Component;
using Gms.DAL.DTO;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gms.DAL.Repo
{
    public interface IUnitRepo
    {
        Task<List<UnitDto>> GetUnits(PagedDatasource pagedDataSource);
        Task Save(UnitDto data);
        Task<UnitDto> Get(int id);
        Task Update(UnitDto data);
        Task Delete(int id);

    }
    public class UnitRepo : IUnitRepo
    {
        private IDapperManager _dapperManager;
        private readonly IPaginationQueryBuilder _paginationQueryBuilder;
        public UnitRepo(IDapperManager dapperManager, IPaginationQueryBuilder paginationQueryBuilder)
        {
            _dapperManager = dapperManager;
            _paginationQueryBuilder = paginationQueryBuilder;
        }
        public async Task<List<UnitDto>> GetUnits(PagedDatasource pagedDataSource)
        {
            if (!pagedDataSource.ISREQUIRED)
            {
                var result = await _dapperManager.QueryAsync<UnitDto>("Select * from Units");
                return result.ToList();
            }
            else
            {
                string query = @"select M.*,count(1) OVER() AS ROW_COUNT FROM (Select * from Units where status=1	) M	";

                query = string.Format(query + "{0}", pagedDataSource.FILTERQUERY);
                string pagedQuery = _paginationQueryBuilder.ToPagedQuery(query, pagedDataSource);
                var result = await _dapperManager.QueryAsync<UnitDto>(pagedQuery, new
                {
                    PAGENUMBER = pagedDataSource.PAGENUMBER,
                    PAGESIZE = pagedDataSource.PAGESIZE,
                    SORTBYCOLUMN = pagedDataSource.SORTBYCOLUMN
                });
                return result.ToList();
            }

        }

        public async Task Save(UnitDto data)
        {
            await _dapperManager.ExecuteAsync("insert into units(name,code,status,created_by,created_on) values(@name,@code,@status,@created_by,@created_on)", data);
        }

        public async Task Update(UnitDto data)
        {
            await _dapperManager.ExecuteAsync("update units set name=@name,code=@code, updated_by=@updated_by,updated_on=@updated_on where id=@id", data);
        }
        public async Task Delete(int id)
        {
            await _dapperManager.ExecuteAsync("update units set status=0 where id=@id", new { id = id });
        }

        public async Task<UnitDto> Get(int id)
        {
            var result = await _dapperManager.QuerySingleOrDefaultAsync<UnitDto>("Select * from Units where id=@id", new { id = id });
            return result;
        }
    }
}
