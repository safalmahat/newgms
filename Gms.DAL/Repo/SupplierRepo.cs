﻿using Gms.DAL.Component;
using Gms.DAL.DTO;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gms.DAL.Repo
{
    public interface ISupplierRepo
    {
        Task<List<SupplierDto>> Get(PagedDatasource pagedDataSource);
        Task Save(SupplierDto data);
        Task<SupplierDto> Get(int id);
        Task Update(SupplierDto data);
        Task Delete(int id);
    }
    public class SupplierRepo : ISupplierRepo
    {
        private IDapperManager _dapperManager;
        private readonly IPaginationQueryBuilder _paginationQueryBuilder;
        public SupplierRepo(IDapperManager dapperManager, IPaginationQueryBuilder paginationQueryBuilder)
        {
            _dapperManager = dapperManager;
            _paginationQueryBuilder = paginationQueryBuilder;
        }
        public async Task Delete(int id)
        {
            await _dapperManager.ExecuteAsync("delete from suppliers where id=@id", new { id = id });
        }

        public async Task<List<SupplierDto>> Get(PagedDatasource pagedDataSource)
        {
            string query = @"select M.*,count(1) OVER() AS ROW_COUNT FROM (select * from suppliers	) M	";
            query = string.Format(query + "{0}", pagedDataSource.FILTERQUERY);
            if (pagedDataSource.ISREQUIRED)
            {

                string pagedQuery = _paginationQueryBuilder.ToPagedQuery(query, pagedDataSource);
                var result = await _dapperManager.QueryAsync<SupplierDto>(pagedQuery, new
                {
                    PAGENUMBER = pagedDataSource.PAGENUMBER,
                    PAGESIZE = pagedDataSource.PAGESIZE,
                    SORTBYCOLUMN = pagedDataSource.SORTBYCOLUMN,
                });
                return result.ToList();
            }
            else
            {
                var result = await _dapperManager.QueryAsync<SupplierDto>(query);
                return result.ToList();
            }


        }

        public async Task<SupplierDto> Get(int id)
        {
            return await _dapperManager.QuerySingleOrDefaultAsync<SupplierDto>("select * from suppliers where id=@id", new { id = id });
        }

        public async Task Save(SupplierDto data)
        {
            string query = @"insert into suppliers(name,address,contact_person,phone_number,mobile_number,email,status,created_by,created_on) values(@name,@address,@contact_person,@phone_number,@mobile_number,@email,@status,@created_by,@created_on)";
            await _dapperManager.ExecuteAsync(query, data);
        }

        public async Task Update(SupplierDto data)
        {
            string query = @"update suppliers set name=@name,address=@address,contact_person=@contact_person,phone_number=@phone_number,mobile_number=@mobile_number,email=@email,updated_by=@updated_by,updated_on=@updated_on where id=@id";
            await _dapperManager.ExecuteAsync(query, data);
        }
    }
}
