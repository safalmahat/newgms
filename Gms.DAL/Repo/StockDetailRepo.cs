﻿using Gms.DAL.DTO;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Gms.DAL.Repo
{
    public interface IStockDetailRepo
    {
        Task<StockDetailDto> GetStockDetailByProductId(int productId, IDbConnection con=null);
        Task Save(StockDetailDto data, IDbConnection con = null);
        Task Update(StockDetailDto data, IDbConnection con = null);
    }
   public class StockDetailRepo: IStockDetailRepo
    {
        private IDapperManager _dapperManager;
        public StockDetailRepo(IDapperManager dapperManager)
        {
            _dapperManager = dapperManager;
        }
        public async Task<StockDetailDto> GetStockDetailByProductId(int productId, IDbConnection con = null)
        {
            string query = "select * from StockDetail where ProductId=@ProductId";
         return   await _dapperManager.QuerySingleOrDefaultAsync<StockDetailDto>(con,query, new  { ProductId=productId });
        }
        public async Task Save(StockDetailDto data, IDbConnection con = null)
        {
            string query = @"insert into StockDetail(ProductId,Quantity,Created_By,Created_On) values(@ProductId,@Quantity,@Created_By,@Created_On)";
          await  _dapperManager.ExecuteAsync(con,query, data);
        }
        public async Task Update(StockDetailDto data, IDbConnection con = null)
        {
            string query = @"update StockDetail set Quantity=@Quantity where ProductId=@ProductId";
            await _dapperManager.ExecuteAsync(con,query, data);
        }
    }
}
