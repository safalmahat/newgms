﻿using Gms.DAL.Component;
using Gms.DAL.DTO;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gms.DAL.Repo
{
    public interface IProductRepo
    {
        Task<List<dynamic>> GetAllProduct(PagedDatasource pagedDataSource);
        Task Save(ProductDto data);
        Task<ProductDto> Get(int id);
        Task Update(ProductDto data);
        Task Delete(int id);
        Task<List<ProductDto>> GetAvailableProduct();
    }
    public class ProductRepo : IProductRepo
    {
        private IDapperManager _dapperManager;
        private readonly IPaginationQueryBuilder _paginationQueryBuilder;
        public ProductRepo(IDapperManager dapperManager, IPaginationQueryBuilder paginationQueryBuilder)
        {
            _dapperManager = dapperManager;
            _paginationQueryBuilder = paginationQueryBuilder;
        }
        public async Task Delete(int id)
        {
            await _dapperManager.ExecuteAsync("update product set status=0 where id=@id", new { id = id });
        }

        public async Task<ProductDto> Get(int id)
        {
            var result = await _dapperManager.QuerySingleOrDefaultAsync<ProductDto>("Select * from product where id=@id", new { id = id });
            return result;
        }

        public async Task<List<dynamic>> GetAllProduct(PagedDatasource pagedDataSource)
        {

            string query = string.Empty;
            if(pagedDataSource.ISREQUIRED)
            {
                 query = @"select M.*,count(1) OVER() AS ROW_COUNT FROM (
select p.*,pc.name as category from product p 
left join ProductCategory pc on pc.id=p.categoryId
where p.status=1	) M		";
                //query = string.Format(query + "{0}", pagedDataSource.FILTERQUERY);
                string pagedQuery = _paginationQueryBuilder.ToPagedQuery(query, pagedDataSource);
                var result = await _dapperManager.QueryAsync<dynamic>(pagedQuery, new
                {
                    PAGENUMBER = pagedDataSource.PAGENUMBER,
                    PAGESIZE = pagedDataSource.PAGESIZE,
                    SORTBYCOLUMN = pagedDataSource.SORTBYCOLUMN,
                });
                return result.ToList();
            }
            else
            {
                query = @"select p.*,pc.name as category from product p 
left join ProductCategory pc on pc.id=p.categoryId
where p.status=1	";
                var result = await _dapperManager.QueryAsync<dynamic>(query, new
                {
                    PAGENUMBER = pagedDataSource.PAGENUMBER,
                    PAGESIZE = pagedDataSource.PAGESIZE,
                    SORTBYCOLUMN = pagedDataSource.SORTBYCOLUMN,
                });
                return result.ToList();
            }
                 
           
            }
        public async Task Save(ProductDto data)
        {
            await _dapperManager.ExecuteAsync("insert into product(code,CategoryId,stock, name,ActualPrice,discountPercentage, price, description,thumbnail, status,created_by,created_on) values(@code,@CategoryId,@stock, @name,@ActualPrice,@DiscountPercentage, @price, @description,@thumbnail, @status,@created_by,@created_on)", data);
        }

        public async Task Update(ProductDto data)
        {
            await _dapperManager.ExecuteAsync("update product set code = @code,CategoryId=@CategoryId,stock=@stock,thumbnail=@thumbnail,name=@name, price=@price,ActualPrice=@ActualPrice, DiscountPercentage=@DiscountPercentage,description=@description, updated_by=@updated_by,updated_on=@updated_on where id=@id", data);
        }
        public async Task<List<ProductDto>> GetAvailableProduct()
        {
            string query = @"select p.* from Product p 
left join StockDetail sd on p.id=sd.ProductId
where sd.Quantity>0 and p.Status=1

";
            var data= await _dapperManager.QueryAsync<ProductDto>(query);
            return data.ToList();
        }
    }
}
