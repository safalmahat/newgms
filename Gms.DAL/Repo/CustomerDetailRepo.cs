﻿using Gms.DAL.Component;
using Gms.DAL.DTO;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gms.DAL.Repo
{
    public interface ICustomerDetailRepo
    {
      Task<List<CustomerDetailsDto>> GetAllCustomer(PagedDatasource pagedDatasource);
        Task Save(CustomerDetailsDto data);
        Task Delete(int id);
        Task<CustomerDetailsDto> GetCustomerById(int id);
        Task Update(CustomerDetailsDto data);
    }
    public class CustomerDetailRepo : ICustomerDetailRepo
    {
        private readonly IDapperManager _dapperManager;
        private readonly IPaginationQueryBuilder _paginationQueryBuilder;
        public CustomerDetailRepo(IDapperManager dapperManager, IPaginationQueryBuilder paginationQueryBuilder)
        {
            _dapperManager = dapperManager;
            _paginationQueryBuilder = paginationQueryBuilder;
        }
        public async Task<List<CustomerDetailsDto>> GetAllCustomer(PagedDatasource pagedDatasource)
        {
            if(pagedDatasource.ISREQUIRED)
            {
                var dt = await _dapperManager.QueryAsync<CustomerDetailsDto>("SELECT * FROM CUSTOMERDETAILS");
                return dt.ToList();
            }
            else
            {
                string query = @"select  M.*,count(1) OVER() AS ROW_COUNT FROM(SELECT * FROM CUSTOMERDETAILS) M";
                query = string.Format(query + "{0}", pagedDatasource.FILTERQUERY);
                string pagedQuery = _paginationQueryBuilder.ToPagedQuery(query, pagedDatasource);
                var data = await _dapperManager.QueryAsync<CustomerDetailsDto>(pagedQuery, new
                {
                    PAGENUMBER = pagedDatasource.PAGENUMBER,
                    PAGESIZE = pagedDatasource.PAGESIZE,
                    SORTBYCOLUMN = pagedDatasource.SORTBYCOLUMN,
                });
                return data.ToList();
            }

        }

        public async Task Save(CustomerDetailsDto data)
        {
            string query = "INSERT INTO CUSTOMERDETAILS (FULL_NAME,MOBILE_NUMBER,PHONE_NUMBER,EMAIL,ADDRESS,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON) VALUES(@FULL_NAME,@MOBILE_NUMBER,@PHONE_NUMBER,@EMAIL,@ADDRESS,@CREATED_BY,@CREATED_ON,@UPDATED_BY,@UPDATED_ON)";
           await _dapperManager.ExecuteAsync(query, data);
        }
        public async Task Delete(int id)
        {
            string query = "DELETE FROM CUSTOMERDETAILS WHERE ID=@ID";
            await _dapperManager.ExecuteAsync(query, new { Id = id });
        }

        public async Task<CustomerDetailsDto> GetCustomerById(int id)
        {
            string query = "SELECT * FROM CUSTOMERDETAILS WHERE ID=@ID";
           return await _dapperManager.QuerySingleOrDefaultAsync<CustomerDetailsDto>(query, new { Id = id });
        }

        public async Task Update(CustomerDetailsDto data)
        {
            try
            {
                string query = @"UPDATE  CUSTOMERDETAILS SET FULL_NAME=@FULL_NAME,MOBILE_NUMBER=@MOBILE_NUMBER,PHONE_NUMBER=@PHONE_NUMBER,
EMAIL=@EMAIL,ADDRESS=@ADDRESS,UPDATED_BY=@UPDATED_BY,UPDATED_ON=@UPDATED_ON WHERE ID=@ID";
                await _dapperManager.ExecuteAsync(query, data);
            }
            catch(Exception ex)
            {

            }
          
           
        }
    }
}
