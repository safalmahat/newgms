﻿using Gms.DAL.Component;
using Gms.DAL.DTO;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gms.DAL.Repo
{
    public interface IJobCardRepo
    {
        Task<List<dynamic>> GetAllJob(PagedDatasource pagedDatasource);
        Task<List<JobCardDetailsDto>> GetJobCardDetailsByJobId(int id);
        Task<int> SaveJobCard(JobCardDto data, IDbConnection con=null);
        Task SaveJobCardDetail(JobCardDetailsDto data, IDbConnection con = null);
        Task<JobCardDto> GetjobCard(int id);
        Task SaveJobCardInventoryDetail(JobCardInventoryDetailDto data, IDbConnection con = null);
        Task<List<JobCardInventoryDetailDto>> GetJobCardInventoryDetailsByJobId(int id);
        Task UpdateJobCard(JobCardDto data);
        Task UpdateJobCardDetail(JobCardDetailsDto data);
        Task UpdateJobCardInventoryDetail(JobCardInventoryDetailDto data);
        Task SaveJobCardInventoryCheckDetail(JobCardInventoryCheckDetailsDto data, IDbConnection con = null);
        Task UpdateJobCardInventoryCheckDetail(JobCardInventoryCheckDetailsDto data, IDbConnection con = null);
        Task<List<JobCardInventoryCheckDetailsDto>> GetInventoryCheckItemsByJobId(int jobId);
        Task SaveJobCardLabourDetail(JobCardLabourDetailDto data, IDbConnection con = null);
        Task UpdateJobCardLabourDetail(JobCardLabourDetailDto data, IDbConnection con = null);
        Task<List<JobCardLabourDetailDto>> GetLabourByJobId(int jobId);
        Task<int> GetTotalJobCard();
    }


   public class JobCardRepo: IJobCardRepo
    {
        private readonly IDapperManager _dapperManager;
        private readonly IPaginationQueryBuilder _paginationQueryBuilder;
        public JobCardRepo(IDapperManager dapperManager, IPaginationQueryBuilder paginationQueryBuilder)
        {
            _dapperManager = dapperManager;
            _paginationQueryBuilder = paginationQueryBuilder;
        }

        public async Task<List<dynamic>> GetAllJob(PagedDatasource pagedDatasource)
        {
            string query = @"select  M.*,count(1) OVER() AS ROW_COUNT FROM(Select j.*,v.Registration_Number,c.Full_Name as Customer_Name from JobCard j  
join VehicleDetails v on v.id=j.vehicle_id
join CustomerDetails c on c.Id=v.Customer_Id) M";
            query = string.Format(query + "{0}", pagedDatasource.FILTERQUERY);
            string pagedQuery = _paginationQueryBuilder.ToPagedQuery(query, pagedDatasource);
            var data = await _dapperManager.QueryAsync<dynamic>(pagedQuery, new
            {
                PAGENUMBER = pagedDatasource.PAGENUMBER,
                PAGESIZE = pagedDatasource.PAGESIZE,
                SORTBYCOLUMN = pagedDatasource.SORTBYCOLUMN,
            });
            return data.ToList();
        }
        public async Task<List<JobCardDetailsDto>> GetJobCardDetailsByJobId(int id)
        {
            string query = @"SELECT * FROM JobCardDetails WHERE JOB_ID=@ID";
            var data = await _dapperManager.QueryAsync<JobCardDetailsDto>(query, new { Id = id });
            return data.ToList();
        }

        public async Task<int> SaveJobCard(JobCardDto data, IDbConnection con = null)
        {

            string query = @"insert into jobCard (job_number,vehicle_id,km_reading,Fuel_Quantity,created_by,created_on,updated_by,updated_on) values(@job_number,@vehicle_id,@km_reading,@Fuel_Quantity,@created_by,@created_on,@updated_by,@updated_on) SELECT SCOPE_IDENTITY()";
            int id = await _dapperManager.QuerySingleOrDefaultAsync<int>(query, data);
            return id;
        }
        public async Task SaveJobCardDetail(JobCardDetailsDto data, IDbConnection con = null)
        {
              string query = @"insert into JobCardDetails (job_id,Complain_Description,created_by,created_on,updated_by,updated_on) values(@job_id,@Complain_Description,@created_by,@created_on,@updated_by,@updated_on)";
             await _dapperManager.QuerySingleOrDefaultAsync<int>(query, data);
        }
        public async Task SaveJobCardInventoryDetail(JobCardInventoryDetailDto data, IDbConnection con = null)
        {
            try
            {
                string query = @"insert into JobCardInventoryDetail (job_id,Product,Price,Unit,Amount,created_by,created_on,updated_by,updated_on) values(@job_id,@Product,@Price,@Unit,@Amount,@created_by,@created_on,@updated_by,@updated_on)";
                await _dapperManager.QuerySingleOrDefaultAsync<int>(query, data);
            }
            catch(Exception ex)
            {

            }
        
        }

        public async Task<JobCardDto> GetjobCard(int id)
        {
            return await _dapperManager.QuerySingleOrDefaultAsync<JobCardDto>("select * from jobCard where id=@id", new { id = id });
        }
        public async Task<List<JobCardInventoryDetailDto>> GetJobCardInventoryDetailsByJobId(int id)
        {
            string query = @"SELECT * FROM JobCardInventoryDetail WHERE JOB_ID=@ID";
            var data = await _dapperManager.QueryAsync<JobCardInventoryDetailDto>(query, new { Id = id });
            return data.ToList();
        }

        public async Task UpdateJobCard(JobCardDto data)
        {
            string query = @"UPDATE JOBCARD SET VEHICLE_ID=@VEHICLE_ID, KM_READING=@KM_READING,Fuel_Quantity=@Fuel_Quantity,
updated_by=@updated_by,updated_on=@updated_on
WHERE ID=@ID";
          await  _dapperManager.ExecuteAsync(query, data);
        }
        public async Task UpdateJobCardDetail(JobCardDetailsDto data)
        {
            string query = @"UPDATE JobCardDetails SET Complain_Description=@Complain_Description where ID=@ID";
            await _dapperManager.ExecuteAsync(query, data);
        }
        public async Task UpdateJobCardInventoryDetail(JobCardInventoryDetailDto data)
        {
            string query = @"UPDATE JobCardInventoryDetail SET Product=@Product,unit=@unit,Price=@Price,amount=@unit,updated_by=@updated_by,updated_on=@updated_on  where ID=@ID";
            await _dapperManager.ExecuteAsync(query, data);
        }
        #region inventory check
        public async Task SaveJobCardInventoryCheckDetail(JobCardInventoryCheckDetailsDto data, IDbConnection con = null)
        {
            
                string query = @"insert into JobCardInventoryCheckDetails (job_id,item,unit,created_by,created_on,updated_by,updated_on) values(@job_id,@item,@unit,@created_by,@created_on,@updated_by,@updated_on)";
                await _dapperManager.ExecuteAsync(query, data);
          
        }
        public async Task UpdateJobCardInventoryCheckDetail(JobCardInventoryCheckDetailsDto data, IDbConnection con = null)
        {
            string query = @"update JobCardInventoryCheckDetails set item=@item,unit=@unit,updated_by=@updated_by,updated_on=@updated_on where id=@id";
            await _dapperManager.QuerySingleOrDefaultAsync<int>(query, data);
        }
        public async Task<List<JobCardInventoryCheckDetailsDto>> GetInventoryCheckItemsByJobId(int jobId)
        {
            var data = await _dapperManager.QueryAsync<JobCardInventoryCheckDetailsDto>("select * from JobCardInventoryCheckDetails where job_id=@jobId", new { jobId = jobId });
            return data.ToList();
        }
        #endregion

        #region job card labour details
        public async Task SaveJobCardLabourDetail(JobCardLabourDetailDto data, IDbConnection con = null)
        {

            string query = @"insert into JobCardLabourDetail (job_id,description,quantity,unit,amount,totalAmount,created_by,created_on,updated_by,updated_on) values(@job_id,@description,@quantity,@unit,@amount,@totalAmount,@created_by,@created_on,@updated_by,@updated_on)";
            await _dapperManager.ExecuteAsync(query, data);

        }
        public async Task UpdateJobCardLabourDetail(JobCardLabourDetailDto data, IDbConnection con = null)
        {
            string query = @"update JobCardLabourDetail set description=@description,unit=@unit,quantity=@quantity, amount=@amount, totalAmount=@totalAmount,updated_by=@updated_by,updated_on=@updated_on where id=@id";
            await _dapperManager.QuerySingleOrDefaultAsync<int>(query, data);
        }
        public async Task<List<JobCardLabourDetailDto>> GetLabourByJobId(int jobId)
        {
            var data = await _dapperManager.QueryAsync<JobCardLabourDetailDto>("select * from JobCardLabourDetail where job_id=@jobId", new { jobId = jobId });
            return data.ToList();
        }
        #endregion
        public async Task<int> GetTotalJobCard()
        {
            return await _dapperManager.QuerySingleOrDefaultAsync<int>("select count(*) from JobCard");
        }
    }
}
