﻿using Gms.DAL.Component;
using Gms.DAL.DTO;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gms.DAL.Repo
{
    public interface IVehicleDetailRepo
    {
        Task<List<dynamic>> GetAllVechiles(PagedDatasource pagedDatasource);
        Task Save(VehicleDetailsDto data);
        Task Delete(int id);
        Task<VehicleDetailsDto> GetById(int id);
        Task Update(VehicleDetailsDto data);
    }
    public class VehicleDetailRepo : IVehicleDetailRepo
    {
        private readonly IDapperManager _dapperManager;
        private readonly IPaginationQueryBuilder _paginationQueryBuilder;
        public VehicleDetailRepo(IDapperManager dapperManager, IPaginationQueryBuilder paginationQueryBuilder)
        {
            _dapperManager = dapperManager;
            _paginationQueryBuilder = paginationQueryBuilder;
        }
        public async Task<VehicleDetailsDto> GetById(int id)
        {
            string query = "SELECT * from VehicleDetails WHERE ID=@ID";
           return await _dapperManager.QuerySingleOrDefaultAsync<VehicleDetailsDto>(query, new { Id = id });
        }
        public async Task Update(VehicleDetailsDto data)
        {
            string query = "update VehicleDetails set Registration_Number=@Registration_Number,Manufacturer=@Manufacturer,Model=@Model,Variant=@Variant,Options=@Options,Mfd_Year=@Mfd_Year,Transmition_Type=@Transmition_Type,Fuel_Type=@Fuel_Type,Body_Type=@Body_Type,Customer_Id =@Customer_Id  WHERE ID=@ID";
             await _dapperManager.ExecuteAsync(query, data);
        }
        public async Task Delete(int id)
        {
            string query = "DELETE FROM VehicleDetails WHERE ID=@ID";
            await _dapperManager.ExecuteAsync(query, new { Id = id });
        }

        public async Task<List<dynamic>> GetAllVechiles(PagedDatasource pagedDatasource)
        {
            if(!pagedDatasource.ISREQUIRED)
            {
                string query = @"SELECT vd.*,cd.Full_Name as Customer_Name FROM VehicleDetails Vd
                                JOIN CustomerDetails cd on cd.id=vd.customer_id";
                            var data = await _dapperManager.QueryAsync<dynamic>(query);
                return data.ToList();
            }
            else
            {
                string query = @"select  M.*,count(1) OVER() AS ROW_COUNT FROM(SELECT vd.*,cd.Full_Name as                      Customer_Name FROM VehicleDetails Vd
                                 JOIN CustomerDetails cd on cd.id=vd.customer_id) M";
                query = string.Format(query + "{0}", pagedDatasource.FILTERQUERY);
                string pagedQuery = _paginationQueryBuilder.ToPagedQuery(query, pagedDatasource);
                var data = await _dapperManager.QueryAsync<dynamic>(pagedQuery, new
                {
                    PAGENUMBER = pagedDatasource.PAGENUMBER,
                    PAGESIZE = pagedDatasource.PAGESIZE,
                    SORTBYCOLUMN = pagedDatasource.SORTBYCOLUMN,
                });
                return data.ToList();
            }

        }

        public async Task Save(VehicleDetailsDto data)
        {
            string query = "INSERT INTO VehicleDetails (Registration_Number,Manufacturer,Model,Variant,Options,Mfd_Year,Transmition_Type,Fuel_Type,Body_Type,Customer_Id,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON) VALUES(@Registration_Number,@Manufacturer,@Model,@Variant,@Options,@Mfd_Year,@Transmition_Type,@Fuel_Type,@Body_Type,@Customer_Id,@CREATED_BY,@CREATED_ON,@UPDATED_BY,@UPDATED_ON)";
            await _dapperManager.ExecuteAsync(query, data);
        }
    }
}
