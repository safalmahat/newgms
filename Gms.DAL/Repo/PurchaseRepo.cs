﻿using Gms.DAL.Component;
using Gms.DAL.DTO;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Gms.DAL.Repo
{
    public interface IPurchaseRepo
    {
        Task<int> Save(PurchaseDto data, IDbConnection con = null);
        Task Update(PurchaseDto data, IDbConnection con = null);
        Task<IEnumerable<dynamic>> Get(PagedDatasource pagedDatasource);
        Task<IEnumerable<dynamic>> GetPurchaseDetails(int purchaseId);
        Task<PurchaseDto> GetPurchase(int id);
        Task<int> GetTotalPurchaseCount();
        Task Delete(int id);
    }
    public class PurchaseRepo : IPurchaseRepo
    {
        private IDapperManager _dapperManager;
        private readonly IPaginationQueryBuilder _paginationQueryBuilder;
        public PurchaseRepo(IDapperManager dapperManager, IPaginationQueryBuilder paginationQueryBuilder)
        {
            _dapperManager = dapperManager;
            _paginationQueryBuilder = paginationQueryBuilder;

        }
        public async Task<int> Save(PurchaseDto data, IDbConnection con = null)
        {
            string query = @"insert into purchase(PurchaseDate,PurchaseNo,TotalBillAmount,TotalPaidAmount,TotalDueAmount,PaymentMethod,SupplierId,created_by,created_on) values(@PurchaseDate,@PurchaseNo,@TotalBillAmount,@TotalPaidAmount,@TotalDueAmount,@PaymentMethod,@SupplierId,@created_by,@created_on) SELECT SCOPE_IDENTITY()";
            int id = await _dapperManager.QuerySingleOrDefaultAsync<int>(con, query, data);
            return id;
        }

        public async Task Update(PurchaseDto data, IDbConnection con = null)
        {
            string query = @"update purchase set PurchaseDate=@PurchaseDate,PurchaseNo=@PurchaseNo,TotalBillAmount=@TotalBillAmount,TotalPaidAmount=@TotalPaidAmount,TotalDueAmount=@TotalDueAmount,PaymentMethod=@PaymentMethod,updated_by=@updated_by,updated_on=@updated_on where id=@id";
            await _dapperManager.ExecuteAsync(con, query, data);
        }
        public async Task<IEnumerable<dynamic>> Get(PagedDatasource pagedDatasource)
        {
            string query = @"select M.*,count(1) OVER() AS ROW_COUNT FROM (select p.*,s.name as supplierName from purchase p
left join suppliers s on p.SupplierId=s.id where p.status=1	) M	";
            query = string.Format(query + "{0}", pagedDatasource.FILTERQUERY);
            string pagedQuery = _paginationQueryBuilder.ToPagedQuery(query, pagedDatasource);
            var result = await _dapperManager.QueryAsync<dynamic>(pagedQuery, new
            {
                PAGENUMBER = pagedDatasource.PAGENUMBER,
                PAGESIZE = pagedDatasource.PAGESIZE,
                SORTBYCOLUMN = pagedDatasource.SORTBYCOLUMN,
            });
            return result;
        }
        public async Task<IEnumerable<dynamic>> GetPurchaseDetails(int purchaseId)
        {
            string query = @"select p.name as product_name, u.name as unit_name,pd.quantity,pd.price,pd.total_price as   total_price from purchase ps
                left join purchase_details pd on ps.id=pd.purchase_id
                left join product p on p.id=pd.product_id
                left join units u on u.id=pd.unit_id where pd.purchase_id=@purchase_id";
            var result = await _dapperManager.QueryAsync<dynamic>(query, new { purchase_id = purchaseId });
            return result;
        }
        public async Task<PurchaseDto> GetPurchase(int id)
        {
            return await _dapperManager.QuerySingleOrDefaultAsync<PurchaseDto>("select * from purchase where id=@id", new { id = id });
        }
        public async Task<int> GetTotalPurchaseCount()
        {
            return await _dapperManager.QuerySingleOrDefaultAsync<int>("select count(*) from purchase");
        }
        public  async Task Delete(int id)
        {
            string query = "update purchase set status=0 where id=@id";
            await _dapperManager.ExecuteAsync(query, new { id = id });
        }
    }
}
