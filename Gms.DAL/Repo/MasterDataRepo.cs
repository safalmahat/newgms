﻿using Gms.DAL.DTO;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gms.DAL.Repo
{
    public interface IMasterDataRepo
    {
        Task<List<MasterDataDto>> GetMasterDataByCategory(string category);
    }
    public class MasterDataRepo : IMasterDataRepo
    {
        private readonly IDapperManager _dapperManager;
        public MasterDataRepo(IDapperManager dapperManager)
        {
            _dapperManager = dapperManager;
        }
        public async Task<List<MasterDataDto>> GetMasterDataByCategory(string category)
        {
            var data= await _dapperManager.QueryAsync<MasterDataDto>("select * from GmsMaster where category=@category", new { category = category });
            return data.ToList();
        }
    }
}
