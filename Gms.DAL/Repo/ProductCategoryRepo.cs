﻿using Gms.DAL.Component;
using Gms.DAL.DTO;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gms.DAL.Repo
{
    public interface IProductCategoryRepo
    {
        Task<List<ProductCategoryDto>> GetProductAllCategory(PagedDatasource pagedDataSource);
        Task Save(ProductCategoryDto data);
        Task<ProductCategoryDto> Get(int id);
        Task Update(ProductCategoryDto data);
        Task Delete(int id);
    }
    public class ProductCategoryRepo : IProductCategoryRepo
    {
        private IDapperManager _dapperManager;
        private readonly IPaginationQueryBuilder _paginationQueryBuilder;
        public ProductCategoryRepo(IDapperManager dapperManager,
            IPaginationQueryBuilder paginationQueryBuilder
            )
        {
            _dapperManager = dapperManager;
            _paginationQueryBuilder = paginationQueryBuilder;
        }



        public async Task<List<ProductCategoryDto>> GetProductAllCategory(PagedDatasource pagedDataSource)
        {
            if (!pagedDataSource.ISREQUIRED)
            {
                var result = await _dapperManager.QueryAsync<ProductCategoryDto>("Select * from ProductCategory");
                return result.ToList();
            }
            else
            {
                string query = @"select M.*,count(1) OVER() AS ROW_COUNT FROM (Select * from ProductCategory where status=1) M	";

                query = string.Format(query + "{0}", pagedDataSource.FILTERQUERY);
                string pagedQuery = _paginationQueryBuilder.ToPagedQuery(query, pagedDataSource);
                var result = await _dapperManager.QueryAsync<ProductCategoryDto>(pagedQuery, new
                {
                    PAGENUMBER = pagedDataSource.PAGENUMBER,
                    PAGESIZE = pagedDataSource.PAGESIZE,
                    SORTBYCOLUMN = pagedDataSource.SORTBYCOLUMN
                });
                return result.ToList();
            }

        }

        public async Task Save(ProductCategoryDto data)
        {
            await _dapperManager.ExecuteAsync("insert into ProductCategory(name,description,status,created_by,created_on) values(@name,@description,@status,@created_by,@created_on)", data);
        }

        public async Task Update(ProductCategoryDto data)
        {
            await _dapperManager.ExecuteAsync("update ProductCategory set name=@name,description=@description, updated_by=@updated_by,updated_on=@updated_on where id=@id", data);
        }
        public async Task Delete(int id)
        {
            await _dapperManager.ExecuteAsync("update ProductCategory set status=0 where id=@id", new { id = id });
        }

        public async Task<ProductCategoryDto> Get(int id)
        {
            var result = await _dapperManager.QuerySingleOrDefaultAsync<ProductCategoryDto>("Select * from ProductCategory where id=@id", new { id = id });
            return result;
        }
    }
}
