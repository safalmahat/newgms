﻿using Gms.DAL.DTO;
using Gms.Database;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gms.DAL.Repo
{
    public interface ITokenRepo
    {
        void DeleteTokenByUserId(int userId);
        void CreateToken(TokenDto newToken);
        Task<TokenDto> GetTokenByValue(string value);
        void DeleteToken(string value);
    }
   public class TokenRepo: ITokenRepo
    {
        public readonly IDapperManager _dapperManager;
        public TokenRepo(IDapperManager dapperManager)
        {
            _dapperManager = dapperManager;
        }
        public void CreateToken(TokenDto newToken)
        {
            string query = @"INSERT INTO Token(User_Id,Value,Time_Stamp) VALUES(@User_Id,@Value,@Time_Stamp)";
            _dapperManager.Execute(query, newToken);
        }

        public void DeleteTokenByUserId(int userId)
        {
            var token = new TokenDto
            {
                user_id = userId
            };

            string query = "DELETE FROM Token  WHERE user_id = @user_id";
            _dapperManager.Execute(query, token);
        }
        public async Task<TokenDto> GetTokenByValue(string value)
        {
            string query = "SELECT * FROM TOKEN WHERE VALUE = @VALUE";

            return await _dapperManager.QuerySingleOrDefaultAsync<TokenDto>(query, new { VALUE = value });
        }
        public void DeleteToken(string value)
        {
            
            string query = "DELETE FROM TOKEN WHERE VALUE = @VALUE";
            _dapperManager.Execute(query, new { VALUE=value });
        }

    }
}
