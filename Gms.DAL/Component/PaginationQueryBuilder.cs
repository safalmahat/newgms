﻿using Gms.DAL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.Component
{
    public interface IPaginationQueryBuilder
    {
        string ToPagedQuery(string query, PagedDatasource pagedDatasource);
    }
    public class PaginationQueryBuilder : IPaginationQueryBuilder
    {

        public string ToPagedQuery(string query, PagedDatasource pagedDatasource)
        {
            string pagedQuery = string.Format(@" {0} ORDER BY {2} {1}
                                                 OFFSET((@PAGENUMBER - 1) * @PAGESIZE) ROWS
                                                 FETCH NEXT @PAGESIZE ROWS ONLY", query, pagedDatasource.SORTBYORDER == ColumnSortOrder.Descending ? "DESC" : "ASC", pagedDatasource.SORTBYCOLUMN);
            return pagedQuery;
        }
    }
}
