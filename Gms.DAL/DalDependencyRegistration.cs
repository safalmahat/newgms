﻿using Gms.DAL.Component;
using Gms.DAL.Repo;
using Gms.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Unity;

namespace Gms.DAL
{
    public class DalDependencyRegistration
    {
        public static void Configure(IUnityContainer container)
        {
            container.RegisterType<ICustomerDetailRepo, CustomerDetailRepo>()
            .RegisterType<IPaginationQueryBuilder, PaginationQueryBuilder>()
            .RegisterType<IVehicleDetailRepo, VehicleDetailRepo>()
            .RegisterType<IUserRepo, UserRepo>()
            .RegisterType<ITokenRepo, TokenRepo>()
            .RegisterType<IJobCardRepo, JobCardRepo>()
            .RegisterType<IMasterDataRepo,MasterDataRepo>()
            .RegisterType<ISupplierRepo, SupplierRepo>()
            .RegisterType<IUnitRepo, UnitRepo>()
            .RegisterType<IProductCategoryRepo, ProductCategoryRepo>()
            .RegisterType<IProductRepo, ProductRepo>()
            .RegisterType<IPurchaseDetailRepo, PurchaseDetailRepo>()
            .RegisterType<IPurchaseRepo, PurchaseRepo>()
            .RegisterType<IStockDetailRepo, StockDetailRepo>()
            ;
        }
    }
}
