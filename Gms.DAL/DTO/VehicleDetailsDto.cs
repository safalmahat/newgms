﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
   public class VehicleDetailsDto:BaseDto
    {
        public int Id { get; set; }
        public string Registration_Number { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Variant { get; set; }
        public string Options { get; set; }
        public int Mfd_Year { get; set; }
        public string Transmition_Type { get; set; }
        public string Fuel_Type { get; set; }
        public string Body_Type { get; set; }
        public string Customer_Id { get; set; }

    }
}
