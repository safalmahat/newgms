﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
   public class JobCardInventoryDetailDto:BaseDto
    {
        public int Id { get; set; }
        public int Job_Id { get; set; }
        public string Product { get; set; }
        public float Amount { get; set; }
        public float Price { get; set; }
        public int Unit { get; set; }
    }
}
