﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
  public  class ProductDto:BaseDto
    {
        public int id { get; set; }
        public int CategoryId { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string thumbnail { get; set; }
        public double? ActualPrice { get; set; }
        public double? DiscountPercentage { get; set; }
        public double? price { get; set; }
        public int stock { get; set; }
        public string description { get; set; }
        public bool status { get; set; } = true;
    }
}
