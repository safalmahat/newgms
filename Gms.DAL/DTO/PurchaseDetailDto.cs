﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
   public class PurchaseDetailDto:BaseDto
    {
        public int Id { get; set; }
        public int PurchaseId { get; set; }
        public int Quantity { get; set; }
        public int ProductId { get; set; }
        public int UnitId { get; set; }
        public int Price { get; set; }
        public int TotalPrice { get; set; }
        public string ProductName { get; set; }
        public string UnitName { get; set; }
    }
}
