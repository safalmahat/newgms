﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
   public class UserRolesDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
