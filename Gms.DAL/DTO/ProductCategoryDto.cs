﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
   public class ProductCategoryDto:BaseDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool status { get; set; }

    }
}
