﻿using Gms.DAL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
    public class UnitDto : BaseDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public bool status { get; set; }
    }
}
