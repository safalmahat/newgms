﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
    public class PagedDatasource : DataSourceFilter
    {
        public int PAGENUMBER { get; set; }
        public int PAGESIZE { get; set; }
        public string FILTERQUERY { get; set; }
        public bool ISREQUIRED { get; set; } = true;

    }
    public abstract class DataSourceFilter
    {
        public string SORTBYCOLUMN { get; set; }
        public ColumnSortOrder SORTBYORDER { get; set; }
    }

    public enum ColumnSortOrder
    {
        Ascending = 1,
        Descending = 2
    }
}
