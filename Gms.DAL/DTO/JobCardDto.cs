﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
   public class JobCardDto:BaseDto
    {
        public int Id { get; set; }
        public string Job_Number { get; set; }
        public string Km_Reading { get; set; }
        public int Vehicle_Id { get; set; }
        public float Fuel_Quantity { get; set; }
    }
}
