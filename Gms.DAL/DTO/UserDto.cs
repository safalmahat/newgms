﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
   public class UserDto:BaseDto
    {
        public int id { get; set; }
        public string user_name { get; set; }
        public string hash { get; set; }
        public string temp_hash { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public bool is_locked { get; set; }
        public int password_failure_count { get; set; }
    
    }
}
