﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
   public class JobCardLabourDetailDto:BaseDto
    {
        public int Id { get; set; }
        public int Job_Id { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public string Unit { get; set; }
        public float Amount { get; set; }
        public float TotalAmount { get; set; }

    }
}
