﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
  public  class SupplierDto:BaseDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string contact_person { get; set; }
        public string phone_number { get; set; }
        public string mobile_number { get; set; }
        public string email { get; set; }
        public bool status { get; set; }
    }
}
