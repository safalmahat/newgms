﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
  public  class RolesDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
    }
}
