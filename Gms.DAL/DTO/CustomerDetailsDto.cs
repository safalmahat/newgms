﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
   public class CustomerDetailsDto: BaseDto
    {
        public int Id { get; set; }
        public string Full_Name { get; set; }
        public string Phone_Number { get; set; }
        public string Mobile_Number { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
       
    }
}
