﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
  public  class JobCardDetailsDto:BaseDto
    {
        public int Id { get; set; }
        public int Job_Id { get; set; }
        public string Complain_Description { get; set; }
    }
}
