﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
    public class PurchaseDto:BaseDto
    {
        public int Id { get; set; }
        public DateTime? PurchaseDate{ get; set; }
        public string PurchaseNo { get; set; }
        public float TotalBillAmount { get; set; }
        public float TotalPaidAmount { get; set; }
        public float TotalDueAmount { get; set; }
        public string PaymentMethod { get; set; }
        public int? SupplierId { get; set; }
        public bool Status { get; set; } = true;
    }
}
