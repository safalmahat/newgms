﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
   public class TokenDto
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public string value { get; set; }
        public DateTime time_stamp { get; set; }
    }
}
