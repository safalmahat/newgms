﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gms.DAL.DTO
{
   public class StockDetailDto:BaseDto
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public int ProductId { get; set; }
    }
}
