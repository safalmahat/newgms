﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Unity;

namespace Gms.Database
{
   public class DatabaseDependencyRegistration
    {
        public static void Configure(IUnityContainer container, IConfiguration configuration)
        {
            container.RegisterType<IDapperManager, DapperManager>()
            .RegisterInstance<IDatabaseManager>(new DatabaseManager()
            {
                ConnectionString = configuration.GetSection("ConnectionStrings")["DefaultConnection"]
            })
            .RegisterType<ITransctionScopeFactory, TransctionScopeFactory>();
        }
           
    }
}
