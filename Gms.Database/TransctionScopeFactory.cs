﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace Gms.Database
{
    public interface ITransctionScopeFactory
    {
        TransactionScope CreatetransctionScope();
    }
    public class TransctionScopeFactory : ITransctionScopeFactory
    {
        public TransactionScope CreatetransctionScope()
        {
            return new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
        }
    }
}
